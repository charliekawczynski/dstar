# ************* CLUSTER CONFIGURATION ******************
# CLUSTER = 'PC_CK'
# CLUSTER = 'Workstation_CK'
# On Hoffman / DOE, run "module load gcc/4.9.3" in terminal before compile
CLUSTER = 'Hoffman2'
# CLUSTER = 'DOE'
# ****************** OS SETTINGS ********************
ifdef SystemRoot
	OS_USED = 'OS_WINDOWS'
else
	OS_USED = 'OS_LINUX'
endif

ifeq ($(OS_USED),'OS_WINDOWS')
	RM = del
	PS = $(strip \\)
else
	PS = $(strip /)
	RM = rm
endif

ifeq ($(CLUSTER),$(filter $(CLUSTER),'Workstation_CK' 'PC_CK'))
	PS_clean = $(strip \)
endif
ifeq ($(CLUSTER),$(filter $(CLUSTER),'Hoffman2'))
	PS_clean = $(strip /)
endif

# ************* MAKE CONFIGURATION ******************

# MKE = gmake
MKE = make
# ************* DIRECTORY SETTINGS ******************
SRC_DIR = ..$(PS)code

ifeq ($(CLUSTER),'PC_CK')
	TARGET_DIR = C:$(PS)Users$(PS)Charlie$(PS)Documents$(PS)DSTAR$(PS)SIMS$(PS)DSTAR1
endif

ifeq ($(CLUSTER),'Workstation_CK')
	TARGET_DIR = C:$(PS)Users$(PS)charl$(PS)Documents$(PS)DSTAR$(PS)DSTAR5
endif

ifeq ($(CLUSTER),'Hoffman2')
	TARGET_DIR = $(PS)u$(PS)home$(PS)c$(PS)ckawczyn$(PS)project-morley$(PS)DSTAR_out$(PS)SIMS$(PS)D1
endif

ifeq ($(CLUSTER),'DOE')
	TARGET_DIR = $(PS)global$(PS)cscratch1$(PS)sd$(PS)cfusion$(PS)DSTAR_out
endif

MOD_DIR = .$(PS)mod$(PS)
OBJ_DIR = .$(PS)obj$(PS)
LIB_DIR = $(SRC_DIR)$(PS)lib

# ************** COMPILER & STANDARD ****************
# ifeq ($(CLUSTER),$(filter $(CLUSTER),'Workstation_CK' 'PC_CK' 'Hoffman2'))
# 	FC = gfortran
# endif
ifeq ($(CLUSTER),$(filter $(CLUSTER),'Workstation_CK' 'PC_CK'))
	FC = gfortran
endif
ifeq ($(CLUSTER),'Hoffman2')
	FC = mpif90
endif
ifeq ($(CLUSTER),'DOE')
	FC = ftn
endif

$(info CLUSTER = $(CLUSTER))
$(info BUILD COMMAND = $(FC))
$(info TARGET = $(TARGET_DIR))
$(info PS = $(PS))
$(info PS_clean = $(PS_clean))
$(info OS_USED = $(OS_USED))

# ****************** DEFAULT FLAGS ******************
FCFLAGS = -J"$(MOD_DIR)" -fimplicit-none -Wuninitialized -cpp
# FCFLAGS += -std=legacy
FCFLAGS += -std=gnu
# FCFLAGS += -std=f95
# FCFLAGS += -std=f2003
# FCFLAGS += -std=2008

# Precision (most files contain this)
# FCFLAGS += -D_SINGLE_PRECISION_
FCFLAGS += -D_DOUBLE_PRECISION_
# FCFLAGS += -D_QUAD_PRECISION_

# FFT_poisson.f90:
# FCFLAGS += -D_FFT_FULL_
# FCFLAGS += -D_FFT_RADIX2_

# is_nan.f90:
# FCFLAGS += -D_ISNAN_USE_HACK_

# Order of accuracy of finite difference stencil
# stencils.f90:
# FCFLAGS += -D_STENCILS_O2_
# FCFLAGS += -D_STENCILS_O4_

FCFLAGS += -fopenmp

ifeq ($(OS_USED),'OS_WINDOWS')
	FCFLAGS += -D_OS_WINDOWS_
else
	FCFLAGS += -D_OS_LINUX_
endif

# FCFLAGS += -D_DEL_PLANAR_X_
# FCFLAGS += -D_DEL_PLANAR_Y_
# FCFLAGS += -D_DEL_PLANAR_Z_

# ******************** LIBRARIES ********************
# FLIBS = $(LIB_DIR)$(PS)tecio.lib

# ****************** DEBUG **********************
# fcheck=all includes -fcheck-array-temporaries, 
# which triggers tons of warning in stencils.f90
# So include all but this one:

# -O0 bad in cygwin generated gfortran
# FC_DEBUG += -Wall -Wextra -fbacktrace -O0 -Og
FC_DEBUG += -Wall -Wextra -fbacktrace -O0

# FC_DEBUG += -Wall -Wextra -fbacktrace
# FC_DEBUG += -fcheck=bounds -fcheck=do -fcheck=mem -fcheck=pointer -fcheck=recursion

FC_DEBUG += -D_DEBUG_VEC_OMP_
FC_DEBUG += -D_DEBUG_IS_OMP_
FC_DEBUG += -D_DEBUG_MPI_

# _DEBUG_COORDINATES_ is for rigorous mesh / stencil checking
# FC_DEBUG += -D_DEBUG_COORDINATES_

# FC_DEBUG += -D_DEBUG_APPLY_STITCHES
# FC_DEBUG += -D_STENCILS_SUPPRESS_WARNING_

# Suppress temporrary array warning:
# FC_DEBUG += -Wno-array-temporaries

# Code verification
# FCFLAGS += -D_EXPORT_JAC_SF_CONVERGENCE_
# FCFLAGS += -D_EXPORT_JAC_VF_CONVERGENCE_
# FCFLAGS += -D_EXPORT_SOR_CONVERGENCE_
# FCFLAGS += -D_EXPORT_CG_CONVERGENCE_
# FCFLAGS += -D_EXPORT_PCG_SF_CONVERGENCE_
# FCFLAGS += -D_EXPORT_PCG_VF_CONVERGENCE_
# FCFLAGS += -D_EXPORT_GS_CONVERGENCE_
# FCFLAGS += -D_EXPORT_MG_CONVERGENCE_

# ****************** PROFILING **********************
# To profile: run sim profile flag, then run
# gprof DSTAR.exe > timeReport.txt
FC_PROFILE += -pg

# **************** OPTIMIZE *********************
FC_OPTIMIZE += -g -O3
FC_OPTIMIZE += -D_PARALLELIZE_VEC_OMP_
FC_OPTIMIZE += -D_PARALLELIZE_IS_OMP_
FC_OPTIMIZE += -D_PARALLELIZE_VEC_
FC_OPTIMIZE += -D_OPTIMIZE_IO_TOOLS_

# **************** OPTIMIZE *********************
PARALLELIZE += -D_USE_MPI_
PARALLELIZE += -D_USE_OMP_
# PARALLELIZE += mpif90
# PARALLELIZE += mpirun -np 4

FCFLAGS += $(FC_DEBUG)
# FCFLAGS += $(FC_PROFILE)
# FCFLAGS += $(FC_OPTIMIZE)
FCFLAGS += $(PARALLELIZE)

TARGET = $(TARGET_DIR)$(PS)DSTAR

# **************** INCLUDE PATHS ********************
VPATH =\
	$(TARGET_DIR) \
	$(SRC_DIR)$(PS)cell \
	$(SRC_DIR)$(PS)clock \
	$(SRC_DIR)$(PS)ordinates \
	$(SRC_DIR)$(PS)domain \
	$(SRC_DIR)$(PS)globals \
	$(SRC_DIR)$(PS)IT \
	$(SRC_DIR)$(PS)IO \
	$(SRC_DIR)$(PS)index_mapping \
	$(SRC_DIR)$(PS)mesh \
	$(SRC_DIR)$(PS)mpi \
	$(SRC_DIR)$(PS)string \
	$(SRC_DIR)$(PS)user \
	$(SRC_DIR)$(PS)vec \
	$(SRC_DIR)$(PS)VF \
	$(SRC_DIR)$(PS)VG \
	$(SRC_DIR)$(PS)VI \

# **************** SOURCE FILES *********************

SRCS_F =\
	$(SRC_DIR)$(PS)globals$(PS)current_precision.f90 \
	$(SRC_DIR)$(PS)string$(PS)string.f90 \
	$(SRC_DIR)$(PS)index_mapping$(PS)index_mapping.f90 \
	$(SRC_DIR)$(PS)mpi$(PS)mpi_obj.f90 \
	$(SRC_DIR)$(PS)IT$(PS)IS_OMP.f90 \
	$(SRC_DIR)$(PS)IO$(PS)datatype_conversion.f90 \
	$(SRC_DIR)$(PS)IO$(PS)inquire_funcs.f90 \
	$(SRC_DIR)$(PS)IO$(PS)IO_check.f90 \
	$(SRC_DIR)$(PS)IO$(PS)IO_tools.f90 \
	$(SRC_DIR)$(PS)IO$(PS)exp_Tecplot_Header.f90 \
	$(SRC_DIR)$(PS)IO$(PS)exp_Tecplot_Zone.f90 \
	$(SRC_DIR)$(PS)clock$(PS)unit_conversion.f90 \
	$(SRC_DIR)$(PS)clock$(PS)clock.f90 \
	$(SRC_DIR)$(PS)clock$(PS)stop_clock.f90 \
	$(SRC_DIR)$(PS)vec$(PS)vec_OMP.f90 \
	$(SRC_DIR)$(PS)vec$(PS)vec.f90 \
	$(SRC_DIR)$(PS)user$(PS)DSTAR.f90 \

SRCS_F += $(TARGET_DIR)$(PS)main.f90

# **************** DO NOT EDIT BELOW HERE *********************
# **************** DO NOT EDIT BELOW HERE *********************
# **************** DO NOT EDIT BELOW HERE *********************

OBJS_F = $(patsubst %.f90,$(OBJ_DIR)$(PS)%.o,$(notdir $(SRCS_F)))

$(warning ************************************)
$(warning ********** BUILDING DSTAR **********)
$(warning ************************************)

all: $(TARGET)

$(TARGET): $(OBJS_F)
	$(FC) -o $@ $(FCFLAGS) $(OBJS_F) $(FLIBS)

$(OBJ_DIR)$(PS)%.o: %.f90
	$(FC) $(FCFLAGS) -c -o $@ $< $(info )

clean:
	-$(RM) $(subst $(PS),$(PS_clean),$(MOD_DIR)$(PS)*.mod)
	-$(RM) $(subst $(PS),$(PS_clean),$(OBJ_DIR)$(PS)*.o)
	-$(RM) $(subst $(PS),$(PS_clean),$(TARGET).exe)
	-$(RM) $(subst $(PS),$(PS_clean),$(TARGET))

run: myRun
myRun: $(TARGET)
	cd $(TARGET_DIR) && $(MKE) run


info:;  @echo " "
	@echo " "
	@echo "Source files:"
	@echo $(SRCS_F)
	@echo " "
	@echo "Object files:"
	@echo $(OBJS_F)
	@echo " "
	@echo "Compiler          : $(FC)"
	@echo "Library directory : $(LIB_DIR)"
	@echo "Target directory  : $(TARGET_DIR)"
	@echo "Modules directory : $(MOD_DIR)"
	@echo "Object directory  : $(OBJ_DIR)"
	@echo " "