      module ordinates_mod
      use current_precision_mod
      use VI_mod
      use VG_mod
      implicit none

      private
      public :: ordinates
      public :: init,delete,print,export

      type ordinates
        type(VG),dimension(3) :: d
        ! cell ID to which VG belongs, 
        ! necessary for plotting. CID = i for C data
        type(VI) :: CID
      end type

      interface init;    module procedure init_c;        end interface
      interface init;    module procedure init_copy_c;   end interface
      interface delete;  module procedure delete_c;      end interface
      interface print;   module procedure print_c;       end interface
      interface export;  module procedure export_c;      end interface

      contains

      subroutine init_c(c,d,dir)
        implicit none
        type(ordinates),intent(inout) :: c
        type(VG),intent(in) :: d
        integer,intent(in) :: dir
        call init(c%d(dir),d)
      end subroutine

      subroutine init_copy_c(a,b)
        implicit none
        type(ordinates),intent(inout) :: a
        type(ordinates),intent(in) :: b
        integer :: i
        do i=1,3; call init(a%d(i),b%d(i)); enddo
      end subroutine

      subroutine delete_c(dir)
        implicit none
        type(ordinates),intent(inout) :: dir
        integer :: i
        do i=1,3; call delete(dir%d(i)); enddo
      end subroutine

      subroutine print_c(dir)
        implicit none
        type(ordinates),intent(in) :: dir
        call export(dir,6)
      end subroutine

      subroutine export_c(dir,un)
        implicit none
        type(ordinates),intent(in) :: dir
        integer,intent(in) :: un
        integer :: i
        do i=1,3; call export(dir%d(i),un); enddo
      end subroutine

      end module