Combinations to think about

Index Tree (IT):
  Domain (D):                        T,I,B,G (total,interior,boundary,ghost)
  Computational Cell Location (CCL): C,N,F,E (cell center, node, face, edge)
  Boundary Condition (BC):           D,N,P,R (Dirichlet, Neumann, periodic, Robin)

  Neighbor index (NI):               ?
  Index along direction (NI):        ?

Options:

  D,CCL,BC: IT%T%C%P(1)%i%s  ! total cell centered with periodic along x
  D,CCL,BC: IT%T%C%P(1)%M%i%s ! mirror to total cell centered with periodic along x
  call apply(f,IT%T%C%P(1))%s,IT%T%C%P(1)%i%s) ! will work without a doubt
  call apply(f,IT%T%C%D(1)%s,IT%T%C%P(1)%i%s) ! will work without a doubt

  D,CCL,BC: IT%I%C%i%s ! interior
  D,CCL,BC: IT%T%C%i%s ! total
  D,CCL,BC: IT%I%C%i%s ! interior cell centered
  D,CCL,BC: IT%T%C%A(1)%s ! total along X for derivatives / interpolations
  D,CCL,BC: IT%T%C%A(2)%s ! total along Y for derivatives / interpolations
  D,CCL,BC: IT%T%C%A(3)%s ! total along Z for derivatives / interpolations

  CCL,D,BC: IT%C%I%P

  D,BC,CCL: IT%I%D%C
  CCL,BC,D: IT%C%C%I

Situations where indexes are needed:
  interp: call assign(f,g,m%alpha,i_f,i_g,i_alpha,m)
  interp: call add_product(f,g,m%beta,i_f,i_g,i_beta,m)
  interp: call add_product(f,g,m%alpha,m%beta,i_f,i_g,i_alpha,i_beta)


Good points:
  -BC should be last, because this does not always exist, so the tree may
  have leaves but no branches to reach them (not natural).
  -
