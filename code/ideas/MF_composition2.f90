This allows for

type(mesh_flat) :: MF

y-locations at x-face of cells of total domain: MF%total%F(1)%d(2)%f%f(i)
y-locations at x-face of cells of total domain: MF%total%F(1)%d(2)%i%i(i)

Better option?:
y-locations at x-face of cells of interior domain: MF%F(1)%d(2)%total%i(i)


type mesh_flat
  type(domain_flat) :: total,interior,boundary,ghost

type domain_flat
  type(ordinates) :: C,N
  type(ordinates),dimension(3) :: F,E

type ordinates
  type(group),dimension(3) :: d
  type(VI) :: CID

type group
  type(IS) :: i
  type(vec) :: f

type vec
  integer :: s
  real(cp),dimension(:),allocatable :: f

type IS
  integer :: s
  integer,dimension(:),allocatable :: i


type group ! will require all routines that group has
  type(vec) :: v
  type(IS) :: interior,ghost
  type(IS) :: boundary         ! null for C data
  type(IS) :: CID              ! = MF%C%total
  logical :: TF_boundary       ! F uf data lives on C
end type

type IT ! index tree
  type(IS) :: total,interior,boundary,ghost
end type

type IT ! index tree
  type(IS) :: interior,ghost
  type(IS) :: boundary         ! null for C data
  type(IS) :: CID              ! = MF%C%total
  logical :: TF_boundary       ! F uf data lives on C
end type
