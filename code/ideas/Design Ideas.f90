Design Ideas
--------------------------- DATA STRUCTURES ---------------------------

index_set (IS)
	integer,dimension(:),allocatable :: i
	integer :: N; logical :: defined

float_vector (vec)
	real(cp),dimension(:),allocatable :: f
	integer :: N; logical :: defined

sparse (S): type(vec),dimension(M),allocatable :: row  (1:N)

interp_stencils (S): type(tridiag) :: alpha,beta
derivative_stencils (T): type(tridiag) :: coll_CC,col_N

coordinates (c): real(cp),dimension(:),allocatable :: hc,hn,dhc,dhn,alpha,beta

grid (grid): type(coordinates),dimension(3) :: c

mesh (mesh): type(grid),dimension(:),allocatable :: g; integer :: N

mesh_flat (mesh):
	type(vec) :: 

--------------------------- IMPLEMENTATION ---------------------------

sub interp(g,f,m,i)

call add_product(a,i%face,b,i%CC,m%alpha,i%alpha,b,i%face,i%s,m%alpha)

call assign(a,i%face,b,i%CC,m%alpha,i%alpha,b,i%face,i%s,m%alpha)


do j=1,i%N
g%f(i%s(j)) = f%f(i%s(j))*m%alpha(i%s(j)) +
              f%f(i%s(j))*m%beta(i%s(j))
enddo

