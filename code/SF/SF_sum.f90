      module SF_sum_mod
      use current_precision_mod
      use SF_base_mod
      implicit none

      private
      public :: sum

      interface sum;       module procedure sum_SF1;      end interface
      interface sum;       module procedure sum_SF3;      end interface
      interface sum;       module procedure sum_SF9;      end interface

      contains

      function sum_SF1(B,i,s) result(A)
        implicit none
        type(SF),intent(in) :: B
        integer,intent(in) :: s
        integer,dimension(s),intent(in) :: i
        real(cp) :: A
        integer :: j
        A = 0.0_cp
        do j=1,s
          A = A + B%f(i(j))
        enddo
      end function

      function sum_SF3(B1,B2,B3,i,s) result(A)
        implicit none
        type(SF),intent(in) :: B1,B2,B3
        integer,intent(in) :: s
        integer,dimension(s),intent(in) :: i
        real(cp) :: A
        integer :: j
        A = 0.0_cp
        do j=1,s
          A = A + B1%f(i(j)) + B2%f(i(j)) + B3%f(i(j))
        enddo
      end function

      function sum_SF9(B1,B2,B3,B4,B5,B6,B7,B8,B9,i,s) result(A)
        implicit none
        type(SF),intent(in) :: B1,B2,B3,B4,B5,B6,B7,B8,B9
        integer,intent(in) :: s
        integer,dimension(s),intent(in) :: i
        real(cp) :: A
        integer :: j
        A = 0.0_cp
        do j=1,s
          A = A + B1%f(i(j)) + B2%f(i(j)) + B3%f(i(j)) + &
                  B4%f(i(j)) + B5%f(i(j)) + B6%f(i(j)) + &
                  B7%f(i(j)) + B8%f(i(j)) + B9%f(i(j))
        enddo
      end function

      end module