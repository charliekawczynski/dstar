      module SF_add_mod
      use current_precision_mod
      use SF_base_mod
      implicit none

      private
      public :: add

      interface add;       module procedure add_SF_C;       end interface
      interface add;       module procedure add_SF1;        end interface
      interface add;       module procedure add_SF2;        end interface
      interface add;       module procedure add_SF3;        end interface
      interface add;       module procedure add_SF9;        end interface

      contains

      subroutine add_SF_C(A,B,i,s)
        implicit none
        type(SF),intent(inout) :: A
        real(cp),intent(in) :: B
        integer,intent(in) :: s
        integer,dimension(s),intent(in) :: i
        integer :: j
        do j=1,s
          A%f(i(j)) = A%f(i(j)) + B
        enddo
      end subroutine

      subroutine add_SF1(A,B1,i,s)
        implicit none
        type(SF),intent(inout) :: A
        type(SF),intent(in) :: B1
        integer,intent(in) :: s
        integer,dimension(s),intent(in) :: i
        integer :: j
        do j=1,s
          A%f(i(j)) = A%f(i(j)) + B1%f(i(j))
        enddo
      end subroutine

      subroutine add_SF2(A,B1,B2,i,s)
        implicit none
        type(SF),intent(inout) :: A
        type(SF),intent(in) :: B1,B2
        integer,intent(in) :: s
        integer,dimension(s),intent(in) :: i
        integer :: j
        do j=1,s
          A%f(i(j)) = B1%f(i(j)) + B2%f(i(j))
        enddo
      end subroutine

      subroutine add_SF3(A,B1,B2,B3,i,s)
        implicit none
        type(SF),intent(inout) :: A
        type(SF),intent(in) :: B1,B2,B3
        integer,intent(in) :: s
        integer,dimension(s),intent(in) :: i
        integer :: j
        do j=1,s
          A%f(i(j)) = B1%f(i(j)) + B2%f(i(j)) + B3%f(i(j))
        enddo
      end subroutine

      subroutine add_SF9(A,B1,B2,B3,B4,B5,B6,B7,B8,B9,i,s)
        implicit none
        type(SF),intent(inout) :: A
        type(SF),intent(in) :: B1,B2,B3,B4,B5,B6,B7,B8,B9
        integer,intent(in) :: s
        integer,dimension(s),intent(in) :: i
        integer :: j
        do j=1,s
          A%f(i(j)) = B1%f(i(j)) + B2%f(i(j)) + B3%f(i(j)) + &
                      B4%f(i(j)) + B5%f(i(j)) + B6%f(i(j)) + &
                      B7%f(i(j)) + B8%f(i(j)) + B9%f(i(j))
        enddo
      end subroutine

      end module