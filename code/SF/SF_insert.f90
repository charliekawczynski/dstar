      module SF_insert_mod
      ! Compiler flags: (_DEBUG_SF_)
      use current_precision_mod
      use SF_base_mod
      use SF_assign_mod
      implicit none

      private
      public :: insert
      public :: insert_unique
      public :: append

      interface insert;          module procedure insert_element_SF;    end interface
      interface insert_unique;   module procedure insert_unique_SF;     end interface
      interface append;          module procedure app_element_SF;       end interface

      contains

      subroutine insert_element_SF(v,e,i)
        implicit none
        type(SF),intent(inout) :: v
        real(cp),intent(in) :: e
        integer,intent(in) :: i
        type(SF) :: temp
        integer :: j,k,s_temp
        if (.not.allocated(v%f)) then
          call init(v,1)
          call assign(v,e,1)
        else
#ifdef _DEBUG_SF_
          if ((i.lt.1).or.(i.gt.v%s+1)) then
            write(*,*) 'Error: insert_element outside of allowable bounds.'
            write(*,*) 'i = ',i
            write(*,*) 'v%s = ',v%s
            stop 'Code stopped in insert_element_SF in SF_insert.f90'
          endif
#endif
          call init(temp,v)
          s_temp = v%s+1
          call init(v,s_temp)
          call assign(v,temp,(/(j,j=1,i-1)/),i-1)
          call assign(v,e,i)
          call assign(v,temp,(/(j,j=i+1,v%s)/),(/(k,k=i,v%s-1)/),v%s-i)
          call delete(temp)
        endif
      end subroutine

      subroutine insert_unique_SF(A,e,tol)
        ! Insert i into A if i is not already in A
        implicit none
        type(SF),intent(inout) :: A
        real(cp),intent(in) :: e,tol
        integer :: j
        logical :: in_set
        if (.not.allocated(A%f)) then
          call init(A,1)
          call assign(A,e,1)
        else
          in_set = .false.
          do j=1,A%s
            if (abs(A%f(j) - e).lt.tol) then
              in_set = .true.; exit
            endif
          enddo
          if (.not.in_set) call append(A,e)
        endif
      end subroutine

      subroutine app_element_SF(v,e)
        implicit none
        type(SF),intent(inout) :: v
        real(cp),intent(in) :: e
        type(SF) :: temp
        integer :: j,s_temp
        if (.not.allocated(v%f)) then
          call init(v,1)
          call assign(v,e,1)
        else
          call init(temp,v)
          s_temp = v%s+1
          call init(v,s_temp)
          call assign(v,temp,(/(j,j=1,s_temp-1)/),s_temp-1)
          call assign(v,e,s_temp)
          call delete(temp)
        endif
      end subroutine

      end module