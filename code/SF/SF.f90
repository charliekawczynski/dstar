      module SF_mod
      use SF_base_mod
      use SF_assign_mod
      use SF_add_mod
      use SF_sum_mod
      use SF_norm_L2_mod
      use SF_insert_mod
      use SF_unique_mod
      implicit none

      private
      public :: SF,init,delete,export,print ! from SF_base_mod
      public :: assign
      public :: add
      public :: sum
      public :: norm_L2
      public :: insert
      public :: insert_unique
      public :: append
      public :: unique

      end module