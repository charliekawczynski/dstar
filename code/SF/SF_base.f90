      module SF_base_mod
      ! Compiler flags: (_DEBUG_SF_)
      use current_precision_mod
      implicit none

      private
      public :: SF
      public :: init,delete,export,print

      public :: check_allocated,check_bounds,check_size

      type SF
        integer :: s
        real(cp),dimension(:),allocatable :: f
      end type

      interface init;              module procedure init_size;            end interface
      interface init;              module procedure init_array;           end interface
      interface init;              module procedure init_copy;            end interface
      interface delete;            module procedure delete_SF;            end interface
      interface export;            module procedure export_SF;            end interface
      interface print;             module procedure print_SF;             end interface

      interface check_bounds;      module procedure check_bounds_SF;      end interface
      interface check_allocated;   module procedure check_allocated_SF;   end interface
      interface check_size;        module procedure check_size_SF;        end interface

      contains

      subroutine init_size(v,s)
        implicit none
        type(SF),intent(inout) :: v
        integer,intent(in) :: s
#ifdef _DEBUG_SF_
        if (s.lt.1) then
          write(*,*) 'Error: cannot init array of size<1 in init_size in SF_base.f90'
          write(*,*) 's = ',s
          stop 'Done'
        endif
#endif
        call delete(v)
        v%s = s
        allocate(v%f(s))
      end subroutine

      subroutine init_array(v,f,s)
        implicit none
        type(SF),intent(inout) :: v
        real(cp),dimension(s),intent(in) :: f
        integer,intent(in) :: s
#ifdef _DEBUG_SF_
        if (size(f).ne.s) stop 'Error: size(f) mismatch in init_size in SF_base.f90'
#endif
        call init(v,s)
        v%f = f
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(SF),intent(inout) :: a
        type(SF),intent(in) :: b
#ifdef _DEBUG_SF_
        call check_allocated(b,'init_copy')
#endif
        call delete(a)
        allocate(a%f(b%s))
        a%s = b%s
        a%f = b%f
      end subroutine

      subroutine check_allocated_SF(v,s)
        implicit none
        type(SF),intent(in) :: v
        character(len=*),intent(in) :: s
        if (.not.allocated(v%f)) then
          write(*,*) 'Error: need allocated SF in ',s,' in SF_base.f90'
          stop 'Done'
        endif
      end subroutine

      subroutine check_bounds_SF(v,i,s)
        implicit none
        type(SF),intent(in) :: v
        integer,intent(in) :: i
        character(len=*),intent(in) :: s
        call check_allocated(v,s)
        if (.not.((i.le.v%s).and.(i.ge.1))) then
          write(*,*) 'Error: SF index bound violated in ',s,' in SF_base.f90'
          write(*,*) '(i.ge.1) = ',(i.ge.1)
          write(*,*) '(i.le.v%s) = ',(i.le.v%s)
          write(*,*) 'i = ',i
          write(*,*) 'v%s = ',v%s
          stop 'Done'
        endif
      end subroutine

      subroutine check_size_SF(i,s,st)
        ! Is this routine flawed? Will specifying the dimension
        ! force the dimension?
        implicit none
        integer,dimension(s),intent(in) :: i
        integer,intent(in) :: s
        character(len=*),intent(in) :: st
        if (size(i).ne.s) then
          write(*,*) 'Error: bad index size match in ',st,' in SF_base.f90'
          write(*,*) 'i = ',i
          write(*,*) 's = ',s
          stop 'Done'
        endif
      end subroutine

      subroutine delete_SF(v)
        implicit none
        type(SF),intent(inout) :: v
        if (allocated(v%f)) deallocate(v%f)
        v%s = 0
      end subroutine

      subroutine print_SF(v)
        implicit none
        type(SF),intent(in) :: v
        call export(v,6)
      end subroutine

      subroutine export_SF(v,un)
        implicit none
        type(SF),intent(in) :: v
        integer,intent(in) :: un
        integer :: j
#ifdef _DEBUG_SF_
        call check_allocated(v,'export_SF')
#endif
        write(un,*) 'v%s = ',v%s
        do j=1,v%s; write(un,*) 'v%f(',j,') = ',v%f(j); enddo
      end subroutine

      end module