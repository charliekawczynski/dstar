      module index_mapping_mod
      implicit none

      private
      public :: index_1D,index_3D

      contains

      function index_1D(i,j,k,imax,jmax) result(t)
        ! Notes:
        !     For i=1,j=1,k=1 we have
        !     m = 1 + im*(0 + 0) = 1
        !     For i=im,j=jm,k=km we have
        !     m = im + im*((jm-1) + jm*(km-1))
        !       = im + im*jm - im + im*jm*(km-1)
        !       =      im*jm      + im*jm*km-im*jm
        !       =                 + im*jm*km
        !     Which should equal
        !     m = im*jm*km
        implicit none
        integer,intent(in) :: i,j,k,imax,jmax
        integer :: t
        t = i + imax*( (j-1) + jmax*(k-1) )
      end function

      subroutine index_3D(i_3D,j_3D,k_3D,imax,jmax,t)
        implicit none
        integer,intent(inout) :: i_3D,j_3D,k_3D
        integer,intent(in) :: imax,jmax,t
        k_3D = (t-1)/(imax*jmax)+1
        j_3D = ((t-1) - ((k_3D-1)*imax*jmax))/imax+1
        i_3D = t - (j_3D-1)*imax - (k_3D-1)*imax*jmax
      end subroutine

      end module