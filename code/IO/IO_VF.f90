      module IO_VF_mod
      use current_precision_mod
      use IO_tools_mod
      use exp_Tecplot_Header_mod
      use mesh_mod
      use VI_mod
      use VF_mod
      implicit none

      private
      public :: export

      interface export;  module procedure export_VF;            end interface
      interface export;  module procedure export_mesh_new;      end interface

      contains

      subroutine export_VF(m,v,i_index,dir,name)
        implicit none
        type(mesh),intent(in) :: m
        type(VF),intent(in) :: v
        type(VI),intent(in) :: i_index
        character(len=*),intent(in) :: dir,name
        integer :: i,j,k,un
        un = newAndOpen(dir,name)
        write(un,*) 'TITLE = "'//name//'_VF"'
        write(un,*) 'VARIABLES = "x", "y", "z", "'//name//'"'

        ! This is bad practice, but works...
        do j=1,i_index%s
        i = i_index%i(j)
        write(un,*) 'ZONE N=8, E=1, DATAPACKING=BLOCK, ZONETYPE=FEBRICK'
        write(un,'(8E23.12E3)') (/(m%c(i)%n(k)%h(1),k=1,8)/)
        write(un,'(8E23.12E3)') (/(m%c(i)%n(k)%h(2),k=1,8)/)
        write(un,'(8E23.12E3)') (/(m%c(i)%n(k)%h(3),k=1,8)/)
        write(un,'(1E23.12E3)') (/(v%f(i),k=1,8)/)
        ! write(un,'(15A)') '# Connectivity:'
        write(un,'(8I10)') (/1,2,7,3,4,6,8,5/)
        write(un,*) ''
        enddo
        close(un)
      end subroutine

      subroutine export_mesh_new(m,x,y,z,v,CN_index,dir,name)
        ! Some code here was adopted (and modified) from
        ! http://ossanworld.com/cfdbooks/cfdcodes/hexgrid_cube_v3.f90
        implicit none
        type(mesh),intent(in) :: m
        type(VF),intent(in) :: x,y,z
        type(VF),intent(in) :: v
        type(VI),intent(in) :: CN_index
        character(len=*),intent(in) :: dir,name
        integer :: i,j,k,un,nhex
        integer,dimension(8) :: hex_loc,a_loc
        integer,dimension(:,:),allocatable :: hex
        un = newAndOpen(dir,name)
        nhex  = m%total%C%s
        allocate( hex(nhex,8) )
        nhex = 0
        a_loc = (/1,2,7,3,4,6,8,5/)
        do j=1,m%total%C%s
          i = j
          hex_loc = (/(m%c(i)%n(a_loc(k))%ID,k=1,8)/)
          do k=1,8; hex(i,k) = hex_loc(k); enddo
        enddo

        write(un,*) 'TITLE = "'//name//'_mesh"'
        write(un,*) 'VARIABLES = "x", "y", "z"'
        write(un,*) 'ZONE  N=', m%total%N%s,',E=', m%total%C%s,' , ET=BRICK,  F=FEPOINT'

        write(*,*) 'm%total%N%s = ',m%total%N%s
        do j=1,m%total%N%s
          write(un,'(8E23.12E3)') (/x%f(j),y%f(j),z%f(j)/)
        end do
        write(un,*) ''
        do i=1,m%total%C%s
         write(un,'(8i10)') (/(hex(i,j),j=1,8)/)
        enddo
        deallocate(hex)
        close(un)
      end subroutine

      !       subroutine export_VF_old(m,x,y,z,v,dir,name)
      !         implicit none
      !         type(mesh),intent(in) :: m
      !         type(VI),intent(in) :: x,y,z
      !         type(VF),intent(in) :: v
      !         character(len=*),intent(in) :: dir,name
      !         integer :: i,j,un,nhex
      !         integer,dimension(8) :: hex_loc
      !         integer,dimension(:,:),allocatable :: hex
      !         un = newAndOpen(dir,name)
      !         nhex  = m%total%C%s
      !         allocate( hex(nhex,8) )
      !         nhex = 0
      !         do i=1,m%total%C%s; do j=1,8
      !           hex_loc(j) = m%c(i)%n(j)%ID
      !           hex(i,j) = hex_loc(j)
      !         enddo; enddo
      !         write(un,*) 'TITLE = "'//name//'_VF"'
      !         write(un,*) 'VARIABLES = "x", "y", "z", "'//name//'"'
      !         write(un,*) 'zone  n=', m%total%N%s,',e=', m%total%C%s,' , et=brick,  f=fepoint'
      !         do j=1,m%total%N%s; write(un,'(8E23.12E3)') (/x%i(j),y%i(j),z%i(j)/); end do
      !         write(un,*) ''
      !         ! do j=1,m%total%N%s; write(un,'(8E23.12E3)') v%f(j); enddo
      !         do i=1,m%total%C%s
      !          write(un,'(8i10)') (/(hex(i,j),j=1,8)/)
      !         enddo
      !         close(un)
      !       end subroutine

      end module