      module BCT_mod
      ! Boundary Condition Index
      use current_precision_mod
      use IS_base_mod
      implicit none

      private
      public :: BCT

      type BCT
        type(IS) :: D,N,P,R
      end type

      contains

      end module