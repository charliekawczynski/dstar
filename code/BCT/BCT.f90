      module BCT_mod
      ! Boundary Condition Type
      use current_precision_mod
      use GI_base_mod
      implicit none

      private
      public :: BCT

      type BCT
        type(GI) :: D,N,P,R
      end type

      contains

      end module