      module IT_mod
      use DI_mod
      implicit none

      private
      public :: IT

      type IT
        type(DI) :: total,interior,boundary,ghost
      end type

      end module