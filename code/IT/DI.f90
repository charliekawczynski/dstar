      module DI_mod
      use LI_mod
      implicit none

      private
      public :: DI

      type DI
        type(LI) :: C,N,F,E
      end type

      end module