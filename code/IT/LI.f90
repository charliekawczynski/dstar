      module LI_mod
      use IS_OMP_mod
      use BCT_mod
      implicit none

      private
      public :: LI

      type LI
        type(IS_OMP) :: i
        type(IS_OMP),dimension(3) :: A
        type(BCT) :: B
      end type

      end module