      module IS_OMP_mod
      ! Compiler flags: (_DEBUG_IS_OMP_,_PARALLELIZE_IS_OMP_)
      use current_precision_mod
      implicit none

      private
      public :: IS_OMP
      public :: init,delete,export,import,display,print         ! Essentials

      public :: assign ! ,add,subtract,multiply,square,add_product ! Fixed rank math operations

      public :: minval,maxval,minloc,maxloc                     ! Varied rank math operations
      public :: minabs,maxabs,minabsloc,maxabsloc               ! Varied rank math operations

      public :: swap,size                                       ! Auxiliary operations

      public :: insist_allocated,insist_bounds                  ! Stops execution
      ! public :: check_allocated,check_bounds                  ! Provides warning

      type IS_OMP
        private ! DO NOT MAKE PROPERTIES PUBLIC, PRESERVE ENCAPSULATION
        integer :: N
        integer,dimension(:),allocatable :: s
      end type

      interface init;              module procedure init_size;            end interface
      interface init;              module procedure init_array;           end interface
      interface init;              module procedure init_copy;            end interface
      interface delete;            module procedure delete_IS;            end interface
      interface export;            module procedure export_IS;            end interface
      interface import;            module procedure import_IS;            end interface
      interface print;             module procedure print_IS;             end interface
      interface display;           module procedure display_IS;           end interface

      interface assign;            module procedure assign_IS_C_i;        end interface
      interface assign;            module procedure assign_IS_C_all;      end interface
      interface assign;            module procedure assign_IS_IS;         end interface
      interface assign;            module procedure assign_IS_element;    end interface
      interface assign;            module procedure assign_IS_IS_i_i;     end interface
      interface assign;            module procedure assign_IS_IS_all;     end interface

      interface size;              module procedure size_IS;              end interface

      interface minval;            module procedure minval_IS_i;          end interface
      interface minval;            module procedure minval_IS_all;        end interface
      interface maxval;            module procedure maxval_IS_i;          end interface
      interface maxval;            module procedure maxval_IS_all;        end interface
      interface minloc;            module procedure minloc_IS_i;          end interface
      interface minloc;            module procedure minloc_IS_all;        end interface
      interface maxloc;            module procedure maxloc_IS_i;          end interface
      interface maxloc;            module procedure maxloc_IS_all;        end interface
      interface minabs;            module procedure minabs_IS_i;          end interface
      interface minabs;            module procedure minabs_IS_all;        end interface
      interface maxabs;            module procedure maxabs_IS_i;          end interface
      interface maxabs;            module procedure maxabs_IS_all;        end interface
      interface minabsloc;         module procedure minabsloc_IS_i;       end interface
      interface minabsloc;         module procedure minabsloc_IS_all;     end interface
      interface maxabsloc;         module procedure maxabsloc_IS_i;       end interface
      interface maxabsloc;         module procedure maxabsloc_IS_all;     end interface

      interface swap;              module procedure swap_IS;              end interface

      interface insist_bounds;     module procedure insist_bounds_IS;     end interface
      interface insist_allocated;  module procedure insist_allocated_IS;  end interface

      contains

      ! **************************************************************
      ! ************************* ESSENTIALS *************************
      ! **************************************************************

      subroutine init_size(v,N)
        implicit none
        type(IS_OMP),intent(inout) :: v
        integer,intent(in) :: N
#ifdef _DEBUG_IS_OMP_
        if (N.lt.1) then
          write(*,*) 'Error: cannot init array of size<1 in init_size in IS_OMP.f90'
          write(*,*) 'N = ',N
          stop 'Done'
        endif
#endif
        call delete(v)
        v%N = N
        allocate(v%s(N))
      end subroutine

      subroutine init_array(v,s,N)
        implicit none
        type(IS_OMP),intent(inout) :: v
        integer,dimension(N),intent(in) :: s
        integer,intent(in) :: N
#ifdef _DEBUG_IS_OMP_
        if (size(s).ne.N) stop 'Error: size(s) mismatch in init_size in IS_OMP.f90'
        call insist_allocated(v,'init_array')
#endif
        call init(v,N)
        v%s = s
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(IS_OMP),intent(inout) :: a
        type(IS_OMP),intent(in) :: b
#ifdef _DEBUG_IS_OMP_
        call insist_allocated(b,'init_copy')
#endif
        call delete(a)
        allocate(a%s(b%N))
        a%N = b%N
        a%s = b%s
      end subroutine

      subroutine delete_IS(v)
        implicit none
        type(IS_OMP),intent(inout) :: v
        if (allocated(v%s)) deallocate(v%s)
        v%N = 0
      end subroutine

      subroutine export_IS(v,un)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer,intent(in) :: un
        integer :: j
#ifdef _DEBUG_IS_OMP_
        call insist_allocated(v,'export_IS')
#endif
        write(un,*) 'v%N = '; write(un,*) v%N
        do j=1,v%N; write(un,*) v%s(j); enddo
      end subroutine

      subroutine import_IS(v,un)
        implicit none
        type(IS_OMP),intent(inout) :: v
        integer,intent(in) :: un
        integer :: j,N
        call delete(v)
        read(un,*); read(un,*) N
        call init(v,N)
        do j=1,v%N; read(un,*) v%s(j); enddo
      end subroutine

      subroutine display_IS(v,un)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer,intent(in) :: un
        integer :: j
#ifdef _DEBUG_IS_OMP_
        call insist_allocated(v,'display_IS')
#endif
        write(un,*) 'v%N = ',v%N
        do j=1,v%N; write(un,*) 'v(',j,') = ',v%s(j); enddo
      end subroutine

      subroutine print_IS(v)
        implicit none
        type(IS_OMP),intent(in) :: v
        call display(v,6)
      end subroutine

      ! **************************************************************
      ! ***************** FIXED RANK MATH OPERATIONS *****************
      ! **************************************************************

      subroutine assign_IS_C_i(A,B,s,N)
        implicit none
        type(IS_OMP),intent(inout) :: A
        integer,intent(in) :: B
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: s
        integer :: j
#ifdef _DEBUG_IS_OMP_
          call insist_allocated(A,'assign_IS_C_i')
          call insist_bounds(A,minval(s),maxval(s),'assign_IS_C_i')
#endif
#ifdef _PARALLELIZE_IS_OMP_
        !$OMP PARALLEL DO

#endif

        do j=1,N; A%s(s(j)) = B; enddo
#ifdef _PARALLELIZE_IS_OMP_
        !$OMP END PARALLEL DO

#endif
      end subroutine

      subroutine assign_IS_C_all(A,B)
        implicit none
        type(IS_OMP),intent(inout) :: A
        integer,intent(in) :: B
        integer :: j
#ifdef _DEBUG_IS_OMP_
          call insist_allocated(A,'assign_IS_C')
#endif
#ifdef _PARALLELIZE_IS_OMP_
        !$OMP PARALLEL DO

#endif

        do j=1,A%N; A%s(j) = B; enddo
#ifdef _PARALLELIZE_IS_OMP_
        !$OMP END PARALLEL DO

#endif
      end subroutine

      subroutine assign_IS_element(A,B,i)
        implicit none
        type(IS_OMP),intent(inout) :: A
        integer,intent(in) :: B
        integer,intent(in) :: i
#ifdef _DEBUG_IS_OMP_
        call insist_allocated(A,'assign_IS_element (A)')
        call insist_bounds(A,i,i,'assign_IS_element')
#endif
        A%s(i) = B
      end subroutine

      subroutine assign_IS_IS(A,B,s,N)
        implicit none
        type(IS_OMP),intent(inout) :: A
        type(IS_OMP),intent(in) :: B
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: s
        integer :: j
#ifdef _DEBUG_IS_OMP_
          call insist_allocated(A,'assign_IS_IS (A)')
          call insist_allocated(B,'assign_IS_IS (B)')
          call insist_bounds(A,minval(s),maxval(s),'assign_IS_IS (A)')
          call insist_bounds(B,minval(s),maxval(s),'assign_IS_IS (B)')
#endif
#ifdef _PARALLELIZE_IS_OMP_
        !$OMP PARALLEL DO

#endif

        do j=1,N; A%s(s(j)) = B%s(s(j)); enddo
#ifdef _PARALLELIZE_IS_OMP_
        !$OMP END PARALLEL DO

#endif
      end subroutine

      subroutine assign_IS_IS_i_i(A,B,i_A,i_B,N)
        implicit none
        type(IS_OMP),intent(inout) :: A
        type(IS_OMP),intent(in) :: B
        integer,dimension(N),intent(in) :: i_A,i_B
        integer,intent(in) :: N
        integer :: j
#ifdef _DEBUG_IS_OMP_
          call insist_allocated(A,'assign_IS_IS_i_i (A)')
          call insist_allocated(B,'assign_IS_IS_i_i (B)')
          call insist_bounds(A,minval(i_A),maxval(i_A),'assign_IS_IS_i_i (A)')
          call insist_bounds(B,minval(i_B),maxval(i_B),'assign_IS_IS_i_i (B)')
#endif
#ifdef _PARALLELIZE_IS_OMP_
        !$OMP PARALLEL DO

#endif

        do j=1,N; A%s(i_A(j)) = B%s(i_B(j)); enddo
#ifdef _PARALLELIZE_IS_OMP_
        !$OMP END PARALLEL DO

#endif
      end subroutine

      subroutine assign_IS_IS_all(A,B)
        implicit none
        type(IS_OMP),intent(inout) :: A
        type(IS_OMP),intent(in) :: B
        integer :: j
#ifdef _DEBUG_IS_OMP_
          call insist_allocated(A,'assign_IS_IS_all (A)')
          call insist_allocated(B,'assign_IS_IS_all (B)')
#endif
#ifdef _PARALLELIZE_IS_OMP_
        !$OMP PARALLEL DO

#endif

        do j=1,B%N; A%s(j) = B%s(j); enddo
#ifdef _PARALLELIZE_IS_OMP_
        !$OMP END PARALLEL DO

#endif
      end subroutine

      ! **************************************************************
      ! *************** CHANGING RANK MATH OPERATIONS ****************
      ! **************************************************************

      pure function size_IS(v) result(N)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer :: N
        N = v%N
      end function

      pure function minval_IS_i(v,s,N) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: s
        integer :: m
        m = minval(v%s(s))
      end function

      pure function minval_IS_all(v) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer :: m
        m = minval(v%s)
      end function

      pure function maxval_IS_i(v,s,N) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: s
        integer :: m
        m = maxval(v%s(s))
      end function

      pure function maxval_IS_all(v) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer :: m
        m = maxval(v%s)
      end function

      pure function minloc_IS_i(v,s,N) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: s
        integer :: m
        m = minloc(v%s(s),1)
      end function

      pure function minloc_IS_all(v) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer :: m
        m = minloc(v%s,1)
      end function

      pure function maxloc_IS_i(v,s,N) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: s
        integer :: m
        m = maxloc(v%s(s),1)
      end function

      pure function maxloc_IS_all(v) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer :: m
        m = maxloc(v%s,1)
      end function

      pure function minabs_IS_i(v,s,N) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: s
        integer :: m
        m = minval(abs(v%s(s)))
      end function

      pure function minabs_IS_all(v) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer :: m
        m = minval(abs(v%s))
      end function

      pure function maxabs_IS_i(v,s,N) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: s
        integer :: m
        m = maxval(abs(v%s(s)))
      end function

      pure function maxabs_IS_all(v) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer :: m
        m = maxval(abs(v%s))
      end function

      pure function minabsloc_IS_i(v,s,N) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: s
        integer :: m
        m = minloc(abs(v%s(s)),1)
      end function

      pure function minabsloc_IS_all(v) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer :: m
        m = minloc(abs(v%s),1)
      end function

      pure function maxabsloc_IS_i(v,s,N) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: s
        integer :: m
        m = maxloc(abs(v%s(s)),1)
      end function

      pure function maxabsloc_IS_all(v) result(m)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer :: m
        m = maxloc(abs(v%s),1)
      end function

      ! **************************************************************
      ! ******************** AUXILIARY OPERATIONS ********************
      ! **************************************************************

      subroutine swap_IS(v,i,j)
        implicit none
        type(IS_OMP),intent(inout) :: v
        integer,intent(in) :: i,j
        integer :: temp
#ifdef _DEBUG_IS_OMP_
        call insist_allocated(v,'swap_IS')
        call insist_bounds(v,i,i,'swap_IS i')
        call insist_bounds(v,j,j,'swap_IS j')
#endif
        temp = v%s(i)
        v%s(i) = v%s(j)
        v%s(j) = temp
      end subroutine

      ! **************************************************************
      ! ******************** DEBUGGING OPERATIONS ********************
      ! **************************************************************

      subroutine insist_allocated_IS(v,caller)
        implicit none
        type(IS_OMP),intent(in) :: v
        character(len=*),intent(in) :: caller
        if (.not.allocated(v%s)) then
          write(*,*) 'Error: need allocated IS_OMP in ',caller,' in IS_OMP.f90'
          stop 'Done'
        endif
      end subroutine

      subroutine insist_bounds_IS(v,imin,imax,caller)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer,intent(in) :: imin,imax
        character(len=*),intent(in) :: caller
        logical,dimension(2) :: L
        call insist_allocated(v,caller)
        L(1) = index_inside_bounds(v,imin)
        L(2) = index_inside_bounds(v,imax)
        if (.not.(L(1).and.L(2))) then
          write(*,*) 'Error: IS_OMP index bound violated in ',caller,' in IS_OMP.f90'
          write(*,*) 'L = ',L
          write(*,*) 'imin = ',imin
          write(*,*) 'imax = ',imax
          write(*,*) 'v%N = ',v%N
          stop 'Done'
        endif
      end subroutine

      function index_inside_bounds(v,i) result (L)
        implicit none
        type(IS_OMP),intent(in) :: v
        integer,intent(in) :: i
        logical :: L
        L = (i.ge.1).and.(i.le.v%N)
      end function

      end module