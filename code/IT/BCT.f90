      module BCT_mod
      use IS_OMP_mod
      implicit none

      private
      public :: BCT

      type BCT
        logical :: defined
        type(IS_OMP),dimension(3) :: D,N,P,R,PM
      end type

      end module