      module vec_mod
      ! Compiler flags: (_DEBUG_VEC_,_PARALLELIZE_VEC_)
      use current_precision_mod; use vec_OMP_mod
      implicit none
      character(len=11) :: this_file = 'vec.f90'
#ifdef _USE_
      use mpi
#endif

      private
      public :: vec
      public :: init,delete,export,import,display,print         ! Essentials

      public :: assign ! ,add,subtract,multiply,square,add_product ! Fixed rank math operations

      public :: size                                            ! Auxiliary
      public :: insist_allocated,insist_bounds                  ! Stops execution
      ! public :: check_allocated,check_bounds                  ! Provides warning

      type vec
        private
        integer :: N
        type(vec_OMP),dimension(:),allocatable :: v ! 0:N, since processor starts at 0
      end type

      interface init;              module procedure init_size;             end interface
      interface init;              module procedure init_size_i;           end interface
      interface init;              module procedure init_array_i;          end interface
      interface init;              module procedure init_copy;             end interface
      interface delete;            module procedure delete_vec;            end interface
      interface export;            module procedure export_vec;            end interface
      interface import;            module procedure import_vec;            end interface
      interface print;             module procedure print_vec;             end interface
      interface display;           module procedure display_vec;           end interface

      interface assign;            module procedure assign_vec_C_all;      end interface
      interface assign;            module procedure assign_vec_element;    end interface
      interface assign;            module procedure assign_vec_vec_all;    end interface

      interface size;              module procedure size_vec;          end interface

      interface insist_bounds;     module procedure insist_bounds_vec;     end interface
      interface insist_allocated;  module procedure insist_allocated_vec;  end interface

      contains

      ! **************************************************************
      ! ************************* ESSENTIALS *************************
      ! **************************************************************

      subroutine init_size(v,N)
        implicit none
        type(vec),intent(inout) :: v
        integer,intent(in) :: N
#ifdef _DEBUG_VEC_
        if (N.lt.1) then
          write(*,*) 'Error: cannot init array of size<1 in init_size in ',this_file
          write(*,*) 'N = ',N
          stop 'Done'
        endif
#endif
        call delete(v)
        v%N = N
        allocate(v%v(1:N))
        write(*,*) 'Initilized '
      end subroutine

      subroutine init_size_i(v,N,i)
        implicit none
        type(vec),intent(inout) :: v
        integer,intent(in) :: N,i
#ifdef _DEBUG_VEC_
        call insist_allocated(v,'init_size_i')
        call insist_bounds(v,i,i,'init_size_i')
#endif
        call init(v%v(i),N)
      end subroutine

      subroutine init_array_i(v,f,N,i)
        implicit none
        type(vec),intent(inout) :: v
        integer,intent(in) :: N,i
        real(cp),dimension(N),intent(in) :: f
#ifdef _DEBUG_VEC_
        call insist_allocated(v,'init_array')
        call insist_bounds(v,i,i,'init_size_i')
#endif
        call init(v%v(i),f,N)
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(vec),intent(inout) :: a
        type(vec),intent(in) :: b
        integer :: i
#ifdef _DEBUG_VEC_
        call insist_allocated(b,'init_copy')
#endif
        call delete(a)
        allocate(a%v(size(b)))
        do i=1,size(b)
          call init(a%v(i),b%v(i))
        enddo
        a%N = b%N
      end subroutine

      subroutine delete_vec(v)
        implicit none
        type(vec),intent(inout) :: v
        integer :: i
        if (allocated(v%v)) then
          do i=1,v%N; call delete(v%v(i)); enddo
        endif
        if (allocated(v%v)) deallocate(v%v)
        v%N = 0
      end subroutine

      subroutine export_vec(v,un)
        implicit none
        type(vec),intent(in) :: v
        integer,intent(in) :: un
        integer :: i
#ifdef _DEBUG_VEC_
        call insist_allocated(v,'export_vec')
#endif
        write(un,*) 'v%N = '
        write(un,*) v%N
        do i=1,v%N; call export(v,un); enddo
      end subroutine

      subroutine import_vec(v,un)
        implicit none
        type(vec),intent(inout) :: v
        integer,intent(in) :: un
        integer :: i,N
        call delete(v)
#ifdef _DEBUG_VEC_
        ! call insist_valid_unit(un)

#endif
        read(un,*); read(un,*) N
        call init(v,N)
        do i=1,v%N; call import(v,un); enddo
      end subroutine

      subroutine display_vec(v,un)
        implicit none
        type(vec),intent(in) :: v
        integer,intent(in) :: un
        integer :: j
#ifdef _DEBUG_VEC_
        call insist_allocated(v,'display_vec')
#endif
        write(un,*) 'v%N = ',v%N
        do j=1,v%N; call display(v%v(j),un); enddo
      end subroutine

      subroutine print_vec(v)
        implicit none
        type(vec),intent(in) :: v
        call display(v,6)
      end subroutine

      ! **************************************************************
      ! ***************** FIXED RANK MATH OPERATIONS *****************
      ! **************************************************************

      subroutine assign_vec_C_all(A,B)
        implicit none
        type(vec),intent(inout) :: A
        real(cp),intent(in) :: B
        integer :: j
#ifdef _DEBUG_VEC_
        call insist_allocated(A,'assign_vec_C_all')
#endif
        do j=1,A%N; call assign(A%v(j),B); enddo
      end subroutine

      subroutine assign_vec_element(A,B,i)
        implicit none
        type(vec),intent(inout) :: A
        real(cp),intent(in) :: B
        integer,intent(in) :: i
#ifdef _DEBUG_VEC_
        call insist_allocated(A,'assign_vec_element (A)')
        call insist_bounds(A,i,i,'assign_vec_element')
#endif
        call assign(A%v(i),B)
      end subroutine

      subroutine assign_vec_vec_all(A,B)
        implicit none
        type(vec),intent(inout) :: A
        type(vec),intent(in) :: B
        integer :: j
#ifdef _DEBUG_VEC_
        call insist_allocated(A,'assign_vec_vec_all (A)')
        call insist_allocated(B,'assign_vec_vec_all (B)')
#endif
        do j=1,A%N; call assign(A%v(j),B%v(j)); enddo
      end subroutine

      ! **************************************************************
      ! *************** CHANGING RANK MATH OPERATIONS ****************
      ! **************************************************************

      ! **************************************************************
      ! ******************** AUXILIARY OPERATIONS ********************
      ! **************************************************************

      pure function size_vec(v) result(N)
        implicit none
        type(vec),intent(in) :: v
        integer :: N
        N = v%N
      end function


      ! **************************************************************
      ! ******************** DEBUGGING OPERATIONS ********************
      ! **************************************************************

      subroutine insist_allocated_vec(v,caller)
        implicit none
        type(vec),intent(in) :: v
        character(len=*),intent(in) :: caller
        if (.not.allocated(v%v)) then
          write(*,*) 'Error: need allocated vec in ',caller,' in ',this_file
          stop 'Done'
        endif
      end subroutine

      subroutine insist_bounds_vec(v,imin,imax,caller)
        implicit none
        type(vec),intent(in) :: v
        integer,intent(in) :: imin,imax
        character(len=*),intent(in) :: caller
        logical,dimension(2) :: L
        call insist_allocated(v,caller)
        L(1) = index_inside_bounds(v,imin)
        L(2) = index_inside_bounds(v,imax)
        if (.not.(L(1).and.L(2))) then
          write(*,*) 'Error: vec index bound violated in ',caller,' in ',this_file
          write(*,*) 'L = ',L
          write(*,*) 'imin = ',imin
          write(*,*) 'imax = ',imax
          write(*,*) 'v%N = ',v%N
          stop 'Done'
        endif
      end subroutine

      function index_inside_bounds(v,i) result (L)
        implicit none
        type(vec),intent(in) :: v
        integer,intent(in) :: i
        logical :: L
        L = (i.ge.1).and.(i.le.v%N)
      end function

      end module