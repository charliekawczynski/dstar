      module vec_mod
      ! Compiler flags: (_DEBUG_VEC_,_PARALLELIZE_VEC_)
      use current_precision_mod
      use datatype_conversion_mod
      use IO_tools_mod
      use string_mod
      use vec_OMP_mod
      use mpi
      use mpi_obj_mod
      use index_mapping_mod
      implicit none

      private
      public :: vec
      public :: init,delete,export,import,display,print            ! Essentials

      public :: assign ! ,add,subtract,multiply,square,add_product ! Fixed rank math operations

      public :: push

      type vec
        private
        type(string) :: dir,name
        type(mpi_obj) :: m
        type(vec_OMP) :: v
      end type

      interface init;              module procedure init_size_mpi;         end interface
      interface init;              module procedure init_size_proc;        end interface
      interface init;              module procedure init_size_all;         end interface
      interface init;              module procedure init_array_i;          end interface
      interface init;              module procedure init_copy;             end interface
      interface delete;            module procedure delete_vec;            end interface
      interface export;            module procedure export_vec;            end interface
      interface import;            module procedure import_vec;            end interface
      interface print;             module procedure print_vec;             end interface
      interface display;           module procedure display_vec;           end interface

      interface assign;            module procedure assign_vec_C_all;      end interface
      interface assign;            module procedure assign_vec_element;    end interface
      interface assign;            module procedure assign_vec_proc;       end interface
      interface assign;            module procedure assign_vec_vec_all;    end interface

      interface push;              module procedure push_vec_AB;           end interface

      contains

      ! **************************************************************
      ! ************************* ESSENTIALS *************************
      ! **************************************************************

      subroutine init_size_mpi(v,m,dir,name)
        implicit none
        type(vec),intent(inout) :: v
        type(mpi_obj),intent(in) :: m
        character(len=*),intent(in) :: dir,name
        call init(v%m,m)
        call init(v%dir,dir)
        call init(v%name,name)
      end subroutine

      subroutine init_size_proc(v,proc,N)
        implicit none
        type(vec),intent(inout) :: v
        integer,intent(in) :: proc,N
        if (processor(v%m,proc)) call init(v%v,N)
      end subroutine

      subroutine push_vec_AB(v,A,B) ! pushes data from A to B
        implicit none
        type(vec),intent(inout) :: v
        integer,intent(in) :: A,B
        integer :: status,ierr
        if (processor(v%m,B)) call mpi_send(v%v%f,size(v%v),get_cp(v%m),A-1,tag(v%m),MCW(v%m),status,ierr)
        if (processor(v%m,A)) call mpi_recv(v%v%f,size(v%v),get_cp(v%m),B-1,tag(v%m),MCW(v%m),status,ierr)
        call mpi_barrier(MCW(v%m),ierr)
      end subroutine

      ! subroutine stitch_vec_all(v)
      !   implicit none
      !   type(vec),intent(inout) :: v
      !   integer :: i,message_tag,status,ierr
      !   do i=1,size(v%m); do j=1,i
      !     if (ID(v%m).eq.j) call mpi_send(v%v,a%N,cp,i,message_tag,MCW(v%m),status,ierr)
      !     if (ID(v%m).eq.i) call mpi_recv(v%v,a%N,cp,j,message_tag,MCW(v%m),status,ierr)
      !   enddo; enddo
      ! end subroutine

      subroutine init_size_all(v,N)
        implicit none
        type(vec),intent(inout) :: v
        integer,intent(in) :: N
        call init(v%v,N)
      end subroutine

      subroutine init_array_i(v,f,N,proc)
        implicit none
        type(vec),intent(inout) :: v
        integer,intent(in) :: N,proc
        real(cp),dimension(N),intent(in) :: f
        if (processor(v%m,proc)) call init(v%v,f,N)
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(vec),intent(inout) :: a
        type(vec),intent(in) :: b
        call init(a%v,b%v)
        call init(a%m,b%m)
      end subroutine

      subroutine delete_vec(v)
        implicit none
        type(vec),intent(inout) :: v
        call delete(v%v)
      end subroutine

      subroutine export_vec(v)
        implicit none
        type(vec),intent(in) :: v
        integer :: un
        integer :: proc,i_err
        call mpi_barrier(MCW(v%m),i_err)
        if (master(v%m)) then
          un = new_and_open(str(v%dir),str(v%name))
          call export(v%v,un,'proc = '//int2str(ID(v%m)))
          call close_and_message(un,str(v%dir),str(v%name))
        endif
        do proc=1,size(v%m)
          call mpi_barrier(MCW(v%m),i_err)
          if (processor(v%m,proc).and.(.not.(master(v%m)))) then
            un = open_to_append(str(v%dir),str(v%name))
            call export(v%v,un,'proc = '//int2str(ID(v%m)))
            call close_and_message(un,str(v%dir),str(v%name))
          endif
        enddo
        call mpi_barrier(MCW(v%m),i_err)
      end subroutine

      subroutine import_vec(v,un)
        implicit none
        type(vec),intent(inout) :: v
        integer,intent(in) :: un
        integer :: proc,i_err
        do proc=1,size(v%m)
          if (processor(v%m,proc)) call import(v%v,un)
          call mpi_barrier(MCW(v%m),i_err)
        enddo
      end subroutine

      subroutine display_vec(v,un)
        implicit none
        type(vec),intent(in) :: v
        integer,intent(in) :: un
        integer :: proc,i_err
        call mpi_barrier(MCW(v%m),i_err)
        do proc=1,size(v%m)
          call mpi_barrier(MCW(v%m),i_err)
          if (processor(v%m,proc)) call display(v%v,un)
        enddo
        call mpi_barrier(MCW(v%m),i_err)
      end subroutine

      subroutine print_vec(v)
        implicit none
        type(vec),intent(in) :: v
        call display(v,6)
      end subroutine

      ! **************************************************************
      ! ***************** FIXED RANK MATH OPERATIONS *****************
      ! **************************************************************

      subroutine assign_vec_C_all(A,B)
        implicit none
        type(vec),intent(inout) :: A
        real(cp),intent(in) :: B
        call assign(A%v,B)
      end subroutine

      subroutine assign_vec_element(A,B,proc,i)
        implicit none
        type(vec),intent(inout) :: A
        real(cp),intent(in) :: B
        integer,intent(in) :: proc,i
        if (processor(A%m,proc)) call assign(A%v,B,i)
      end subroutine

      subroutine assign_vec_proc(A,proc,B)
        implicit none
        type(vec),intent(inout) :: A
        integer,intent(in) :: proc
        real(cp),intent(in) :: B
        if (processor(A%m,proc)) call assign(A%v,B)
      end subroutine

      subroutine assign_vec_vec_all(A,B)
        implicit none
        type(vec),intent(inout) :: A
        type(vec),intent(in) :: B
        call assign(A%v,B%v)
      end subroutine

      ! **************************************************************
      ! *************** CHANGING RANK MATH OPERATIONS ****************
      ! **************************************************************

      ! **************************************************************
      ! ******************** AUXILIARY OPERATIONS ********************
      ! **************************************************************


      end module