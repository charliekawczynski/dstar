      module vec_OMP_mod
      ! Compiler flags: (_DEBUG_VEC_OMP_,_PARALLELIZE_VEC_OMP_)
      use current_precision_mod
      use mpi_obj_mod
      implicit none

      private
      public :: vec_OMP
      public :: init,delete,export,import,display,print         ! Essentials

      public :: assign ! ,add,subtract,multiply,square,add_product ! Fixed rank math operations

      public :: dot_product,sum,mean,size                       ! Varied rank math operations
      public :: minval,maxval,minloc,maxloc                     ! Varied rank math operations
      public :: minabs,maxabs,minabsloc,maxabsloc               ! Varied rank math operations

      public :: swap                                            ! Auxiliary operations

      public :: insist_allocated,insist_bounds                  ! Stops execution
      ! public :: check_allocated,check_bounds                  ! Provides warning

      type vec_OMP
        ! private ! DO NOT MAKE PROPERTIES PUBLIC, PRESERVE ENCAPSULATION
        integer :: N
        real(cp) :: N_r
        real(cp),dimension(:),allocatable :: f
      end type

      interface init;              module procedure init_size;             end interface
      interface init;              module procedure init_array;            end interface
      interface init;              module procedure init_copy;             end interface
      interface delete;            module procedure delete_vec;            end interface
      interface export;            module procedure export_vec;            end interface
      interface import;            module procedure import_vec;            end interface
      interface print;             module procedure print_vec;             end interface
      interface display;           module procedure display_vec;           end interface

      interface assign;            module procedure assign_vec_C_i;        end interface
      interface assign;            module procedure assign_vec_C_all;      end interface
      interface assign;            module procedure assign_vec_vec;        end interface
      interface assign;            module procedure assign_vec_element;    end interface
      interface assign;            module procedure assign_vec_vec_i_i;    end interface
      interface assign;            module procedure assign_vec_vec_all;    end interface

      interface dot_product;       module procedure dot_product_vec_i_i;   end interface
      interface dot_product;       module procedure dot_product_vec_i;     end interface
      interface dot_product;       module procedure dot_product_vec_all;   end interface
      interface sum;               module procedure sum_vec_i;             end interface
      interface sum;               module procedure sum_vec_all;           end interface
      interface mean;              module procedure mean_vec_i;            end interface
      interface mean;              module procedure mean_vec_all;          end interface
      interface size;              module procedure size_vec;              end interface

      interface minval;            module procedure minval_vec_i;          end interface
      interface minval;            module procedure minval_vec_all;        end interface
      interface maxval;            module procedure maxval_vec_i;          end interface
      interface maxval;            module procedure maxval_vec_all;        end interface
      interface minloc;            module procedure minloc_vec_i;          end interface
      interface minloc;            module procedure minloc_vec_all;        end interface
      interface maxloc;            module procedure maxloc_vec_i;          end interface
      interface maxloc;            module procedure maxloc_vec_all;        end interface
      interface minabs;            module procedure minabs_vec_i;          end interface
      interface minabs;            module procedure minabs_vec_all;        end interface
      interface maxabs;            module procedure maxabs_vec_i;          end interface
      interface maxabs;            module procedure maxabs_vec_all;        end interface
      interface minabsloc;         module procedure minabsloc_vec_i;       end interface
      interface minabsloc;         module procedure minabsloc_vec_all;     end interface
      interface maxabsloc;         module procedure maxabsloc_vec_i;       end interface
      interface maxabsloc;         module procedure maxabsloc_vec_all;     end interface

      interface swap;              module procedure swap_vec;              end interface

      interface insist_bounds;     module procedure insist_bounds_vec;     end interface
      interface insist_allocated;  module procedure insist_allocated_vec;  end interface

      contains

      ! **************************************************************
      ! ************************* ESSENTIALS *************************
      ! **************************************************************

      subroutine init_size(v,N)
        implicit none
        type(vec_OMP),intent(inout) :: v
        integer,intent(in) :: N
#ifdef _DEBUG_VEC_OMP_
        if (N.lt.1) then
          write(*,*) 'Error: cannot init array of size<1 in init_size in vec_OMP.f90'
          write(*,*) 'N = ',N
          stop 'Done'
        endif
#endif
        call delete(v)
        v%N = N
        v%N_r = real(N,cp)
        allocate(v%f(N))
        write(*,*) 'OMP field allocated to size ',N
      end subroutine

      subroutine init_array(v,f,N)
        implicit none
        type(vec_OMP),intent(inout) :: v
        real(cp),dimension(N),intent(in) :: f
        integer,intent(in) :: N
#ifdef _DEBUG_VEC_OMP_
        if (size(f).ne.N) then
          write(*,*) 'Error: size(f) mismatch in init_size in vec_OMP.f90'
          stop 'Done'
        endif
        call insist_allocated(v,'init_array')
#endif
        call init(v,N)
        ! call assign(v,f,v%N)
        v%f = f
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(vec_OMP),intent(inout) :: a
        type(vec_OMP),intent(in) :: b
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(b,'init_copy')
#endif
        call init(a,b%N)
        a%f = b%f
      end subroutine

      subroutine delete_vec(v)
        implicit none
        type(vec_OMP),intent(inout) :: v
        if (allocated(v%f)) deallocate(v%f)
        v%N = 0
        v%N_r = 0.0_cp
      end subroutine

      subroutine export_vec(v,un,message)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer,intent(in) :: un
        character(len=*),intent(in) :: message
        integer :: j
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(v,'export_vec')
#endif
        write(*,*) 'message = ',message
        call print(v)
        write(un,*) 'v%N = '; write(un,*) v%N
        do j=1,v%N; write(un,*) v%f(j); enddo
      end subroutine

      subroutine import_vec(v,un)
        implicit none
        type(vec_OMP),intent(inout) :: v
        integer,intent(in) :: un
        integer :: j,N
        call delete(v)
        read(un,*); read(un,*) N
        call init(v,N)
        do j=1,v%N; read(un,*) v%f(j); enddo
      end subroutine

      subroutine display_vec(v,un)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer,intent(in) :: un
        integer :: j
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(v,'display_vec')
#endif
        write(un,*) 'v%N = ',v%N
        do j=1,v%N; write(un,*) 'v(',j,') = ',v%f(j); enddo
      end subroutine

      subroutine print_vec(v)
        implicit none
        type(vec_OMP),intent(in) :: v
        call display(v,6)
      end subroutine

      ! **************************************************************
      ! ***************** FIXED RANK MATH OPERATIONS *****************
      ! **************************************************************

      subroutine assign_vec_C_i(A,B,i,N)
        implicit none
        type(vec_OMP),intent(inout) :: A
        real(cp),intent(in) :: B
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i
        integer :: j
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(A,'assign_vec_C_i')
        call insist_bounds(A,minval(i),maxval(i),'assign_vec_C_i')
#endif
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP PARALLEL DO

#endif

        do j=1,N; A%f(i(j)) = B; enddo
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP END PARALLEL DO

#endif
      end subroutine

      subroutine assign_vec_C_all(A,B)
        implicit none
        type(vec_OMP),intent(inout) :: A
        real(cp),intent(in) :: B
        integer :: j
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(A,'assign_vec_C')
#endif
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP PARALLEL DO

#endif

        do j=1,A%N; A%f(j) = B; enddo
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP END PARALLEL DO

#endif
      end subroutine

      subroutine assign_vec_element(A,B,i)
        implicit none
        type(vec_OMP),intent(inout) :: A
        real(cp),intent(in) :: B
        integer,intent(in) :: i
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(A,'assign_vec_element (A)')
        call insist_bounds(A,i,i,'assign_vec_element')
#endif
        A%f(i) = B
      end subroutine

      subroutine assign_vec_vec(A,B,i,N)
        implicit none
        type(vec_OMP),intent(inout) :: A
        type(vec_OMP),intent(in) :: B
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i
        integer :: j
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(A,'assign_vec_vec (A)')
        call insist_allocated(B,'assign_vec_vec (B)')
        call insist_bounds(A,minval(i),maxval(i),'assign_vec_vec (A)')
        call insist_bounds(B,minval(i),maxval(i),'assign_vec_vec (B)')
#endif
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP PARALLEL DO

#endif

        do j=1,N; A%f(i(j)) = B%f(i(j)); enddo
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP END PARALLEL DO

#endif
      end subroutine

      subroutine assign_vec_vec_i_i(A,B,i_A,i_B,N)
        implicit none
        type(vec_OMP),intent(inout) :: A
        type(vec_OMP),intent(in) :: B
        integer,dimension(N),intent(in) :: i_A,i_B
        integer,intent(in) :: N
        integer :: j
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(A,'assign_vec_vec_i_i (A)')
        call insist_allocated(B,'assign_vec_vec_i_i (B)')
        call insist_bounds(A,minval(i_A),maxval(i_A),'assign_vec_vec_i_i (A)')
        call insist_bounds(B,minval(i_B),maxval(i_B),'assign_vec_vec_i_i (B)')
#endif
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP PARALLEL DO

#endif

        do j=1,N; A%f(i_A(j)) = B%f(i_B(j)); enddo
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP END PARALLEL DO

#endif
      end subroutine

      subroutine assign_vec_vec_all(A,B)
        implicit none
        type(vec_OMP),intent(inout) :: A
        type(vec_OMP),intent(in) :: B
        integer :: j
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(A,'assign_vec_vec_all (A)')
        call insist_allocated(B,'assign_vec_vec_all (B)')
#endif
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP PARALLEL DO

#endif

        do j=1,B%N; A%f(j) = B%f(j); enddo
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP END PARALLEL DO

#endif
      end subroutine

      ! **************************************************************
      ! *************** CHANGING RANK MATH OPERATIONS ****************
      ! **************************************************************

      function dot_product_vec_i_i(A,B,i_A,i_B,N) result(dot)
        implicit none
        type(vec_OMP),intent(in) :: A,B
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i_A,i_B
        real(cp) :: dot,temp
        integer :: j
        temp = 0.0_cp
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(A,'dot_product_vec_i_i (A)')
        call insist_allocated(B,'dot_product_vec_i_i (B)')
#endif

#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP PARALLEL DO REDUCTION(+:temp)

#endif
        do j=1,N; temp = temp + A%f(i_A(j))*B%f(i_B(j)); enddo
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP END PARALLEL DO

#endif
        dot = temp
      end function

      function dot_product_vec_i(A,B,i,N) result(dot)
        implicit none
        type(vec_OMP),intent(in) :: A,B
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i
        real(cp) :: dot,temp
        integer :: j
        temp = 0.0_cp
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(A,'dot_product_vec_i (A)')
        call insist_allocated(B,'dot_product_vec_i (B)')
#endif

#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP PARALLEL DO REDUCTION(+:temp)

#endif
        do j=1,N; temp = temp + A%f(i(j))*B%f(i(j)); enddo
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP END PARALLEL DO

#endif
        dot = temp
      end function

      function dot_product_vec_all(A,B) result(dot)
        implicit none
        type(vec_OMP),intent(in) :: A,B
        real(cp) :: dot,temp
        integer :: j
        temp = 0.0_cp
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(A,'dot_product_vec_all (A)')
        call insist_allocated(B,'dot_product_vec_all (B)')
#endif

#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP PARALLEL DO REDUCTION(+:temp)

#endif
        do j=1,B%N; temp = temp + A%f(j)*B%f(j); enddo
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP END PARALLEL DO

#endif
        dot = temp
      end function

      function sum_vec_i(A,i,N) result(s)
        implicit none
        type(vec_OMP),intent(in) :: A
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i
        real(cp) :: s,temp
        integer :: j
        temp = 0.0_cp
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(A,'sum_vec_i (A)')
#endif

#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP PARALLEL DO REDUCTION(+:temp)

#endif
        do j=1,N; temp = temp + A%f(i(j)); enddo
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP END PARALLEL DO

#endif
        s = temp
      end function

      function sum_vec_all(A) result(s)
        implicit none
        type(vec_OMP),intent(in) :: A
        real(cp) :: s,temp
        integer :: j
        temp = 0.0_cp
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(A,'sum_vec_all (A)')
#endif

#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP PARALLEL DO REDUCTION(+:temp)

#endif
        do j=1,A%N; temp = temp + A%f(j); enddo
#ifdef _PARALLELIZE_VEC_OMP_
        !$OMP END PARALLEL DO

#endif
        s = temp
      end function

      function mean_vec_i(A,i,N) result(m)
        implicit none
        type(vec_OMP),intent(in) :: A
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i
        real(cp) :: m
        m = sum(A,i,N)/A%N_r
      end function

      function mean_vec_all(A) result(m)
        implicit none
        type(vec_OMP),intent(in) :: A
        real(cp) :: m
        m = sum(A)/A%N_r
      end function

      pure function size_vec(v) result(N)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer :: N
        N = v%N
      end function

      pure function minval_vec_i(v,i,N) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i
        real(cp) :: m
        m = minval(v%f(i))
      end function

      pure function minval_vec_all(v) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        real(cp) :: m
        m = minval(v%f)
      end function

      pure function maxval_vec_i(v,i,N) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i
        real(cp) :: m
        m = maxval(v%f(i))
      end function

      pure function maxval_vec_all(v) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        real(cp) :: m
        m = maxval(v%f)
      end function

      pure function minloc_vec_i(v,i,N) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i
        integer :: m
        m = minloc(v%f(i),1)
      end function

      pure function minloc_vec_all(v) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer :: m
        m = minloc(v%f,1)
      end function

      pure function maxloc_vec_i(v,i,N) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i
        integer :: m
        m = maxloc(v%f(i),1)
      end function

      pure function maxloc_vec_all(v) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer :: m
        m = maxloc(v%f,1)
      end function

      pure function minabs_vec_i(v,i,N) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i
        real(cp) :: m
        m = minval(abs(v%f(i)))
      end function

      pure function minabs_vec_all(v) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        real(cp) :: m
        m = minval(abs(v%f))
      end function

      pure function maxabs_vec_i(v,i,N) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i
        real(cp) :: m
        m = maxval(abs(v%f(i)))
      end function

      pure function maxabs_vec_all(v) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        real(cp) :: m
        m = maxval(abs(v%f))
      end function

      pure function minabsloc_vec_i(v,i,N) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i
        integer :: m
        m = minloc(abs(v%f(i)),1)
      end function

      pure function minabsloc_vec_all(v) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer :: m
        m = minloc(abs(v%f),1)
      end function

      pure function maxabsloc_vec_i(v,i,N) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i
        integer :: m
        m = maxloc(abs(v%f(i)),1)
      end function

      pure function maxabsloc_vec_all(v) result(m)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer :: m
        m = maxloc(abs(v%f),1)
      end function

      ! **************************************************************
      ! ******************** AUXILIARY OPERATIONS ********************
      ! **************************************************************

      subroutine swap_vec(v,i,j)
        implicit none
        type(vec_OMP),intent(inout) :: v
        integer,intent(in) :: i,j
        real(cp) :: temp
#ifdef _DEBUG_VEC_OMP_
        call insist_allocated(v,'swap_vec')
        call insist_bounds(v,i,i,'swap_vec i')
        call insist_bounds(v,j,j,'swap_vec j')
#endif
        temp = v%f(i)
        v%f(i) = v%f(j)
        v%f(j) = temp
      end subroutine

      ! **************************************************************
      ! ******************** DEBUGGING OPERATIONS ********************
      ! **************************************************************

      subroutine insist_allocated_vec(v,caller)
        implicit none
        type(vec_OMP),intent(in) :: v
        character(len=*),intent(in) :: caller
        if (.not.allocated(v%f)) then
          write(*,*) 'Error: need allocated vec_OMP in ',caller,' in vec_OMP.f90'
          stop 'Done'
        endif
      end subroutine

      subroutine insist_bounds_vec(v,imin,imax,caller)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer,intent(in) :: imin,imax
        character(len=*),intent(in) :: caller
        logical,dimension(2) :: L
        call insist_allocated(v,caller)
        L(1) = index_inside_bounds(v,imin)
        L(2) = index_inside_bounds(v,imax)
        if (.not.(L(1).and.L(2))) then
          write(*,*) 'Error: vec_OMP index bound violated in ',caller,' in vec_OMP.f90'
          write(*,*) 'L = ',L
          write(*,*) 'imin = ',imin
          write(*,*) 'imax = ',imax
          write(*,*) 'v%N = ',v%N
          stop 'Done'
        endif
      end subroutine

      function index_inside_bounds(v,i) result (L)
        implicit none
        type(vec_OMP),intent(in) :: v
        integer,intent(in) :: i
        logical :: L
        L = (i.ge.1).and.(i.le.v%N)
      end function

      end module