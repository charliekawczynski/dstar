      module sparse_mod
      ! Compiler flags: (_DEBUG_sparse_)
      use current_precision_mod
      implicit none

      private
      public :: sparse
      public :: init,delete,export,import,display,print         ! Essentials

      public :: assign ! ,add,subtract,multiply,square,add_product ! Fixed rank math operations

      public :: swap                                            ! Auxiliary operations

      public :: insist_allocated,insist_bounds                  ! Stops execution
      ! public :: check_allocated,check_bounds                  ! Provides warning

      type sparse
        private
        integer :: N
        type(vec_OMP),dimension(:),allocatable :: row
      end type

      interface init;              module procedure init_size;                end interface
      interface init;              module procedure init_array;               end interface
      interface init;              module procedure init_copy;                end interface
      interface delete;            module procedure delete_sparse;            end interface
      interface export;            module procedure export_sparse;            end interface
      interface import;            module procedure import_sparse;            end interface
      interface print;             module procedure print_sparse;             end interface
      interface display;           module procedure display_sparse;           end interface

      interface assign;            module procedure assign_sparse_C;          end interface
      interface assign;            module procedure assign_sparse_sparse;     end interface
      interface assign;            module procedure assign_sparse_element;    end interface
      interface assign;            module procedure assign_sparse_sparse_i_i; end interface
      interface assign;            module procedure assign_sparse_sparse_all; end interface

      interface swap;              module procedure swap_sparse;              end interface

      interface insist_bounds;     module procedure insist_bounds_sparse;     end interface
      interface insist_allocated;  module procedure insist_allocated_sparse;  end interface

      contains

      ! **************************************************************
      ! ************************* ESSENTIALS *************************
      ! **************************************************************

      subroutine init_size(s,N)
        implicit none
        type(sparse),intent(inout) :: s
        integer,intent(in) :: N
#ifdef _DEBUG_sparse_
        if (N.lt.1) then
          write(*,*) 'Error: cannot init array of size<1 in init_size in sparse_base.f90'
          write(*,*) 'N = ',N
          stop 'Done'
        endif
#endif
        call delete(s)
        s%N = N
        s%N_r = real(N,cp)
        allocate(s%row(N))
      end subroutine

      subroutine init_array(s,row,r,N)
        implicit none
        integer,intent(in) :: N
        type(sparse),intent(inout) :: s
        real(cp),dimension(N),intent(in) :: row
        integer,intent(in) :: r
#ifdef _DEBUG_sparse_
        if (size(row).ne.N) stop 'Error: size(row) mismatch in init_size in sparse_base.f90'
#endif
        call init(s,N)
        s%row(r) = row
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(sparse),intent(inout) :: a
        type(sparse),intent(in) :: b
        integer :: i
#ifdef _DEBUG_sparse_
        call insist_allocated(b,'init_copy')
#endif
        call delete(a)
        allocate(a%row(b%N))
        a%N = b%N
        a%N_r = b%N_r
        do i=1,N
          a%row(i) = b%row(i)
        enddo
      end subroutine

      subroutine delete_sparse(s)
        implicit none
        type(sparse),intent(inout) :: s
        if (allocated(s%row)) deallocate(s%row)
        s%N = 0
        s%N_r = 0.0_cp
      end subroutine

      subroutine export_sparse(s,un)
        implicit none
        type(sparse),intent(in) :: s
        integer,intent(in) :: un
        integer :: j
#ifdef _DEBUG_sparse_
        call insist_allocated(s,'export_sparse')
#endif
        write(un,*) 's%N = '; write(un,*) s%N
        do j=1,s%N; write(un,*) s%row(j); enddo
      end subroutine

      subroutine import_sparse(s,un)
        implicit none
        type(sparse),intent(inout) :: s
        integer,intent(in) :: un
        integer :: j,N
        call delete(s)
        read(un,*); read(un,*) N
        call init(s,N)
        do j=1,s%N; read(un,*) s%row(j); enddo
      end subroutine

      subroutine display_sparse(s,un)
        implicit none
        type(sparse),intent(in) :: s
        integer,intent(in) :: un
        integer :: j
#ifdef _DEBUG_sparse_
        call insist_allocated(s,'display_sparse')
#endif
        write(un,*) 's%N = ',s%N
        do j=1,s%N; write(un,*) 's(',j,') = ',s%row(j); enddo
      end subroutine

      subroutine print_sparse(s)
        implicit none
        type(sparse),intent(in) :: s
        call display(s,6)
      end subroutine

      ! **************************************************************
      ! ***************** FIXED RANK MATH OPERATIONS *****************
      ! **************************************************************

      subroutine assign_sparse_C(A,B,r)
        implicit none
        type(sparse),intent(inout) :: A
        real(cp),intent(in) :: B
        integer,intent(in) :: r
#ifdef _DEBUG_sparse_
          call insist_allocated(A,'assign_sparse_C')
          call insist_bounds(A,r,r,'assign_sparse_C')
#endif
        call assign(A%row(r),B)
      end subroutine

      subroutine assign_sparse_element(A,B,i,j)
        implicit none
        type(sparse),intent(inout) :: A
        real(cp),intent(in) :: B
        integer,intent(in) :: i,j
#ifdef _DEBUG_sparse_
        call insist_allocated(A,'assign_sparse_element (A)')
        call insist_bounds(A,i,i,'assign_sparse_element')
        call insist_bounds(A,j,j,'assign_sparse_element')
#endif
        call assign(A%row(i),B,j) ! Not sure which one...
        ! call assign(A%row(j),B,i) ! Not sure which one...
      end subroutine

      subroutine assign_sparse_sparse(A,B,i,N)
        implicit none
        type(sparse),intent(inout) :: A
        type(sparse),intent(in) :: B
        integer,intent(in) :: N
        integer,dimension(N),intent(in) :: i
        integer :: j
#ifdef _DEBUG_sparse_
          call insist_allocated(A,'assign_sparse_sparse (A)')
          call insist_allocated(B,'assign_sparse_sparse (B)')
          call insist_bounds(A,min(i),max(i),'assign_sparse_sparse (A)')
          call insist_bounds(B,min(i),max(i),'assign_sparse_sparse (B)')
#endif
#ifdef _PARALLELIZE_SPARSE_
        !$OMP PARALLEL DO

#endif

        do j=1,N; A%row(i(j)) = B%row(i(j)); enddo
#ifdef _PARALLELIZE_SPARSE_
        !$OMP END PARALLEL DO

#endif
      end subroutine

      subroutine assign_sparse_sparse_i_i(A,B,i_A,i_B,N)
        implicit none
        type(sparse),intent(inout) :: A
        type(sparse),intent(in) :: B
        integer,dimension(N),intent(in) :: i_A,i_B
        integer,intent(in) :: N
        integer :: j
#ifdef _DEBUG_sparse_
          call insist_allocated(A,'assign_sparse_sparse_i_i (A)')
          call insist_allocated(B,'assign_sparse_sparse_i_i (B)')
          call insist_bounds(A,min(i_A),max(i_A),'assign_sparse_sparse_i_i (A)')
          call insist_bounds(B,min(i_B),max(i_B),'assign_sparse_sparse_i_i (B)')
#endif
#ifdef _PARALLELIZE_SPARSE_
        !$OMP PARALLEL DO

#endif

        do j=1,N; A%row(i_A(j)) = B%row(i_B(j)); enddo
#ifdef _PARALLELIZE_SPARSE_
        !$OMP END PARALLEL DO

#endif
      end subroutine

      subroutine assign_sparse_sparse_all(A,B)
        implicit none
        type(sparse),intent(inout) :: A
        type(sparse),intent(in) :: B
        integer :: j
#ifdef _DEBUG_sparse_
          call insist_allocated(A,'assign_sparse_sparse_all (A)')
          call insist_allocated(B,'assign_sparse_sparse_all (B)')
#endif
#ifdef _PARALLELIZE_SPARSE_
        !$OMP PARALLEL DO

#endif

        do j=1,B%N; A%row(j) = B%row(j); enddo
#ifdef _PARALLELIZE_SPARSE_
        !$OMP END PARALLEL DO

#endif
      end subroutine

      subroutine multiply_sparse_vec_all(x,s,v)
        implicit none
        type(vec),intent(inout) :: x
        type(sparse),intent(in) :: s
        type(vec),intent(in) :: v
        integer :: j
#ifdef _DEBUG_sparse_
          call insist_allocated(x,'multiply_sparse_vec_all (x)')
          call insist_allocated(s,'multiply_sparse_vec_all (s)')
          call insist_allocated(v,'multiply_sparse_vec_all (v)')
#endif

#ifdef _PARALLELIZE_SPARSE_
        !$OMP PARALLEL DO

#endif
        do j=1,s%N
          call multiply(x,s%row(j),v)
        enddo
#ifdef _PARALLELIZE_SPARSE_
        !$OMP END PARALLEL DO

#endif
      end subroutine

      ! **************************************************************
      ! ******************** AUXILIARY OPERATIONS ********************
      ! **************************************************************

      subroutine swap_sparse(s,i,j)
        implicit none
        type(sparse),intent(inout) :: s
        integer,intent(in) :: i,j
        real(cp) :: temp_ij,temp_ji
#ifdef _DEBUG_sparse_
        call insist_allocated(s,'swap_sparse')
        call insist_bounds(s,i,i,'swap_sparse i')
        call insist_bounds(s,j,j,'swap_sparse j')
#endif
        call get_element(temp_ij,s%row(i),j)
        call get_element(temp_ji,s%row(j),i)
        call assign(s%row(j),temp,i)
        call assign(s%row(i),temp,j)
      end subroutine

      ! **************************************************************
      ! ******************** DEBUGGING OPERATIONS ********************
      ! **************************************************************

      subroutine insist_allocated_sparse(s,caller)
        implicit none
        type(sparse),intent(in) :: s
        character(len=*),intent(in) :: caller
        integer :: i
        do i=1,s%N
          call insist_allocated(s%row(i),'insist_allocated_sparse')
        enddo
      end subroutine

      subroutine insist_bounds_sparse(s,imin,imax,caller)
        implicit none
        type(sparse),intent(in) :: s
        integer,intent(in) :: imin,imax
        character(len=*),intent(in) :: caller
        integer :: i
        do i=1,s%N
          call insist_bounds(s%row(i),imin,imax,'insist_bounds_sparse')
        enddo
      end subroutine

      end module