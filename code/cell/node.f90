      module node_mod
      use current_precision_mod
      use VI_mod
      implicit none

      private
      public :: node
      public :: init,delete,export,print
      public :: init_neighbor
      public :: same_point
      public :: get_h
      public :: init_ID

      type node
        integer :: ID,CID
        logical,private :: boundary
        type(VI),private :: neighbors
        real(cp),dimension(3) :: h,dh ! dh = dh for dual grid
      end type

      interface init;           module procedure init_node;            end interface
      interface init;           module procedure init_copy;            end interface
      interface delete;         module procedure delete_node;          end interface
      interface print;          module procedure print_node;           end interface
      interface export;         module procedure export_node;          end interface

      interface get_h;          module procedure get_h_node;           end interface
      interface init_ID;        module procedure init_ID_node;         end interface
      interface init_neighbor;  module procedure init_neighbor_node;   end interface

      contains

      subroutine init_node(n,h)
        implicit none
        type(node),intent(inout) :: n
        real(cp),dimension(3),intent(in) :: h
        call delete(n)
        n%h = h
      end subroutine

      subroutine init_copy(n,d)
        implicit none
        type(node),intent(inout) :: n
        type(node),intent(in) :: d
        n%h = d%h
        n%dh = d%dh
        n%boundary = d%boundary
        n%ID = d%ID
        if (allocated(d%neighbors%i)) then
          call init(n%neighbors,d%neighbors)
        endif
      end subroutine

      subroutine init_neighbor_node(a,b,a_ID,b_ID,tol)
        implicit none
        type(node),intent(inout) :: a,b
        integer,intent(in) :: a_ID,b_ID
        real(cp),intent(in) :: tol
        call insert_unique(a%neighbors,a_ID)
        call insert_unique(b%neighbors,b_ID)
        if (same_point(a%h,b%h,tol)) then
          call insert_unique(a%neighbors,b_ID)
          call insert_unique(b%neighbors,a_ID)
        endif
      end subroutine

      subroutine init_ID_node(n,ID)
        implicit none
        type(node),intent(inout) :: n
        integer,intent(in) :: ID
        n%ID = ID
      end subroutine

      function get_h_node(n) result(h)
        implicit none
        type(node),intent(in) :: n
        real(cp),dimension(3) :: h
        h = n%h
      end function

      function same_point(a,b,tol) result(TF)
        real(cp),dimension(3),intent(in) :: a,b
        real(cp),intent(in) :: tol
        logical :: TF
        integer :: i
        TF = all((/(abs(a(i) - b(i)).lt.tol,i=1,3)/))
      end function

      subroutine delete_node(n)
        implicit none
        type(node),intent(inout) :: n
        n%h = 0.0_cp
      end subroutine

      subroutine print_node(n)
        implicit none
        type(node),intent(in) :: n
        call export(n,6)
      end subroutine

      subroutine export_node(n,un)
        implicit none
        type(node),intent(in) :: n
        integer,intent(in) :: un
        write(un,*) 'h = ',n%h
        call export(n%neighbors,un)
      end subroutine

      end module