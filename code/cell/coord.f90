      module coord_mod
      use current_precision_mod
      implicit none

      private
      public :: coord
      public :: init,delete
      public :: print,export

      type coord
        real(cp),dimension(3) :: h,dh
      end type

      interface init;           module procedure init_coord;            end interface
      interface init;           module procedure init_copy;             end interface
      interface delete;         module procedure delete_coord;          end interface
      interface print;          module procedure print_coord;           end interface
      interface export;         module procedure export_coord;          end interface

      contains

      subroutine init_coord(c,h,dh)
        implicit none
        type(coord),intent(inout) :: c
        real(cp),dimension(3),intent(in) :: h,dh
        c%h = h
        c%dh = dh
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(coord),intent(inout) :: a
        type(coord),intent(in) :: b
        a%h = b%h
        a%dh = b%dh
      end subroutine

      subroutine delete_coord(c)
        implicit none
        type(coord),intent(inout) :: c
        c%h = 0.0_cp
        c%dh = 0.0_cp
      end subroutine

      subroutine print_coord(c)
        implicit none
        type(coord),intent(in) :: c
        call export(c,6)
      end subroutine

      subroutine export_coord(c,un)
        implicit none
        type(coord),intent(in) :: c
        integer,intent(in) :: un
        write(un,'(3F12.3)') ' h = ',c%h
        write(un,'(3F12.3)') ' dh = ',c%dh
      end subroutine

      end module