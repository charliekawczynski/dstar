      module cell_mod
      use current_precision_mod
      use CC_mod
      use face_mod
      use node_mod
      implicit none

      private
      public :: cell
      public :: init,delete,export,print
      public :: init_neighbor
      public :: assign_node_ID
      public :: init_ghost
      public :: refine

      type cell
        integer :: ID
        real(cp) :: vol
        logical :: ghost
        type(CC) :: c
        type(face),dimension(6) :: f
        type(node),dimension(8) :: n
        ! type(node),dimension(12) :: e
      end type

      interface init;           module procedure init_cell;            end interface
      interface init;           module procedure init_copy;            end interface
      interface init_neighbor;  module procedure init_neighbor_cell;   end interface
      interface refine;         module procedure refine_cell;          end interface
      interface delete;         module procedure delete_cell;          end interface
      interface print;          module procedure print_cell;           end interface
      interface export;         module procedure export_cell;          end interface

      interface init_ghost;     module procedure init_ghost_cell;      end interface

      contains

      subroutine init_cell(c,ID,h,dh)
        implicit none
        type(cell),intent(inout) :: c
        integer,intent(in) :: ID
        real(cp),dimension(3),intent(in) :: h,dh
        integer :: i
        call delete(c)
        c%ID = ID
        call init(c%c,h,dh)
        do i=1,6; call init(c%f(i),i); enddo
        call init_Faces(c,h,dh)
        call init_Nodes(c,h,dh)
        ! call init_Edges(c,h,dh)
        c%vol = dh(1)*dh(2)*dh(3)
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(cell),intent(inout) :: a
        type(cell),intent(in) :: b
        integer :: i
        a%ID = b%ID
        a%vol = b%vol
        call init(a%c,b%c)
        do i=1,6; call init(a%f(i),b%f(i)); enddo
        ! do i=1,12; call init(a%e(i),b%e(i)); enddo
        do i=1,8; call init(a%n(i),b%n(i)); enddo
      end subroutine

      subroutine delete_cell(c)
        implicit none
        type(cell),intent(inout) :: c
        integer :: i
        call delete(c%c)
        do i=1,6; call delete(c%f(i)); enddo
        ! do i=1,12; call delete(c%e(i)); enddo
        do i=1,8; call delete(c%n(i)); enddo
        c%vol = 0.0_cp; c%ID = 0
      end subroutine

      subroutine init_neighbor_cell(a,b)
        implicit none
        type(cell),intent(inout) :: a,b
        real(cp) :: tol
        logical:: TF
        integer :: i,j
        tol = 10.0_cp**(-10.0_cp)
        TF = (a%ID.eq.27).and.(b%ID.eq.28)
        ! Faces:
        call init_neighbor(a%f(1),b%f(2),a%ID,b%ID,tol)
        call init_neighbor(a%f(2),b%f(1),a%ID,b%ID,tol)
        call init_neighbor(a%f(3),b%f(4),a%ID,b%ID,tol)
        call init_neighbor(a%f(4),b%f(3),a%ID,b%ID,tol)
        call init_neighbor(a%f(5),b%f(6),a%ID,b%ID,tol)
        call init_neighbor(a%f(6),b%f(5),a%ID,b%ID,tol)
        ! Edges:
        ! do i=1,12; do j=1,12
        ! if (i.ne.j) call init_neighbor(a%e(i),b%e(j),a%ID,b%ID,tol)
        ! enddo; enddo
        ! Nodes:
        do i=1,8; do j=1,8
        if (i.ne.j) call init_neighbor(a%n(i),b%n(j),a%ID,b%ID,tol)
        enddo; enddo
      end subroutine

      subroutine assign_node_ID(c,ID,TF,h)
        implicit none
        type(cell),intent(inout) :: c
        logical,intent(inout) :: TF
        integer,intent(in) :: ID
        real(cp),dimension(3),intent(in) :: h
        real(cp) :: tol
        integer :: i
        tol = 10.0_cp**(-10.0_cp)
        TF = .false.
        do i=1,8
          if (TF) cycle
          TF = same_point(get_h(c%n(i)),h,tol)
          if (TF) then
            call init_ID(c%n(i),ID)
          endif
        enddo
      end subroutine

      subroutine refine_cell(a,b,dir,ID_b)
        ! Need to preserve properties if this is used at runtime
        ! Right now, node_ID is not initialized, which means that
        ! exporting will be messed up if the nodes are not re-determined
        ! and assign_node_ID called again.
        implicit none
        type(cell),intent(inout) :: a,b
        integer,intent(in) :: dir,ID_b
        real(cp),dimension(3) :: h,dh
#ifdef _DEBUG_CELL_
        call check_dir(dir,'refine_cell')
#endif
        h = get_h(a%c); dh = get_dh(a%c)
        h(dir) = h(dir)+0.25_cp*dh(dir)
        dh(dir) = 0.5_cp*dh(dir)
        call init(b,ID_b,h,dh)
        h = get_h(a%c); dh = get_dh(a%c)
        h(dir) = h(dir)-0.25_cp*dh(dir)
        dh(dir) = 0.5_cp*dh(dir)
        call init(a,ID_b,h,dh)
        call init_neighbor(a,b)
      end subroutine

#ifdef _DEBUG_CELL_
      subroutine check_dir(dir,s)
        implicit none
        integer,intent(in) :: dir
        character(len=*),intent(in) :: s
        select case (dir)
        case (1:3)
        case default
        write(*,*) 'Error: dir must = 1,2,3 in '//s//' in cell.f90'
        stop 'Done'
        end select
      end subroutine
#endif

      subroutine init_ghost_cell(c)
        implicit none
        type(cell),intent(inout) :: c
        integer :: j
        c%ghost = any((/(get_boundary(c%f(j)),j=1,6)/))
      end subroutine

      subroutine init_Nodes(c,h,dh)
        implicit none
        type(cell),intent(inout) :: c
        real(cp),dimension(3),intent(in) :: h,dh
        call init(c%n(1),(/h(1)-0.5_cp*dh(1),h(2)-0.5_cp*dh(2),h(3)-0.5_cp*dh(3)/))
        call init(c%n(2),(/h(1)+0.5_cp*dh(1),h(2)-0.5_cp*dh(2),h(3)-0.5_cp*dh(3)/))
        call init(c%n(3),(/h(1)-0.5_cp*dh(1),h(2)+0.5_cp*dh(2),h(3)-0.5_cp*dh(3)/))
        call init(c%n(4),(/h(1)-0.5_cp*dh(1),h(2)-0.5_cp*dh(2),h(3)+0.5_cp*dh(3)/))
        call init(c%n(5),(/h(1)-0.5_cp*dh(1),h(2)+0.5_cp*dh(2),h(3)+0.5_cp*dh(3)/))
        call init(c%n(6),(/h(1)+0.5_cp*dh(1),h(2)-0.5_cp*dh(2),h(3)+0.5_cp*dh(3)/))
        call init(c%n(7),(/h(1)+0.5_cp*dh(1),h(2)+0.5_cp*dh(2),h(3)-0.5_cp*dh(3)/))
        call init(c%n(8),(/h(1)+0.5_cp*dh(1),h(2)+0.5_cp*dh(2),h(3)+0.5_cp*dh(3)/))
      end subroutine

      subroutine init_Faces(c,h,dh)
        implicit none
        type(cell),intent(inout) :: c
        real(cp),dimension(3),intent(in) :: h,dh
        call init(c%f(1),(/h(1)-0.5_cp*dh(1),h(2),h(3)/),(/0.0_cp,dh(2),dh(3)/))
        call init(c%f(2),(/h(1)+0.5_cp*dh(1),h(2),h(3)/),(/0.0_cp,dh(2),dh(3)/))
        call init(c%f(3),(/h(1),h(2)-0.5_cp*dh(2),h(3)/),(/dh(1),0.0_cp,dh(3)/))
        call init(c%f(4),(/h(1),h(2)+0.5_cp*dh(2),h(3)/),(/dh(1),0.0_cp,dh(3)/))
        call init(c%f(5),(/h(1),h(2),h(3)-0.5_cp*dh(3)/),(/dh(1),dh(2),0.0_cp/))
        call init(c%f(6),(/h(1),h(2),h(3)+0.5_cp*dh(3)/),(/dh(1),dh(2),0.0_cp/))
      end subroutine

      subroutine print_cell(c)
        implicit none
        type(cell),intent(in) :: c
        call export(c,6)
      end subroutine

      subroutine export_cell(c,un)
        implicit none
        type(cell),intent(in) :: c
        integer,intent(in) :: un
        ! integer :: i
        write(un,*) ' -------------------- CELL -------------------- ',c%ID
        call export(c%c,un)
        ! do i=1,8; call export(c%n(i),un); enddo
        ! do i=1,6; call export(c%f(i),un); enddo
        write(un,'(A12,L,F12.3)') 'vol = ',c%vol
      end subroutine

      end module