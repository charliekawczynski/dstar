      module boundary_mod
      use current_precision_mod
      implicit none

      private
      public :: boundary
      public :: init,init_neighbor,delete
      public :: export,print

      type boundary
        integer :: ID,neighbor_ID
        logical :: TF ! true by default
      end type

      interface init;          module procedure init_boundary;            end interface
      interface init_neighbor; module procedure init_neighbor_boundary;   end interface
      interface delete;        module procedure delete_boundary;          end interface
      interface print;         module procedure print_boundary;           end interface
      interface export;        module procedure export_boundary;          end interface

      contains

      subroutine init_boundary(f,ID)
        implicit none
        type(boundary),intent(inout) :: f
        integer,intent(in) :: ID
        call delete(f)
        f%ID = ID
        f%TF = .true.
      end subroutine

      subroutine delete_boundary(f)
        implicit none
        type(boundary),intent(inout) :: f
        f%ID = 0
        f%neighbor_ID = 0
        f%TF = .true.
      end subroutine

      subroutine init_neighbor_boundary(f,neighbor_ID)
        implicit none
        type(boundary),intent(inout) :: f
        integer,intent(in) :: neighbor_ID
        f%neighbor_ID = neighbor_ID
        f%TF = .false.
      end subroutine

      subroutine print_boundary(f)
        implicit none
        type(boundary),intent(inout) :: f
        call export_boundary(f,6)
      end subroutine

      subroutine export_boundary(f,un)
        implicit none
        type(boundary),intent(inout) :: f
        integer,intent(in) :: un
        if (.not.f%TF) then
          write(un,*) 'ID,TF,neighbor_ID = ',f%ID,f%TF,f%neighbor_ID
        else
          write(un,*) 'ID,TF = ',f%ID,f%TF
        endif
      end subroutine

      end module