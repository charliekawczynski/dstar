      module CC_mod
      use current_precision_mod
      implicit none

      private
      public :: CC
      public :: init,delete,export,print

      public :: get_h,get_dh

      type CC
        real(cp),dimension(3) :: h,dh ! dh = 0 along CC dir
      end type

      interface init;          module procedure init_h;             end interface
      interface init;          module procedure init_copy;          end interface
      interface delete;        module procedure delete_CC;          end interface
      interface print;         module procedure print_CC;           end interface
      interface export;        module procedure export_CC;          end interface

      interface get_h;         module procedure get_h_CC;           end interface
      interface get_dh;        module procedure get_dh_CC;          end interface

      contains

      subroutine init_h(c,h,dh)
        implicit none
        type(CC),intent(inout) :: c
        real(cp),dimension(3),intent(in) :: h,dh
        c%h = h
        c%dh = dh
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(CC),intent(inout) :: a
        type(CC),intent(in) :: b
        a%h = b%h
        a%dh = b%dh
      end subroutine

      subroutine delete_CC(c)
        implicit none
        type(CC),intent(inout) :: c
        c%h = 0.0_cp
        c%dh = 0.0_cp
      end subroutine

      subroutine print_CC(c)
        implicit none
        type(CC),intent(in) :: c
        call export_CC(c,6)
      end subroutine

      subroutine export_CC(c,un)
        implicit none
        type(CC),intent(in) :: c
        integer,intent(in) :: un
        write(un,*) 'h = ',c%h
        write(un,*) 'dh = ',c%dh
      end subroutine

      function get_h_CC(c) result(h)
        implicit none
        type(CC),intent(in) :: c
        real(cp),dimension(3) :: h
        h = c%h
      end function

      function get_dh_CC(c) result(dh)
        implicit none
        type(CC),intent(in) :: c
        real(cp),dimension(3) :: dh
        dh = c%dh
      end function

      end module