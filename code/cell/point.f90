      module point_mod
      use current_precision_mod
      implicit none

      private
      public :: point
      public :: init,delete,export,print

      public :: init_ID,init_CID

      type point
        integer :: ID,CID ! Location ID, cell ID
        real(cp),dimension(3) :: h
      end type

      interface init;          module procedure init_h_p;              end interface
      interface init;          module procedure init_h_p_dir;          end interface
      interface init;          module procedure init_copy;             end interface
      interface delete;        module procedure delete_point;          end interface
      interface print;         module procedure print_point;           end interface
      interface export;        module procedure export_point;          end interface

      interface init_CID;      module procedure init_CID_p;            end interface
      interface init_ID;       module procedure init_ID_p;             end interface

      contains

      subroutine init_h_p(p,h)
        implicit none
        type(point),intent(inout) :: p
        real(cp),dimension(3),intent(in) :: h
        call delete(p)
        p%h = h
      end subroutine

      subroutine init_h_p_dir(p,h,dir)
        implicit none
        type(point),intent(inout) :: p
        real(cp),intent(in) :: h
        integer,intent(in) :: dir
        p%h(dir) = h
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(point),intent(inout) :: a
        type(point),intent(in) :: b
        a%h = b%h
        a%CID = b%CID
        a%ID = b%ID
      end subroutine

      subroutine init_CID_p(p,CID)
        implicit none
        type(point),intent(inout) :: p
        integer,intent(in) :: CID
        p%CID = CID
      end subroutine

      subroutine init_ID_p(p,ID)
        implicit none
        type(point),intent(inout) :: p
        integer,intent(in) :: ID
        p%ID = ID
      end subroutine

      subroutine delete_point(p)
        implicit none
        type(point),intent(inout) :: p
        p%ID = 0
        p%CID = 0
        p%h = 0.0_cp
      end subroutine

      subroutine print_point(p)
        implicit none
        type(point),intent(inout) :: p
        call export_point(p,6)
      end subroutine

      subroutine export_point(p,un)
        implicit none
        type(point),intent(inout) :: p
        integer,intent(in) :: un
        write(un,*) 'CID,ID,h = ',p%CID,p%ID,p%h
      end subroutine

      end module