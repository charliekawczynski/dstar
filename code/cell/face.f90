      module face_mod
      use current_precision_mod
      use VI_mod
      implicit none

      private
      public :: face
      public :: init,delete,export,print
      public :: init_neighbor,get_boundary

      type face
        integer,private :: ID,CID
        type(VI),private :: neighbors
        logical,private :: boundary ! true by default
        real(cp),dimension(3) :: h,dh ! dh = 0 along face dir
      end type

      interface init;          module procedure init_face;            end interface
      interface init;          module procedure init_copy;            end interface
      interface init;          module procedure init_h;               end interface
      interface get_boundary;  module procedure get_boundary_f;       end interface
      interface overlap;       module procedure overlap_face;         end interface
      interface init_neighbor; module procedure init_neighbor_face;   end interface
      interface delete;        module procedure delete_face;          end interface
      interface print;         module procedure print_face;           end interface
      interface export;        module procedure export_face;          end interface

      contains

      subroutine init_face(f,ID)
        implicit none
        type(face),intent(inout) :: f
        integer,intent(in) :: ID
        call delete(f)
        f%ID = ID
        f%boundary = .true.
      end subroutine

      subroutine init_h(f,h,dh)
        implicit none
        type(face),intent(inout) :: f
        real(cp),dimension(3),intent(in) :: h,dh
        f%h = h
        f%dh = dh
      end subroutine

      subroutine init_copy(f,g)
        implicit none
        type(face),intent(inout) :: f
        type(face),intent(in) :: g
        if (allocated(g%neighbors%i)) call init(f%neighbors,g%neighbors)
        f%ID = g%ID
        f%boundary = g%boundary
        f%h = g%h
        f%dh = g%dh
      end subroutine

      subroutine delete_face(f)
        implicit none
        type(face),intent(inout) :: f
        f%ID = 0
        f%boundary = .true.
        f%h = 0.0_cp
        f%dh = 0.0_cp
        call delete(f%neighbors)
      end subroutine

      function get_boundary_f(f) result (b)
        implicit none
        type(face),intent(in) :: f
        logical :: b
        b = f%boundary
      end function

      subroutine init_neighbor_face(a,b,a_ID,b_ID,tol)
        implicit none
        type(face),intent(inout) :: a,b
        integer,intent(in) :: a_ID,b_ID
        real(cp),intent(in) :: tol
        if (overlap(a,b,tol)) then
          call insert_unique(a%neighbors,b_ID)
          call insert_unique(b%neighbors,a_ID)
          a%boundary = .false.
          b%boundary = .false.
        endif
      end subroutine

      function overlap_face(a,b,tol) result(TF_any)
        type(face),intent(in) :: a,b
        real(cp),intent(in) :: tol
        logical,dimension(3) :: TF,TF_dir
        logical :: TF_any
        integer :: i,j,k
        i = 1; j = 2; k = 3
        TF(1) = abs(a%h(i)-b%h(i)).lt.tol
        TF(2) = OI(a%h(j),b%h(j),a%dh(j),b%dh(j),tol)
        TF(3) = OI(a%h(k),b%h(k),a%dh(k),b%dh(k),tol)
        TF_dir(i) = all(TF)

        i = 2; j = 1; k = 3
        TF(1) = abs(a%h(i)-b%h(i)).lt.tol
        TF(2) = OI(a%h(j),b%h(j),a%dh(j),b%dh(j),tol)
        TF(3) = OI(a%h(k),b%h(k),a%dh(k),b%dh(k),tol)
        TF_dir(i) = all(TF)

        i = 3; j = 2; k = 1
        TF(1) = abs(a%h(i)-b%h(i)).lt.tol
        TF(2) = OI(a%h(j),b%h(j),a%dh(j),b%dh(j),tol)
        TF(3) = OI(a%h(k),b%h(k),a%dh(k),b%dh(k),tol)
        TF_dir(i) = all(TF)

        TF_any = any(TF_dir)

#ifdef _DEBUG_FACE_
        if (count(TF_any).gt.1) then
          write(*,*) 'Error: overlap_face yielding unpredicted result in face.f90'
        endif
#endif
      end function

      function OI(a,b,a_dh,b_dh,tol) result(TF_any)
        ! TF = overlapping interval (OI)
        ! 6 possibilities:
        ! This assumes dh is NOT zero.
        ! 
        !       1)
        !                 |-----a-----|
        !             |-----b-----|
        !       2)
        !         |-----a-----|
        !             |-----b-----|
        !       3)
        !             |---a---|      |-------a-------|
        !             |-----b-----|  |-----b-----|
        !       4)
        !                 |---a---|  |-------a-------|
        !             |-----b-----|      |-----b-----|
        !       5)
        !               |---a---|      |-------a-------|
        !             |-----b-----|      |-----b-----|
        !       6)
        !               |---b---|      |-------b-------|
        !             |-----a-----|      |-----a-----|
        real(cp),intent(in) :: a,b
        real(cp),intent(in) :: a_dh,b_dh
        real(cp),intent(in) :: tol
        logical,dimension(6) :: TF
        logical :: TF_any
        TF(1) = (a-0.5_cp*a_dh.gt.b-0.5_cp*b_dh).and.(a-0.5_cp*a_dh.lt.b+0.5_cp*b_dh)
        TF(2) = (a+0.5_cp*a_dh.gt.b-0.5_cp*b_dh).and.(a+0.5_cp*a_dh.lt.b+0.5_cp*b_dh)
        TF(3) = abs(b-0.5_cp*b_dh-(a-0.5_cp*a_dh)).lt.tol
        TF(4) = abs(b+0.5_cp*b_dh-(a+0.5_cp*a_dh)).lt.tol
        TF(5) = (a-0.5_cp*a_dh.gt.b-0.5_cp*b_dh).and.(a+0.5_cp*a_dh.lt.b+0.5_cp*b_dh)
        TF(6) = (b-0.5_cp*b_dh.gt.a-0.5_cp*a_dh).and.(b+0.5_cp*b_dh.lt.a+0.5_cp*a_dh)
        TF_any = any(TF)
      end function

      subroutine print_face(f)
        implicit none
        type(face),intent(in) :: f
        call export_face(f,6)
      end subroutine

      subroutine export_face(f,un)
        implicit none
        type(face),intent(in) :: f
        integer,intent(in) :: un
        write(un,*) 'h = ',f%h
        write(un,*) 'boundary = ',f%boundary
        write(un,*) 'neighbors = ',f%neighbors%i
        write(un,*) 'N_neighbors = ',f%neighbors%s
        ! call export(f%neighbors,un)
        ! if (.not.f%boundary) then
        !   write(un,*) 'ID,boundary,neighbor_ID = ',f%ID,f%boundary,f%neighbor_ID
        ! else
        !   write(un,*) 'ID,boundary,neighbor_ID = ',f%ID,f%boundary
        ! endif
      end subroutine

      end module