      module data_loc_mod
      use current_precision_mod
      use VI_mod
      implicit none

      private
      public :: data_loc
      public :: init,delete,print,export
      public :: init_C
      public :: init_N,init_CN
      public :: init_F,init_CF
      public :: init_E,init_CE

      type data_loc
        type(GI) :: C,N ! cell-centered and node
        type(VG),dimension(3) :: F,E ! Face and Edge
        type(VG) :: CN              ! Indexes for cell-center at each node point ("ownership" so to speak)
        type(VG),dimension(3) :: CF ! Indexes for cell-center at each face point ("ownership" so to speak)
        type(VG),dimension(3) :: CE ! Indexes for cell-center at each edge point ("ownership" so to speak)
      end type

      interface init;    module procedure init_DL;       end interface
      interface init;    module procedure init_copy_DL;  end interface
      interface delete;  module procedure delete_DL;     end interface
      interface print;   module procedure print_DL;      end interface
      interface export;  module procedure export_DL;     end interface

      contains

      subroutine init_DL(DL)
        implicit none
        type(data_loc),intent(inout) :: DL
        call delete(DL)
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(data_loc),intent(inout) :: a
        type(data_loc),intent(in) :: b
        integer :: i
        call ini(a%C,b%C)
        call ini(a%N,b%N)
        do i=1,3; call init(a%F(i),b%F(i)); enddo
        do i=1,3; call init(a%E(i),b%E(i)); enddo
        call ini(a%CN,b%CN)
        do i=1,3; call init(a%CF(i),b%CF(i)); enddo
        do i=1,3; call init(a%CE(i),b%CE(i)); enddo
      end subroutine

      subroutine delete_DL(DL)
        implicit none
        type(data_loc),intent(inout) :: DL
        integer :: i
        call delete(DL%C)
        call delete(DL%N)
        do i=1,3; call delete(DL%F(i)); enddo
        do i=1,3; call delete(DL%E(i)); enddo
        call delete(DL%CN)
        do i=1,3; call delete(DL%CF(i)); enddo
        do i=1,3; call delete(DL%CE(i)); enddo
      end subroutine

      subroutine print_DL(DL)
        implicit none
        type(data_loc),intent(in) :: DL
        call export(DL,6)
      end subroutine

      subroutine export_DL(m,un)
        implicit none
        type(DL),intent(in) :: m
        integer,intent(in) :: un
        integer :: i
        call export(DL%C,un)
        call export(DL%N,un)
        do i=1,3; call export(DL%F(i),un); enddo
        do i=1,3; call export(DL%E(i),un); enddo
        call export(DL%CN,un)
        do i=1,3; call export(DL%CF(i),un); enddo
        do i=1,3; call export(DL%CE(i),un); enddo
      end subroutine

      end module