      module GI_mod
      ! Group Index
      use current_precision_mod
      use DI_base_mod
      implicit none

      private
      public :: GI

      type GI
        type(DI) :: C,N,F,E
      end type

      contains

      end module