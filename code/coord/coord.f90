      module coord_mod
      use current_precision_mod
      use VG_mod
      implicit none

      private
      public :: coord

      type coord
        type(VG),dimension(3) :: h
      end type

      interface init;    module procedure init_c;        end interface
      interface init;    module procedure init_copy_c;   end interface
      interface delete;  module procedure delete_c;      end interface
      interface print;   module procedure print_c;       end interface
      interface export;  module procedure export_c;      end interface

      contains

      subroutine init_c(c,h,dir)
        implicit none
        type(coord),intent(inout) :: c
        type(VG),intent(in) :: h
        integer,intent(in) :: dir
        call init(c%h(dir),h)
      end subroutine

      subroutine init_copy_c(a,b)
        implicit none
        type(coord),intent(inout) :: a
        type(coord),intent(in) :: b
        integer :: i
        do i=1,3; call init(a%h(i),b%h(i)); enddo
      end subroutine

      subroutine delete_c(c)
        implicit none
        type(coord),intent(inout) :: c
        integer :: i
        do i=1,3; call delete(c%h(i)); enddo
      end subroutine

      subroutine print_c(c)
        implicit none
        type(coord),intent(in) :: c
        call export(c,6)
      end subroutine

      subroutine export_c(c)
        implicit none
        type(coord),intent(in) :: c
        integer :: i
        do i=1,3; call export(c%h(i),un); enddo
      end subroutine

      end module