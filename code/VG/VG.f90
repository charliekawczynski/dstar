      module VG_mod
      use VG_base_mod
      implicit none

      private
      public :: VG
      public :: init,delete,export,print

      public :: insert
      public :: append
      public :: swap

      end module