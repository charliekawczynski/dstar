      module VG_base_mod
      use current_precision_mod
      use VI_mod
      use VF_mod
      ! Compiler flags: (_DEBUG_VG_)
      implicit none

      private
      public :: VG
      public :: init,delete,export,print

      public :: insert
      public :: append
      public :: swap

      type VG
        type(VI) :: i
        type(VF) :: f
      end type

      interface init;      module procedure init_VG;      end interface
      interface init;      module procedure init_copy;    end interface
      interface delete;    module procedure delete_VG;    end interface
      interface export;    module procedure export_VG;    end interface
      interface print;     module procedure print_VG;     end interface

      interface append;    module procedure app_VG;       end interface
      interface insert;    module procedure insert_VG;    end interface

      interface swap;      module procedure swap_VG;      end interface

      contains

      subroutine init_VG(g,f,i)
        implicit none
        type(VG),intent(inout) :: g
        type(VF),intent(inout) :: f
        type(VI),intent(inout) :: i
        call init(g%f,f)
        call init(g%i,i)
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(VG),intent(inout) :: a
        type(VG),intent(in) :: b
        call init(a%i,b%i%s); call assign(a%i,b%i)
        call init(a%f,b%f%s); call assign(a%f,b%f)
      end subroutine

      subroutine delete_VG(v)
        implicit none
        type(VG),intent(inout) :: v
        call delete(v%i)
        call delete(v%f)
      end subroutine

      subroutine print_VG(v)
        implicit none
        type(VG),intent(in) :: v
        call export(v,6)
      end subroutine

      subroutine export_VG(v,un)
        implicit none
        type(VG),intent(in) :: v
        integer,intent(in) :: un
        call export(v%i,un)
        call export(v%f,un)
      end subroutine

      subroutine insert_VG(v,e,i)
        implicit none
        type(VG),intent(inout) :: v
        real(cp),intent(in) :: e
        integer,intent(in) :: i
        call insert(v%i,i,i)
        call insert(v%f,e,i)
      end subroutine

      subroutine app_VG(v,e)
        implicit none
        type(VG),intent(inout) :: v
        real(cp),intent(in) :: e
        integer :: s
        s = v%i%s
        call append(v%i,s+1)
        call append(v%f,e)
      end subroutine

      subroutine swap_VG(v,i,j)
        implicit none
        type(VG),intent(inout) :: v
        integer,intent(in) :: i,j
        call swap(v%i,i,j)
        call swap(v%f,i,j)
      end subroutine

      end module