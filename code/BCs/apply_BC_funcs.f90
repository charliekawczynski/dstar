      module apply_BC_funcs_mod
      use current_precision_mod
      implicit none

      private
      public :: N_Dirichlet,N_Neumann,N_Periodic
      public :: C_Dirichlet,C_Neumann,C_Periodic,C_Robin

      contains

       subroutine C_D(U,b,C,F)
         implicit none
         type(VF),intent(inout) :: U
         type(VF),intent(in) :: b
         type(DI),intent(in) :: C
         type(IS),intent(in) :: i_b
         call C_Dirichlet(U,b,C%g%D%s,C%fig%D%s,i_b%i)
       end subroutine
       subroutine C_Dirichlet(u,vals,i_ug,i_ui,i_vals)
         implicit none
         type(VF),intent(inout) :: u
         type(VF),intent(in) :: vals
         type(IS),intent(in) :: i_ug,i_ui,i_vals
         integer :: j
#ifdef _PARALLELIZE_APPLY_BC_FUNCS_
        !$OMP PARALLEL DO

#endif
         do j=1,i_ug%N
           u%v(i_ug%s(j)) = 2.0_cp*vals%v(i_vals%s(j)) - u%v(i_ui%s(j))
         enddo
#ifdef _PARALLELIZE_APPLY_BC_FUNCS_
        !$OMP END PARALLEL DO

#endif
       end subroutine

       subroutine C_N(U,b,dh,nhat,C,F)
         implicit none
         type(VF),intent(inout) :: U
         type(VF),intent(in) :: b
         real(cp),intent(in) :: dh,nhat
         type(DI),intent(in) :: C
         type(IS),intent(in) :: i_b
         call C_Neumann(U,b,dh,nhat,C%g%N%s,C%fig%N%s,i_b%i)
       end subroutine
       subroutine C_Neumann(u,vals,dh,nhat,i_ug,i_ui,i_vals)
         implicit none
         type(VF),intent(inout) :: u
         type(VF),intent(in) :: vals
         real(cp),intent(in) :: dh,nhat
         type(IS),intent(in) :: i_ug,i_ui,i_vals
         integer :: j
#ifdef _PARALLELIZE_APPLY_BC_FUNCS_
        !$OMP PARALLEL DO

#endif
         do j=1,i_ug%N
           u%v(i_ug%s(j)) = u%v(i_ui%s(j)) + nhat*vals%v(i_vals%s(j))*dh
         enddo
#ifdef _PARALLELIZE_APPLY_BC_FUNCS_
        !$OMP END PARALLEL DO

#endif
       end subroutine

       subroutine C_N(U,C)
         implicit none
         type(VF),intent(inout) :: U
         type(DI),intent(in) :: C
         call C_Periodic(U,C%g%N%s,C%i_opp%N%s)
       end subroutine
       subroutine C_Periodic(u,i_ug,i_ui_opp)
         implicit none
         type(VF),intent(inout) :: u
         type(IS),intent(in) :: i_ug,i_ui_opp
         integer :: j
#ifdef _PARALLELIZE_APPLY_BC_FUNCS_
        !$OMP PARALLEL DO

#endif
         do j=1,i_ug%N
           u%v(i_ug%s(j)) = u%v(i_ui_opp%s(j))
         enddo
#ifdef _PARALLELIZE_APPLY_BC_FUNCS_
        !$OMP END PARALLEL DO

#endif
       end subroutine

       subroutine C_R(U,b,dh,nhat,C,i_b)
         implicit none
         type(VF),intent(inout) :: U
         type(VF),intent(in) :: b
         real(cp),intent(in) :: dh,nhat
         type(DI),intent(in) :: C
         type(IS),intent(in) :: i_b
         call C_Robin(U,b,dh,nhat,C%g%R%s,C%fig%R%s,i_b%i)
       end subroutine
       subroutine C_Robin(u,vals,dh,nhat,i_ug,i_ui,i_vals) ! NOTE: vals is cw
         implicit none
         type(VF),intent(inout) :: u
         type(VF),intent(in) :: vals
         real(cp),intent(in) :: dh,nhat
         type(IS),intent(in) :: i_ug,i_ui,i_vals
         integer :: j
#ifdef _PARALLELIZE_APPLY_BC_FUNCS_
        !$OMP PARALLEL DO

#endif
         do j=1,i_ug%N
           u%v(i_ug%s(j)) = u%v(i_ui%s(j))*&
           (2.0_cp*vals%v(i_vals%s(j))/dh*nhat - &
            1.0_cp)/(2.0_cp*vals%v(i_vals%s(j))/dh*nhat + 1.0_cp)
         enddo
#ifdef _PARALLELIZE_APPLY_BC_FUNCS_
        !$OMP END PARALLEL DO

#endif
       end subroutine

       subroutine N_Periodic(u,i_ug,i_ui_opp)
         implicit none
         type(VF),intent(inout) :: u
         type(IS),intent(in) :: i_ug,i_ui_opp
         integer :: j
#ifdef _PARALLELIZE_APPLY_BC_FUNCS_
        !$OMP PARALLEL DO

#endif
         do j=1,i%N
           u%v(i_ug%s(j)) = u%v(i_ui_opp%s(j))
         enddo
#ifdef _PARALLELIZE_APPLY_BC_FUNCS_
        !$OMP END PARALLEL DO

#endif
       end subroutine

       subroutine N_Neumann(ug,ui,vals,dh,nhat,i_ug,i_ui,i_vals)
         implicit none
         type(VF),intent(inout) :: ug
         type(VF),intent(in) :: ui,vals
         real(cp),intent(in) :: dh,nhat
         type(IS),intent(in) :: i_ug,i_ui,i_vals
         integer :: j
#ifdef _PARALLELIZE_APPLY_BC_FUNCS_
        !$OMP PARALLEL DO

#endif
         do i=1,N
           u%v(i_ug%s(j)) = u%v(i_ui%s(j)) + 2.0_cp*vals%v(i_vals%s(j))*dh*nhat
         enddo
#ifdef _PARALLELIZE_APPLY_BC_FUNCS_
        !$OMP END PARALLEL DO

#endif
       end subroutine

       subroutine N_Dirichlet(u,vals,i_ug,i_ub,i_ui,i_vals)
         implicit none
         type(VF),intent(inout) :: u
         type(VF),intent(in) :: vals
         type(IS),intent(in) :: i_ug,i_ub,i_ui,i_vals
         integer :: j
#ifdef _PARALLELIZE_APPLY_BC_FUNCS_
        !$OMP PARALLEL DO

#endif
         do j=1,i%N
           u%v(i_ub%s(j)) = vals%v(i_vals%s(j))
           u%v(i_ug%s(j)) = 2.0_cp*u%v(i_ub%s(j)) - u%v(i_ui%s(j))
         enddo
#ifdef _PARALLELIZE_APPLY_BC_FUNCS_
        !$OMP END PARALLEL DO

#endif
       end subroutine

      end module