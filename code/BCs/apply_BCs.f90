      module apply_BCs_mod
      ! Compiler flags: (_DEBUG_APPLY_BCS_)
      use apply_BC_funcs_mod
      use current_precision_mod
      implicit none

      private
      public :: apply_BCs

      contains

      subroutine apply_BCs(U,b,i,i_b)
        implicit none
        type(FV),intent(inout) :: U
        type(FV),intent(in) :: b
        type(GI),intent(in) :: i
        type(BCT),intent(in) :: i_b
#ifdef _DEBUG_APPLY_BCS_
        if (.not.v%BCs%defined) stop 'Error: BCs not defined in apply_BCs.f90'
#endif
        if (U%is_CC) then
          if (U%i%C%D%defined) call C_Dirichlet(U,b,i%C,i_b%D%F%b)
          if (U%i%C%N%defined) call C_Neumann(U,b,i%C,i_b%D%F%b)
          if (U%i%C%P%defined) call C_Periodic(U,b,i%C,i_b%D%F%b)
          if (U%i%C%R%defined) call C_Robin(U,b,i%C,i_b%D%F%b)
        elseif (U%is_Node) then
          if (U%i%N%P%defined) call N_Periodic(U,b,i%N%P)
          if (U%i%N%N%defined) call N_Neumann(U,b,i%N%N)
          if (U%i%N%D%defined) call N_Dirichlet(U,b,i%N%D)
        elseif (U%is_Face) then
          if (U%i%F(U%face)%D%defined) call C_Dirichlet(U,b,i%F(U%face)%D)
          if (U%i%F(U%face)%N%defined) call C_Neumann(U,b,i%F(U%face)%N)
          if (U%i%F(U%face)%P%defined) call C_Periodic(U,b,i%F(U%face)%P)
          if (U%i%F(U%face)%R%defined) call C_Robin(U,b,i%F(U%face)%R)
          if (U%i%F(U%face)%P%defined) call N_Periodic(U,b,i%F(U%face)%P)
          if (U%i%F(U%face)%N%defined) call N_Neumann(U,b,i%F(U%face)%N)
          if (U%i%F(U%face)%D%defined) call N_Dirichlet(U,b,i%F(U%face)%D)
        elseif (U%is_Edge) then
          if (U%i%E(U%edge)%D%defined) call C_Dirichlet(U,b,i%E(U%edge)%D)
          if (U%i%E(U%edge)%N%defined) call C_Neumann(U,b,i%E(U%edge)%N)
          if (U%i%E(U%edge)%P%defined) call C_Periodic(U,b,i%E(U%edge)%P)
          if (U%i%E(U%edge)%R%defined) call C_Robin(U,b,i%E(U%edge)%R)
          if (U%i%E(U%edge)%P%defined) call N_Periodic(U,b,i%E(U%edge)%P)
          if (U%i%E(U%edge)%N%defined) call N_Neumann(U,b,i%E(U%edge)%N)
          if (U%i%E(U%edge)%D%defined) call N_Dirichlet(U,b,i%E(U%edge)%D)
          else; stop 'Error: bad data input to apply_BCs.f90'
        endif
      end subroutine


      end module