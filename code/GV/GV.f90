      module GV_mod
      use current_precision_mod
      use VI_base_mod
      use BCI_base_mod
      implicit none

      private
      public :: GV

      type GV
        type(VF) :: v
        type(GI) :: i
        type(VF) :: b
      end type

      contains

      end module