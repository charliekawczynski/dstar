      module mesh_mod
      use current_precision_mod
      use IO_tools_mod
      use exp_Tecplot_Header_mod
      use cell_mod
      use VF_mod
      use VI_mod
      use mesh_flat_mod
      implicit none

      private
      public :: mesh
      public :: init_all_mesh_props
      public :: init,add,delete
      public :: print,export
      public :: insert_cell
      public :: init_Node_ID

      integer,parameter :: FACES_PER_CELL = 6
      integer,parameter :: EDGES_PER_CELL = 12
      integer,parameter :: CORNERS_PER_CELL = 8

      type data_loc
        type(VI) :: C,N ! Cell-center,Node
        type(VI),dimension(3) :: F,E ! Face,Edge
        type(VF),dimension(3) :: hn
        type(VI) :: CN ! Cell-center corresponding to node
      end type

      interface init;    module procedure init_copy_DL;         end interface
      interface delete;  module procedure delete_DL;            end interface

      type mesh
        type(data_loc) :: total,interior,ghost
        type(VI) :: i_connectivity
        type(cell),dimension(:),allocatable :: c
        logical :: i_defined
      end type

      interface init;    module procedure init_mesh;            end interface
      interface init;    module procedure init_copy;            end interface
      interface add;     module procedure add_cell_mesh;        end interface
      interface delete;  module procedure delete_mesh;          end interface
      interface print;   module procedure print_mesh;           end interface
      interface export;  module procedure export_mesh;          end interface
      interface check;   module procedure check_mesh;           end interface
      interface insert_cell; module procedure insert_cell_mesh; end interface

      contains

      subroutine init_mesh(m)
        implicit none
        type(mesh),intent(inout) :: m
        call delete(m)
      end subroutine

      subroutine add_cell_mesh(m,c)
        implicit none
        type(mesh),intent(inout) :: m
        type(cell),intent(in) :: c
        type(mesh) :: temp
        integer :: i,N_cells
        if (allocated(m%c)) then
          call init(temp,m)
          N_cells = m%total%C%s
          call delete(m)
          m%total%C%s = N_cells+1
          allocate(m%c(m%total%C%s))
          do i=1,N_cells; call init(m%c(i),temp%c(i)); enddo
          call init(m%c(m%total%C%s),c)
          call delete(temp)
        else
          call delete(m)
          m%total%C%s = 1
          allocate(m%c(1))
          call init(m%c(1),c)
        endif
      end subroutine

      subroutine insert_cell_mesh(m,c,ID)
        ! Needs to be developed
        implicit none
        type(mesh),intent(inout) :: m
        type(cell),intent(in) :: c
        integer,intent(in) :: ID
        type(mesh) :: temp
        integer :: i,N_cells
        if (allocated(m%c)) then
          call init(temp,m)
          N_cells = m%total%C%s
          call delete(m)
          m%total%C%s = N_cells+1
#ifdef _DEBUG_MESH_
          call check_bounds(m,ID,'insert_cell_before_mesh')
#endif
          allocate(m%c(m%total%C%s))
          do i=1,ID-1; call init(m%c(i),temp%c(i)); enddo
          call init(m%c(ID),c)
          do i=ID+1,m%total%C%s; call init(m%c(i),temp%c(i-1)); enddo
          call delete(temp)
        else
          call delete(m)
          m%total%C%s = 1
          allocate(m%c(1))
          call init(m%c(1),c)
        endif
      end subroutine

      subroutine check_bounds(m,ID,s)
        implicit none
        type(mesh),intent(in) :: m
        integer,intent(in) :: ID
        character(len=*),intent(in) :: s
        if ((ID.lt.1).or.(ID.gt.m%total%C%s)) then
          write(*,*) 'Error: ID is not within bounds of mesh cells in '//s//' in mesh.f90'
          stop 'Done'
        endif
      end subroutine

      subroutine init_copy(m,n)
        implicit none
        type(mesh),intent(inout) :: m
        type(mesh),intent(in) :: n
        integer :: i
        call check(n,'init_copy')
        call delete(m)
        allocate(m%c(n%total%C%s))
        do i=1,n%total%C%s; call init(m%c(i),n%c(i)); enddo
        if (n%i_defined) then
          call init(m%interior%C,n%interior%C)
          call init(m%ghost%C,n%ghost%C)
          call init(m%total%C,n%total%C)
        endif
        call init(m%total,n%total)
        call init(m%interior,n%interior)
        call init(m%ghost,n%ghost)
        m%total%C%s = n%total%C%s
      end subroutine

      subroutine init_all_mesh_props(m)
        implicit none
        type(mesh),intent(inout) :: m
        integer :: i,j
        write(*,*) 'About to init mesh props'
        call check(m,'init_all_mesh_props')
        write(*,*) '     init mesh props 1'
        do i=1,m%total%C%s; do j=i,m%total%C%s
         call init_neighbor(m%c(i),m%c(j))
        enddo; enddo
        write(*,*) '     init mesh props 2'

        do i=1,m%total%C%s; call init_ghost(m%c(i)); enddo
        write(*,*) '     init mesh props 3'
        m%ghost%C%s = count((/(m%c(i)%ghost,i=1,m%total%C%s)/))
        write(*,*) '     init mesh props 4'
        do i=1,m%total%C%s
        call insert_unique(m%total%C,i)
        if (m%c(i)%ghost) then; call insert_unique(m%ghost%C,i)
        else;                   call insert_unique(m%interior%C,i)
        endif
        enddo
        write(*,*) '     init mesh props 5'
        m%i_defined = .true.

        write(*,*) '     init mesh props 6'
        write(*,*) '     m%total%C%s = ',m%total%C%s
        m%total%N%s    = init_N_Nodes_general(m,m%total%C)
        write(*,*) '     init mesh props 7'
        write(*,*) '     m%interior%C%s = ',m%interior%C%s
        if (m%interior%C%s.gt.0) then
        m%interior%N%s = init_N_Nodes_general(m,m%interior%C)
        endif
        write(*,*) '     init mesh props 8'
        
        m%total%F(:)%s    = init_N_Faces_general(m,m%total%C)
        write(*,*) '     init mesh props 9'
        if (m%interior%C%s.gt.0) then
        m%interior%F(:)%s = init_N_Faces_general(m,m%interior%C)
        endif
        write(*,*) '     init mesh props 10'

        ! m%ghost%C%s = m%total%C%s - m%interior%C%s ! Done manually
        m%ghost%N%s = m%total%N%s - m%interior%N%s
        m%ghost%F%s = m%total%F%s - m%interior%F%s
        write(*,*) '     init mesh props 11'
        call init_Node_ID(m,m%total%C,m%total%CN,m%total%hn)
        write(*,*) '     init mesh props 12'
      end subroutine

      subroutine init_Node_ID(m,i_index,CN_index,hn)
        implicit none
        type(mesh),intent(inout) :: m
        type(VI),intent(in) :: i_index
        type(VI),intent(inout) :: CN_index ! C index in coorespondence with N_index
        type(VF),dimension(3) :: hn
        type(VF) :: Ux,Uy,Uz
        integer :: i,j
        logical :: TF
        call init_i_Nodes_general(m,i_index,Ux,Uy,Uz,10.0_cp**(-10.0_cp))
        call init(hn(1),Ux)
        call init(hn(2),Uy)
        call init(hn(3),Uz)
        call init(CN_index,Ux%s)
        do i=1,Ux%s
          TF = .false.
          do j=1,i_index%s
          call assign_node_ID(m%c(j),i,TF,(/Ux%f(i),Uy%f(i),Uz%f(i)/))
          if (TF) call assign(CN_index,j,i)
          if (TF) cycle
          enddo
        enddo
        call delete(Ux)
        call delete(Uy)
        call delete(Uz)
      end subroutine

      subroutine init_mesh_VF(m,Ux,Uy,Uz,dx,dy,dz)
        implicit none
        type(mesh),intent(inout) :: m
        type(VF),intent(in) :: Ux,Uy,Uz,dx,dy,dz
        type(cell) :: c
        integer :: i
        do i=1,Ux%s
          call init(c,i,(/Ux%f(i),Uy%f(i),Uz%f(i)/),(/dx%f(i),dy%f(i),dz%f(i)/))
          call add(m,c)
        enddo
        call delete(c)
      end subroutine

      subroutine check_mesh(m,N_cells)
        implicit none
        type(mesh),intent(in) :: m
        character(len=*),intent(in) :: N_cells
        if (.not.allocated(m%c)) then
        write(*,*) 'Error: need allocated mesh in ',N_cells,' in mesh.f90'
        stop
        endif
      end subroutine

      subroutine delete_mesh(m)
        implicit none
        type(mesh),intent(inout) :: m
        integer :: i
        if (allocated(m%c)) then
          do i=1,m%total%C%s; call delete(m%c(i)); enddo
          ! if (m%total%C%s.gt.1) then
          ! else; write() 'Error: attempting to delete allocated mesh with N_cells<1 in mesh.f90'
          ! endif
        endif
        call delete(m%total)
        call delete(m%interior)
        call delete(m%ghost)
        if (allocated(m%c)) deallocate(m%c)
        m%i_defined = .false.
      end subroutine

      subroutine print_mesh(m)
        implicit none
        type(mesh),intent(in) :: m
        call export(m,6)
      end subroutine

      subroutine export_mesh(m,un)
        implicit none
        type(mesh),intent(in) :: m
        integer,intent(in) :: un
        ! integer :: j
        write(un,*) ' -------------------- mesh -------------------- '
        write(un,*) ' C DATA'
        write(un,*) ' Total,interior,ghost:    ',m%total%C%s,m%interior%C%s,m%ghost%C%s
        write(un,*) ' N DATA'
        write(un,*) ' Total,interior,ghost:    ',m%total%N%s,m%interior%N%s,m%ghost%N%s

        ! write(un,*) ' TOTAL '
        ! write(un,*) ' C,N,F,E:    ',m%total%C%s,m%total%N%s,m%total%F%s,m%total%E%s
        ! write(un,*) ' INTERIOR '
        ! write(un,*) ' C,N,F,E:    ',m%interior%C%s,m%interior%N%s,m%interior%F%s,m%interior%E%s
        ! write(un,*) ' GHOST '
        ! write(un,*) ' C,N,F,E:    ',m%ghost%C%s,m%ghost%N%s,m%ghost%F%s,m%ghost%E%s
        ! do j=1,m%interior%C%s; call export(m%c(m%interior%C%i(j)),un); enddo
        ! do j=1,m%total%C%s; call export(m%c(j),un); enddo
        ! do j=1,m%total%C%s
          ! write(un,*) 'Cell,ghost: ',j,m%c(j)%ghost
          ! call export(m%c(j),un)
        ! enddo
        ! write(un,*) 'interior%C = ',m%interior%C%i
      end subroutine

      subroutine init_copy_DL(a,b)
        implicit none
        type(data_loc),intent(inout) :: a
        type(data_loc),intent(in) :: b
        integer :: i
        call init(a%C,b%C)
        call init(a%N,b%N)
        do i=1,3; call init(a%F(i),b%F(i)); enddo
        do i=1,3; call init(a%E(i),b%E(i)); enddo
      end subroutine

      subroutine delete_DL(DL)
        implicit none
        type(data_loc),intent(inout) :: DL
        integer :: i
        call delete(DL%C)
        call delete(DL%N)
        do i=1,3; call delete(DL%F(i)); enddo
        do i=1,3; call delete(DL%E(i)); enddo
      end subroutine

      function init_N_Nodes_general(m,ind) result(N_Nodes)
        implicit none
        type(mesh),intent(in) :: m
        type(VI),intent(in) :: ind
        type(VF) :: Ux,Uy,Uz
        integer :: N_Nodes
        call init_i_Nodes_general(m,ind,Ux,Uy,Uz,10.0_cp**(-10.0_cp))
        N_Nodes = Ux%s
        call delete(Ux)
        call delete(Uy)
        call delete(Uz)
      end function

      function init_N_Faces_general(m,ind) result(N_Faces)
        implicit none
        type(mesh),intent(in) :: m
        type(VI),intent(in) :: ind
        type(VF),dimension(3) :: Ux,Uy,Uz
        integer,dimension(3) :: N_Faces
        integer :: i
        call init_i_Faces_general(m,ind,Ux,Uy,Uz)
        do i=1,3
          N_Faces(i) = Ux(i)%s
          call delete(Ux(i))
          call delete(Uy(i))
          call delete(Uz(i))
        enddo
      end function

      subroutine init_i_Nodes_general(m,ind,Ux,Uy,Uz,tol)
        implicit none
        type(mesh),intent(in) :: m
        type(VI),intent(in) :: ind
        type(VF),intent(inout) :: Ux,Uy,Uz
        real(cp),intent(in) :: tol
        type(VF) :: x,y,z
        integer :: i,j,k,t
        call check(m,'init_i_Nodes_general')
        call init(x,ind%s*8); call delete(Ux)
        call init(y,ind%s*8); call delete(Uy)
        call init(z,ind%s*8); call delete(Uz)
        k = 1
        do t=1,ind%s; do j=1,8
          i = ind%i(t)
          call assign(x,m%c(i)%n(j)%h(1),k)
          call assign(y,m%c(i)%n(j)%h(2),k)
          call assign(z,m%c(i)%n(j)%h(3),k)
          k = k+1
        enddo; enddo
        call unique(Ux,Uy,Uz,x,y,z,tol)
        call delete(x)
        call delete(y)
        call delete(z)
      end subroutine

      subroutine init_i_Faces_general(m,ind,Ux,Uy,Uz)
        implicit none
        type(mesh),intent(in) :: m
        type(VI),intent(in) :: ind
        type(VF),dimension(3) :: Ux,Uy,Uz
        type(VF) :: x,y,z
        integer :: i,j,k
        call check(m,'init_N_Faces_general')
        call init(x,ind%s*2); do i=1,3; call delete(Ux(i)); enddo
        call init(y,ind%s*2); do i=1,3; call delete(Uy(i)); enddo
        call init(z,ind%s*2); do i=1,3; call delete(Uz(i)); enddo
        k = 1
        do j=1,ind%s
          i = ind%i(j)
          call assign(x,m%c(i)%f(1)%h(1),k); call assign(y,m%c(i)%f(1)%h(2),k)
          call assign(z,m%c(i)%f(1)%h(3),k); k = k+1
          call assign(x,m%c(i)%f(2)%h(1),k); call assign(y,m%c(i)%f(2)%h(2),k)
          call assign(z,m%c(i)%f(2)%h(3),k); k = k+1
        enddo
        call unique(Ux(1),Uy(1),Uz(1),x,y,z,10.0_cp**(-10.0_cp)); k = 1
        call init(x,ind%s*2)
        call init(y,ind%s*2)
        call init(z,ind%s*2)
        do j=1,ind%s
          i = ind%i(j)
          call assign(x,m%c(i)%f(3)%h(1),k); call assign(y,m%c(i)%f(3)%h(2),k)
          call assign(z,m%c(i)%f(3)%h(3),k); k = k+1
          call assign(x,m%c(i)%f(4)%h(1),k); call assign(y,m%c(i)%f(4)%h(2),k)
          call assign(z,m%c(i)%f(4)%h(3),k); k = k+1
        enddo
        call unique(Ux(2),Uy(2),Uz(2),x,y,z,10.0_cp**(-10.0_cp)); k = 1
        call init(x,ind%s*2)
        call init(y,ind%s*2)
        call init(z,ind%s*2)
        do j=1,ind%s
          i = ind%i(j)
          call assign(x,m%c(i)%f(5)%h(1),k); call assign(y,m%c(i)%f(5)%h(2),k)
          call assign(z,m%c(i)%f(5)%h(3),k); k = k+1
          call assign(x,m%c(i)%f(6)%h(1),k); call assign(y,m%c(i)%f(6)%h(2),k)
          call assign(z,m%c(i)%f(6)%h(3),k); k = k+1
        enddo
        call unique(Ux(3),Uy(3),Uz(3),x,y,z,10.0_cp**(-10.0_cp))
        call delete(x)
        call delete(y)
        call delete(z)
      end subroutine

      end module