      module domain_flat_mod
      use current_precision_mod
      use ordinates_mod
      implicit none

      private
      public :: domain_flat
      public :: init,delete,print,export

      public :: init_C,init_N
      public :: init_F,init_E

      type domain_flat
        type(ordinates) :: C,N                ! unique set of x,y,z locations for (C,N) data
        type(ordinates),dimension(3) :: F,E   ! unique set of x,y,z locations for (F,E) data
        ! (1,2,3) = (x,y,z)-face,edge
      end type

      interface init_C;  module procedure init_DF_C;     end interface
      interface init_N;  module procedure init_DF_N;     end interface
      interface init_F;  module procedure init_DF_F;     end interface
      interface init_E;  module procedure init_DF_E;     end interface
      interface init;    module procedure init_copy_DF;  end interface
      interface delete;  module procedure delete_DF;     end interface
      interface print;   module procedure print_DF;      end interface
      interface export;  module procedure export_DF;     end interface

      contains

      subroutine init_DF_C(DF,C)
        implicit none
        type(domain_flat),intent(inout) :: DF
        type(ordinates),intent(in) :: C
        call init(DF%C,C)
      end subroutine

      subroutine init_DF_N(DF,N)
        implicit none
        type(domain_flat),intent(inout) :: DF
        type(ordinates),intent(in) :: N
        call init(DF%N,N)
      end subroutine

      subroutine init_DF_F(DF,F,dir)
        implicit none
        type(domain_flat),intent(inout) :: DF
        type(ordinates),intent(in) :: F
        integer,intent(in) :: dir
        call init(DF%F(dir),F)
      end subroutine

      subroutine init_DF_E(DF,E,dir)
        implicit none
        type(domain_flat),intent(inout) :: DF
        type(ordinates),intent(in) :: E
        integer,intent(in) :: dir
        call init(DF%E(dir),E)
      end subroutine

      subroutine init_copy_DF(a,b)
        implicit none
        type(domain_flat),intent(inout) :: a
        type(domain_flat),intent(in) :: b
        integer :: i
        call init(a%C,b%C)
        call init(a%N,b%N)
        do i=1,3; call init(a%F(i),b%F(i)); enddo
        do i=1,3; call init(a%E(i),b%E(i)); enddo
      end subroutine

      subroutine delete_DF(DF)
        implicit none
        type(domain_flat),intent(inout) :: DF
        integer :: i
        call delete(DF%C)
        call delete(DF%N)
        do i=1,3; call delete(DF%F(i)); enddo
        do i=1,3; call delete(DF%E(i)); enddo
      end subroutine

      subroutine print_DF(DF)
        implicit none
        type(domain_flat),intent(in) :: DF
        call export(DF,6)
      end subroutine

      subroutine export_DF(DF,un)
        implicit none
        type(domain_flat),intent(in) :: DF
        integer,intent(in) :: un
        integer :: i
        call export(DF%C,un)
        call export(DF%N,un)
        do i=1,3; call export(DF%F(i),un); enddo
        do i=1,3; call export(DF%E(i),un); enddo
      end subroutine

      end module