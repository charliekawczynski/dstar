      module mesh_geometries_mod
      use current_precision_mod
      use cell_mod
      use mesh_mod
      implicit none

      private
      public :: init_cube
      public :: init_BC_geometry
      public :: straight_duct

      contains

      subroutine init_cube(m)
        implicit none
        type(mesh),intent(inout) :: m
        type(cell) :: c
        integer :: i,j
        call delete(m)
        call init(c,1,(/(0.0_cp,i=1,3)/),(/(1.0_cp,j=1,3)/))
        call add(m,c)
        call refine_all(m,1)
        call delete(c)
      end subroutine

      subroutine init_BC_geometry(m)
        implicit none
        type(mesh),intent(inout) :: m
        type(cell) :: c
        integer :: i,j
        call delete(m)
        call init(c,1,(/(0.0_cp,i=1,3)/),(/(2.0_cp,j=1,3)/))
        call add(m,c)
        call refine_all(m,1)
        call init(c,m%total%C%s+1,(/2.0_cp,0.0_cp,0.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/0.0_cp,2.0_cp,0.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/0.0_cp,0.0_cp,2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/-2.0_cp,0.0_cp,0.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/0.0_cp,-2.0_cp,0.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/0.0_cp,0.0_cp,-2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)

        call init(c,m%total%C%s+1,(/0.0_cp,2.0_cp,2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/2.0_cp,0.0_cp,2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/2.0_cp,2.0_cp,0.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/0.0_cp,-2.0_cp,2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/-2.0_cp,0.0_cp,2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/-2.0_cp,2.0_cp,0.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/0.0_cp,2.0_cp,-2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/2.0_cp,0.0_cp,-2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/2.0_cp,-2.0_cp,0.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/0.0_cp,-2.0_cp,-2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/-2.0_cp,0.0_cp,-2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/-2.0_cp,-2.0_cp,0.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)

        call init(c,m%total%C%s+1,(/2.0_cp,2.0_cp,2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/-2.0_cp,2.0_cp,2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/2.0_cp,-2.0_cp,2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/2.0_cp,2.0_cp,-2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/2.0_cp,-2.0_cp,-2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/-2.0_cp,2.0_cp,-2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/-2.0_cp,-2.0_cp,2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call init(c,m%total%C%s+1,(/-2.0_cp,-2.0_cp,-2.0_cp/),(/(2.0_cp,j=1,3)/)); call add(m,c)
        call refine_all(m,2)
        call delete(c)
      end subroutine

      subroutine straight_duct(m)
        implicit none
        type(mesh),intent(inout) :: m
        type(cell) :: c
        call delete(m)
        call init(c,1,(/5.0_cp,0.0_cp,0.0_cp/),(/10.0_cp,1.0_cp,1.0_cp/)); call add(m,c)
        call refine_all(m,3)
        call delete(c)
      end subroutine

      subroutine refine_all(m,N_refinements)
        implicit none
        type(mesh),intent(inout) :: m
        integer,intent(in) :: N_refinements
        type(cell) :: c
        integer :: i,j,k
        if (N_refinements.gt.5) then
         write(*,*) 'Error: N_refinements must < 5 in refine_all in mesh_geometries.f90'
         stop 'Done'
        endif
        do j=1,N_refinements; do k=1,3; do i=1,m%total%C%s
        call refine(m%c(i),c,k,i); call add(m,c)
        enddo; enddo; enddo
        call delete(c)
      end subroutine

      end module