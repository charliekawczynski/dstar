      module mesh_flat_mod
      use current_precision_mod
      use domain_flat_mod
      implicit none

      private
      public :: mesh_flat
      public :: init,delete,print,export

      public :: init_T,init_I
      public :: init_B,init_G

      type mesh_flat
        type(domain_flat) :: total,interior,boundary,ghost
        ! x,y,z on the x-face of cell i is
        ! type(mesh_flat) :: MF
        ! (MF%total%F(1)%d(1)%f%f(i),
        !  MF%total%F(1)%d(2)%f%f(i),
        !  MF%total%F(1)%d(3)%f%f(i))
        ! on the y-face we have
        ! (MF%total%F(2)%d(1)%f%f(i),
        !  MF%total%F(2)%d(2)%f%f(i),
        !  MF%total%F(2)%d(3)%f%f(i))
      end type

      interface init_T;  module procedure init_MF_T;     end interface
      interface init_I;  module procedure init_MF_I;     end interface
      interface init_B;  module procedure init_MF_B;     end interface
      interface init_G;  module procedure init_MF_G;     end interface
      interface init;    module procedure init_copy_MF;  end interface
      interface delete;  module procedure delete_MF;     end interface
      interface print;   module procedure print_MF;      end interface
      interface export;  module procedure export_MF;     end interface

      contains

      subroutine init_MF_T(MF,T)
        implicit none
        type(mesh_flat),intent(inout) :: MF
        type(domain_flat),intent(in) :: T
        call init(MF%total,T)
      end subroutine

      subroutine init_MF_I(MF,I)
        implicit none
        type(mesh_flat),intent(inout) :: MF
        type(domain_flat),intent(in) :: I
        call init(MF%interior,I)
      end subroutine

      subroutine init_MF_B(MF,B)
        implicit none
        type(mesh_flat),intent(inout) :: MF
        type(domain_flat),intent(in) :: B
        call init(MF%boundary,B)
      end subroutine

      subroutine init_MF_G(MF,G)
        implicit none
        type(mesh_flat),intent(inout) :: MF
        type(domain_flat),intent(in) :: G
        call init(MF%ghost,G)
      end subroutine

      subroutine init_copy_MF(a,b)
        implicit none
        type(mesh_flat),intent(inout) :: a
        type(mesh_flat),intent(in) :: b
        call init(a%total,b%total)
        call init(a%interior,b%interior)
        call init(a%boundary,b%boundary)
        call init(a%ghost,b%ghost)
      end subroutine

      subroutine delete_MF(MF)
        implicit none
        type(mesh_flat),intent(inout) :: MF
        call delete(MF%total)
        call delete(MF%interior)
        call delete(MF%boundary)
        call delete(MF%ghost)
      end subroutine

      subroutine print_MF(MF)
        implicit none
        type(mesh_flat),intent(in) :: MF
        call export(MF,6)
      end subroutine

      subroutine export_MF(MF,un)
        implicit none
        type(mesh_flat),intent(in) :: MF
        integer,intent(in) :: un
        call export(MF%total,un)
        call export(MF%interior,un)
        call export(MF%boundary,un)
        call export(MF%ghost,un)
      end subroutine

      end module