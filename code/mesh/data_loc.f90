      module data_loc_mod
      use VI_mod
      implicit none

      private
      public :: data_loc
      public :: init,delete

      type data_loc
        type(VI) :: C,N,F,E
      end type

      contains

      subroutine init_copy_DL(a,b)
        implicit none
        type(data_loc),intent(inout) :: a
        type(data_loc),intent(in) :: b
        call init(a%C,b%C)
        call init(a%N,b%N)
        call init(a%F,b%F)
        call init(a%E,b%E)
      end subroutine

      subroutine delete_DL(DL)
        implicit none
        type(data_loc),intent(inout) :: DL
        call delete(DL%C)
        call delete(DL%N)
        call delete(DL%F)
        call delete(DL%E)
      end subroutine

      end module