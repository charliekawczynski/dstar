      module mesh_mod
      use current_precision_mod
      use IO_tools_mod
      use exp_Tecplot_Header_mod
      use cell_mod
      use VF_mod
      use VI_mod
      implicit none

      private
      public :: init_N_Nodes
      public :: init_N_Nodes_interior
      public :: init_N_Faces

      contains

      subroutine init_N_Nodes(m)
        implicit none
        type(mesh),intent(inout) :: m
        type(VF) :: x,y,z,Ux,Uy,Uz
        integer :: i,j,k
        call check(m,'init_N_Nodes')
        call init(x,m%N_cells*8)
        call init(y,m%N_cells*8)
        call init(z,m%N_cells*8)
        k = 1
        do i=1,m%N_cells; do j=1,8
          call assign(x,m%c(i)%n(j)%h(1),k)
          call assign(y,m%c(i)%n(j)%h(2),k)
          call assign(z,m%c(i)%n(j)%h(3),k)
          k = k+1
        enddo; enddo
        call unique(Ux,Uy,Uz,x,y,z,10.0_cp**(-10.0_cp))
        m%N_Nodes = Ux%s
        call delete(x); call delete(Ux)
        call delete(y); call delete(Uy)
        call delete(z); call delete(Uz)
      end subroutine

      subroutine init_N_Nodes_interior(m)
        implicit none
        type(mesh),intent(inout) :: m
        type(VF) :: x,y,z,Ux,Uy,Uz
        integer :: i,j,k,t
        call check(m,'init_N_Nodes_interior')
        if (.not.(m%i_interior%s.gt.0)) then
          write(*,*) 'Error: m%i_interior%s<1 in init_N_Nodes_interior in mesh.f90'
          write(*,*) 'm%i_interior%s = ',m%i_interior%s
          stop 'Done'
        endif
        call init(x,m%i_interior%s*8)
        call init(y,m%i_interior%s*8)
        call init(z,m%i_interior%s*8)
        k = 1
        do t=1,m%i_interior%s; do j=1,8
          i = m%i_interior%i(t)
          call assign(x,m%c(i)%n(j)%h(1),k)
          call assign(y,m%c(i)%n(j)%h(2),k)
          call assign(z,m%c(i)%n(j)%h(3),k)
          k = k+1
        enddo; enddo
        call unique(Ux,Uy,Uz,x,y,z,10.0_cp**(-10.0_cp))
        m%N_Nodes_interior = Ux%s
        call delete(x); call delete(Ux)
        call delete(y); call delete(Uy)
        call delete(z); call delete(Uz)
      end subroutine

      subroutine init_N_Faces(m)
        implicit none
        type(mesh),intent(inout) :: m
        type(VF) :: x,y,z,Ux,Uy,Uz
        integer :: i,j,k
        call check(m,'init_N_Faces')
        call init(x,m%N_cells*2)
        call init(y,m%N_cells*2)
        call init(z,m%N_cells*2)
        k = 1
        do i=1,m%N_cells
          call assign(x,m%c(i)%f(1)%h(1),k); call assign(y,m%c(i)%f(1)%h(2),k)
          call assign(z,m%c(i)%f(1)%h(3),k); k = k+1
          call assign(x,m%c(i)%f(2)%h(1),k); call assign(y,m%c(i)%f(2)%h(2),k)
          call assign(z,m%c(i)%f(2)%h(3),k); k = k+1
        enddo
        call unique(Ux,Uy,Uz,x,y,z,10.0_cp**(-10.0_cp)); k = 1
        m%N_Faces(1) = Ux%s
        call init(x,m%N_cells*2)
        call init(y,m%N_cells*2)
        call init(z,m%N_cells*2)
        do i=1,m%N_cells
          call assign(x,m%c(i)%f(3)%h(1),k); call assign(y,m%c(i)%f(3)%h(2),k)
          call assign(z,m%c(i)%f(3)%h(3),k); k = k+1
          call assign(x,m%c(i)%f(4)%h(1),k); call assign(y,m%c(i)%f(4)%h(2),k)
          call assign(z,m%c(i)%f(4)%h(3),k); k = k+1
        enddo
        call unique(Ux,Uy,Uz,x,y,z,10.0_cp**(-10.0_cp)); k = 1
        m%N_Faces(2) = Uy%s
        call init(x,m%N_cells*2)
        call init(y,m%N_cells*2)
        call init(z,m%N_cells*2)
        do i=1,m%N_cells
          call assign(x,m%c(i)%f(5)%h(1),k); call assign(y,m%c(i)%f(5)%h(2),k)
          call assign(z,m%c(i)%f(5)%h(3),k); k = k+1
          call assign(x,m%c(i)%f(6)%h(1),k); call assign(y,m%c(i)%f(6)%h(2),k)
          call assign(z,m%c(i)%f(6)%h(3),k); k = k+1
        enddo
        call unique(Ux,Uy,Uz,x,y,z,10.0_cp**(-10.0_cp))
        m%N_Faces(3) = Uz%s
        call delete(x); call delete(Ux)
        call delete(y); call delete(Uy)
        call delete(z); call delete(Uz)
      end subroutine

      end module