      module mesh_props_mod
      use current_precision_mod
      implicit none

      private
      public :: mesh_props
      public :: init,delete,print,export

      type mesh_props
        integer :: N_cells,N_Nodes
        integer,dimension(3) :: N_Faces,N_Edges
        real(cp) :: vol
      end type

      interface init;    module procedure init_MP;      end interface
      interface init;    module procedure copy_MP;      end interface
      interface delete;  module procedure delete_MP;    end interface
      interface export;  module procedure export_MP;    end interface
      interface print;   module procedure print_MP;     end interface

      contains

      subroutine init_MP(mp,N_cells,N_Nodes,N_Faces,N_Edges,vol)
        implicit none
        type(mesh_props),intent(inout) :: mp
        integer,intent(in) :: N_cells,N_Nodes
        integer,dimension(3),intent(in) :: N_Faces,N_Edges
        real(cp),intent(in) :: vol
        mp%N_cells = N_cells
        mp%N_Nodes = N_Nodes
        mp%N_Faces = N_Faces
        mp%N_Edges = N_Edges
        mp%vol = vol
      end subroutine

      subroutine copy_MP(a,b)
        implicit none
        type(mesh_props),intent(inout) :: a
        type(mesh_props),intent(in) :: b
        a%N_cells = b%N_cells
        a%N_Nodes = b%N_Nodes
        a%N_Faces = b%N_Faces
        a%N_Edges = b%N_Edges
        a%vol = b%vol
      end subroutine

      subroutine delete_MP(mp)
        implicit none
        type(mesh_props),intent(inout) :: mp
        mp%N_cells = 0
        mp%N_Nodes = 0
        mp%N_Faces = 0
        mp%N_Edges = 0
        mp%vol = 0.0_cp
      end subroutine

      subroutine print_MP(mp)
        implicit none
        type(mesh_props),intent(in) :: mp
        call export(mp,6)
      end subroutine

      subroutine export_MP(mp,un)
        implicit none
        type(mesh_props),intent(in) :: mp
        integer,intent(in) :: un
        write(un,*) 'N_cells,N_Nodes = ',mp%N_cells,mp%N_Nodes
        ! write(un,*) 'vol = ',mp%vol
        ! write(un,*) 'N_Faces = ',mp%N_Faces
        ! write(un,*) 'N_Edges = ',mp%N_Edges
      end subroutine

      end module