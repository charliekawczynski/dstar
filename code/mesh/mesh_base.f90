      module mesh_base_mod
      use current_precision_mod
      use IO_tools_mod
      use cell_mod
      use VF_mod
      use VI_mod
      implicit none

      private
      public :: mesh
      public :: init_all_mesh_props
      public :: init,add,delete
      public :: print,export
      public :: insert_cell
      public :: init_Node_ID

      integer,parameter :: FACES_PER_CELL = 6
      integer,parameter :: EDGES_PER_CELL = 12
      integer,parameter :: CORNERS_PER_CELL = 8

      type data_loc
        type(VI) :: C,N ! Cell-center,Node
        type(VI),dimension(3) :: F,E ! Face,Edge
        type(VF),dimension(3) :: hn
        type(VI) :: CN ! Cell-center corresponding to node
      end type

      interface init;    module procedure init_copy_DL;         end interface
      interface delete;  module procedure delete_DL;            end interface

      type mesh
        type(data_loc) :: total,interior,ghost
        type(VI) :: i_connectivity
        type(cell),dimension(:),allocatable :: c
        logical :: i_defined
      end type

      interface init;    module procedure init_mesh;            end interface
      interface init;    module procedure init_copy;            end interface
      interface add;     module procedure add_cell_mesh;        end interface
      interface delete;  module procedure delete_mesh;          end interface
      interface print;   module procedure print_mesh;           end interface
      interface export;  module procedure export_mesh;          end interface
      interface check;   module procedure check_mesh;           end interface
      interface insert_cell; module procedure insert_cell_mesh; end interface

      contains

      subroutine init_mesh(m)
        implicit none
        type(mesh),intent(inout) :: m
        call delete(m)
      end subroutine

      subroutine add_cell_mesh(m,c)
        implicit none
        type(mesh),intent(inout) :: m
        type(cell),intent(in) :: c
        type(mesh) :: temp
        integer :: i,N_cells
        if (allocated(m%c)) then
          call init(temp,m)
          N_cells = m%total%C%s
          call delete(m)
          m%total%C%s = N_cells+1
          allocate(m%c(m%total%C%s))
          do i=1,N_cells; call init(m%c(i),temp%c(i)); enddo
          call init(m%c(m%total%C%s),c)
          call delete(temp)
        else
          call delete(m)
          m%total%C%s = 1
          allocate(m%c(1))
          call init(m%c(1),c)
        endif
      end subroutine

      subroutine insert_cell_mesh(m,c,ID)
        ! Needs to be developed
        implicit none
        type(mesh),intent(inout) :: m
        type(cell),intent(in) :: c
        integer,intent(in) :: ID
        type(mesh) :: temp
        integer :: i,N_cells
        if (allocated(m%c)) then
          call init(temp,m)
          N_cells = m%total%C%s
          call delete(m)
          m%total%C%s = N_cells+1
#ifdef _DEBUG_MESH_
          call check_bounds(m,ID,'insert_cell_before_mesh')
#endif
          allocate(m%c(m%total%C%s))
          do i=1,ID-1; call init(m%c(i),temp%c(i)); enddo
          call init(m%c(ID),c)
          do i=ID+1,m%total%C%s; call init(m%c(i),temp%c(i-1)); enddo
          call delete(temp)
        else
          call delete(m)
          m%total%C%s = 1
          allocate(m%c(1))
          call init(m%c(1),c)
        endif
      end subroutine

      subroutine init_copy(m,n)
        implicit none
        type(mesh),intent(inout) :: m
        type(mesh),intent(in) :: n
        integer :: i
        call check(n,'init_copy')
        call delete(m)
        allocate(m%c(n%total%C%s))
        do i=1,n%total%C%s; call init(m%c(i),n%c(i)); enddo
        if (n%i_defined) then
          call init(m%interior%C,n%interior%C)
          call init(m%ghost%C,n%ghost%C)
          call init(m%total%C,n%total%C)
        endif
        call init(m%total,n%total)
        call init(m%interior,n%interior)
        call init(m%ghost,n%ghost)
        m%total%C%s = n%total%C%s
      end subroutine

      subroutine check_bounds(m,ID,s)
        implicit none
        type(mesh),intent(in) :: m
        integer,intent(in) :: ID
        character(len=*),intent(in) :: s
        if ((ID.lt.1).or.(ID.gt.m%total%C%s)) then
          write(*,*) 'Error: ID is not within bounds of mesh cells in '//s//' in mesh.f90'
          stop 'Done'
        endif
      end subroutine

      subroutine check_mesh(m,N_cells)
        implicit none
        type(mesh),intent(in) :: m
        character(len=*),intent(in) :: N_cells
        if (.not.allocated(m%c)) then
        write(*,*) 'Error: need allocated mesh in ',N_cells,' in mesh.f90'
        stop
        endif
      end subroutine

      subroutine delete_mesh(m)
        implicit none
        type(mesh),intent(inout) :: m
        integer :: i
        if (allocated(m%c)) then
          do i=1,m%total%C%s; call delete(m%c(i)); enddo
          ! if (m%total%C%s.gt.1) then
          ! else; write() 'Error: attempting to delete allocated mesh with N_cells<1 in mesh.f90'
          ! endif
        endif
        call delete(m%total)
        call delete(m%interior)
        call delete(m%ghost)
        if (allocated(m%c)) deallocate(m%c)
        m%i_defined = .false.
      end subroutine

      subroutine print_mesh(m)
        implicit none
        type(mesh),intent(in) :: m
        call export(m,6)
      end subroutine

      subroutine export_mesh(m,un)
        implicit none
        type(mesh),intent(in) :: m
        integer,intent(in) :: un
        integer :: j
        write(un,*) ' -------------------- mesh -------------------- '
        write(un,*) ' C DATA'
        write(un,*) ' Total,interior,ghost:    ',m%total%C%s,m%interior%C%s,m%ghost%C%s
        write(un,*) ' N DATA'
        write(un,*) ' Total,interior,ghost:    ',m%total%N%s,m%interior%N%s,m%ghost%N%s

        ! write(un,*) ' TOTAL '
        ! write(un,*) ' C,N,F,E:    ',m%total%C%s,m%total%N%s,m%total%F%s,m%total%E%s
        ! write(un,*) ' INTERIOR '
        ! write(un,*) ' C,N,F,E:    ',m%interior%C%s,m%interior%N%s,m%interior%F%s,m%interior%E%s
        ! write(un,*) ' GHOST '
        ! write(un,*) ' C,N,F,E:    ',m%ghost%C%s,m%ghost%N%s,m%ghost%F%s,m%ghost%E%s
        ! do j=1,m%interior%C%s; call export(m%c(m%interior%C%i(j)),un); enddo
        ! do j=1,m%total%C%s; call export(m%c(j),un); enddo
        ! do j=1,m%total%C%s
          ! write(un,*) 'Cell,ghost: ',j,m%c(j)%ghost
          ! call export(m%c(j),un)
        ! enddo
        ! write(un,*) 'interior%C = ',m%interior%C%i
      end subroutine

      end module