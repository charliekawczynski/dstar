      module VF_unique_mod
      use current_precision_mod
      use VF_base_mod
      use VF_insert_mod
      use VF_assign_mod
      implicit none

      private
      public :: unique

      interface unique;       module procedure unique_coord_VF; end interface

      contains

      subroutine unique_coord_VF(Ux,Uy,Uz,x,y,z,tol)
        implicit none
        type(VF),intent(inout) :: Ux,Uy,Uz
        type(VF),intent(in) :: x,y,z
        real(cp),intent(in) :: tol
        logical :: new_point
        integer :: i
        call delete(Ux); call delete(Uy); call delete(Uz)
        if (x%s.gt.0) then
#ifdef _DEBUG_VF_
          call check_allocated(x,'unique_coord_VF')
          call check_allocated(y,'unique_coord_VF')
          call check_allocated(z,'unique_coord_VF')
#endif
          call append(Ux,x%f(1))
          call append(Uy,y%f(1))
          call append(Uz,z%f(1))
          do i=1,x%s
            new_point = .not.point_in_set(Ux,Uy,Uz,(/x%f(i),y%f(i),z%f(i)/),tol)
            if (new_point) then
              call append(Ux,x%f(i))
              call append(Uy,y%f(i))
              call append(Uz,z%f(i))
            endif
          enddo
        endif
      end subroutine

      function same_point(x,y,tol) result(TF_all)
        implicit none
        real(cp),dimension(3),intent(in) :: x,y
        real(cp),intent(in) :: tol
        logical,dimension(3) :: TF
        logical :: TF_all
        TF(1) = abs(x(1) - y(1)).lt.tol
        TF(2) = abs(x(2) - y(2)).lt.tol
        TF(3) = abs(x(3) - y(3)).lt.tol
        TF_all = all(TF)
      end function

      function point_in_set(x,y,z,p,tol) result(TF)
        implicit none
        type(VF),intent(in) :: x,y,z
        real(cp),intent(in) :: tol
        real(cp),dimension(3),intent(in) :: p
        logical :: TF
        integer :: j
        TF = .false.
        do j=1,x%s
          if (same_point((/x%f(j),y%f(j),z%f(j)/),p,tol)) TF = .true.
        enddo
      end function

      end module