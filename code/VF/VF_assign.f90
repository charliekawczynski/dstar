      module VF_assign_mod
      use current_precision_mod
      use VF_base_mod
      implicit none

      private
      public :: assign

      interface assign;       module procedure assign_VF_C;       end interface
      interface assign;       module procedure assign_VF_VF;      end interface
      interface assign;       module procedure assign_VF_element; end interface
      interface assign;       module procedure assign_VF_VF_i_i;  end interface
      interface assign;       module procedure assign_VF_VF_all;  end interface

      contains

      subroutine assign_VF_C(A,B,i,s)
        implicit none
        type(VF),intent(inout) :: A
        real(cp),intent(in) :: B
        integer,intent(in) :: s
        integer,dimension(s),intent(in) :: i
        integer :: j
        do j=1,s
#ifdef _DEBUG_VF_
          call check_bounds(A,i(j),'assign_VF_VF')
          call check_size(i,s,'assign_VF_VF')
#endif
          A%f(i(j)) = B
        enddo
      end subroutine

      subroutine assign_VF_element(A,B,i)
        implicit none
        type(VF),intent(inout) :: A
        real(cp),intent(in) :: B
        integer,intent(in) :: i
#ifdef _DEBUG_VF_
        call check_bounds(A,i,'assign_VF_element')
#endif
        A%f(i) = B
      end subroutine

      subroutine assign_VF_VF(A,B,i,s)
        implicit none
        type(VF),intent(inout) :: A
        type(VF),intent(in) :: B
        integer,dimension(s),intent(in) :: i
        integer,intent(in) :: s
        integer :: j
        do j=1,s
#ifdef _DEBUG_VF_
          call check_bounds(A,i(j),'assign_VF_VF')
          call check_bounds(B,i(j),'assign_VF_VF')
#endif
          A%f(i(j)) = B%f(i(j))
        enddo
      end subroutine

      subroutine assign_VF_VF_i_i(A,B,i_A,i_B,s)
        implicit none
        type(VF),intent(inout) :: A
        type(VF),intent(in) :: B
        integer,dimension(s),intent(in) :: i_A,i_B
        integer,intent(in) :: s
        integer :: j
        do j=1,s
#ifdef _DEBUG_VF_
          call check_bounds(A,i_A(j),'assign_VF_VF')
          call check_bounds(B,i_B(j),'assign_VF_VF')
#endif
          A%f(i_A(j)) = B%f(i_B(j))
        enddo
      end subroutine

      subroutine assign_VF_VF_all(A,B)
        implicit none
        type(VF),intent(inout) :: A
        type(VF),intent(in) :: B
        integer :: j
        do j=1,B%s
#ifdef _DEBUG_VF_
          call check_bounds(A,j,'assign_VF_VF_all')
          call check_bounds(B,j,'assign_VF_VF_all')
#endif
          A%f(j) = B%f(j)
        enddo
      end subroutine

      end module