      module VF_base_mod
      ! Compiler flags: (_DEBUG_VF_)
      use current_precision_mod
      implicit none

      private
      public :: VF
      public :: init,delete,export,import,display,print

      public :: check_allocated,check_bounds,check_size
      public :: swap

      type VF 
        integer :: s
        real(cp),dimension(:),allocatable :: f
      end type

      interface init;              module procedure init_size;            end interface
      interface init;              module procedure init_array;           end interface
      interface init;              module procedure init_copy;            end interface
      interface delete;            module procedure delete_VF;            end interface
      interface export;            module procedure export_VF;            end interface
      interface import;            module procedure import_VF;            end interface
      interface print;             module procedure print_VF;             end interface
      interface display;           module procedure display_VF;           end interface

      interface check_bounds;      module procedure check_bounds_VF;      end interface
      interface check_allocated;   module procedure check_allocated_VF;   end interface
      interface check_size;        module procedure check_size_VF;        end interface

      interface swap;              module procedure swap_VF;              end interface

      contains

      subroutine init_size(v,s)
        implicit none
        type(VF),intent(inout) :: v
        integer,intent(in) :: s
#ifdef _DEBUG_VF_
        if (s.lt.1) then
          write(*,*) 'Error: cannot init array of size<1 in init_size in VF_base.f90'
          write(*,*) 's = ',s
          stop 'Done'
        endif
#endif
        call delete(v)
        v%s = s
        allocate(v%f(s))
      end subroutine

      subroutine init_array(v,f,s)
        implicit none
        type(VF),intent(inout) :: v
        real(cp),dimension(s),intent(in) :: f
        integer,intent(in) :: s
#ifdef _DEBUG_VF_
        if (size(f).ne.s) stop 'Error: size(f) mismatch in init_size in VF_base.f90'
#endif
        call init(v,s)
        v%f = f
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(VF),intent(inout) :: a
        type(VF),intent(in) :: b
#ifdef _DEBUG_VF_
        call check_allocated(b,'init_copy')
#endif
        call delete(a)
        allocate(a%f(b%s))
        a%s = b%s
        a%f = b%f
      end subroutine

      subroutine delete_VF(v)
        implicit none
        type(VF),intent(inout) :: v
        if (allocated(v%f)) deallocate(v%f)
        v%s = 0
      end subroutine

      subroutine export_VF(v,un)
        implicit none
        type(VF),intent(in) :: v
        integer,intent(in) :: un
        integer :: j
#ifdef _DEBUG_VF_
        call check_allocated(v,'export_VF')
#endif
        write(un,*) 'v%s = '; write(un,*) v%s
        do j=1,v%s; write(un,*) v%f(j); enddo
      end subroutine

      subroutine import_VF(v,un)
        implicit none
        type(VF),intent(inout) :: v
        integer,intent(in) :: un
        integer :: j,s
        call delete(v)
        read(un,*); read(un,*) s
        call init(v,s)
        do j=1,v%s; read(un,*) v%f(j); enddo
      end subroutine

      subroutine display_VF(v,un)
        implicit none
        type(VF),intent(in) :: v
        integer,intent(in) :: un
        integer :: j
#ifdef _DEBUG_VF_
        call check_allocated(v,'export_VF')
#endif
        write(un,*) 'v%s = ',v%s
        do j=1,v%s; write(un,*) 'v(',j,') = ',v%f(j); enddo
      end subroutine

      subroutine print_VF(v)
        implicit none
        type(VF),intent(in) :: v
        call display(v,6)
      end subroutine

      subroutine check_allocated_VF(v,s)
        implicit none
        type(VF),intent(in) :: v
        character(len=*),intent(in) :: s
        if (.not.allocated(v%f)) then
          write(*,*) 'Error: need allocated VF in ',s,' in VF_base.f90'
          stop 'Done'
        endif
      end subroutine

      subroutine check_bounds_VF(v,i,s)
        implicit none
        type(VF),intent(in) :: v
        integer,intent(in) :: i
        character(len=*),intent(in) :: s
        call check_allocated(v,s)
        if (.not.((i.le.v%s).and.(i.ge.1))) then
          write(*,*) 'Error: VF index bound violated in ',s,' in VF_base.f90'
          write(*,*) '(i.ge.1) = ',(i.ge.1)
          write(*,*) '(i.le.v%s) = ',(i.le.v%s)
          write(*,*) 'i = ',i
          write(*,*) 'v%s = ',v%s
          stop 'Done'
        endif
      end subroutine

      subroutine check_size_VF(i,s,st)
        ! Is this routine flawed? Will specifying the dimension
        ! force the dimension?
        implicit none
        integer,dimension(s),intent(in) :: i
        integer,intent(in) :: s
        character(len=*),intent(in) :: st
        if (size(i).ne.s) then
          write(*,*) 'Error: bad index size match in ',st,' in VF_base.f90'
          write(*,*) 'i = ',i
          write(*,*) 's = ',s
          stop 'Done'
        endif
      end subroutine

      subroutine swap_VF(v,i,j)
        implicit none
        type(VF),intent(inout) :: v
        integer,intent(in) :: i,j
        real(cp) :: temp
#ifdef _DEBUG_VF_
        call check_allocated(v,'swap_VF')
        call check_bounds(v,i,'swap_VF i')
        call check_bounds(v,j,'swap_VF j')
#endif
        temp = v%f(i)
        v%f(i) = v%f(j)
        v%f(j) = temp
      end subroutine

      end module