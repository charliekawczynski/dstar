      module VF_mod
      use VF_base_mod
      use VF_assign_mod
      use VF_add_mod
      use VF_sum_mod
      use VF_norm_L2_mod
      use VF_insert_mod
      use VF_unique_mod
      implicit none

      private
      public :: VF,init,delete,export,print ! from VF_base_mod
      public :: assign
      public :: add
      public :: sum
      public :: norm_L2
      public :: insert
      public :: insert_unique
      public :: append
      public :: swap
      public :: unique

      end module