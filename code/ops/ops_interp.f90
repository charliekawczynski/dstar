      module ops_interp_mod
      ! Compiler flags: (_DEBUG_INTERP_)
      use current_precision_mod
      use VI_mod
      use VF_mod
      implicit none

      private
      public :: interp

      interface interp;       module procedure interp_O2;         end interface
      interface interp;       module procedure interp_dir;        end interface

      contains

      subroutine interp_dir(A,B,m,dir)
        ! We seek to occupy a value in A
        ! that is interpolated from B.
        implicit none
        type(VF),intent(inout) :: A
        type(VF),intent(in) :: B
        type(mesh),intent(in) :: m
        integer :: j
        if (A%is_CC) then
        elseif (A%is_N) then
        elseif (A%is_F) then
        elseif (A%is_E) then
        else; stop 'Error: no datatype found in interp in ops_interp.f90'
        endif
        do j=1,A%s
          A%i(j) = B
        enddo
      end subroutine

      subroutine F2C_VF(A,B,m)
        implicit none
        type(VF),intent(inout) :: A
        type(VF),intent(in) :: B
        type(mesh),intent(in) :: m
        integer :: i,j
#ifdef _DEBUG_INTERP_
        if (.not.A%is_C) stop 'Error: bad input (1) to F2C_VF in interp.f90'
        if (.not.B%is_F) stop 'Error: bad input (2) to F2C_VF in interp.f90'
#endif
        call assign(A,0.0_cp)
        do i=1,A%s
          do j=1,m%F%neighbors(i)%s
            A%f(i) = A%f(i) + B%f(m%F%neighbors(i)%i(j))*m%dA_j%f(j)
          enddo
        enddo

        do i=1,A%s; A%f(i) = sum((/B%f(j)*m%dA_j%f(j),j=1,m%dA_j%s/)); enddo

      end subroutine

      end module