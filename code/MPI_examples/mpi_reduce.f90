	! from http://mpi-forum.org/docs/mpi-1.1/mpi-11-html/node78.html

	! Example:
	! A routine that computes the dot product of two vectors that 
	! are distributed across a group of processes and returns the 
	! answer at node zero.

	subroutine par_blas1(m, a, b, c, comm) 
	real a(m), b(m)       ! local slice of array 
	real c                ! result (at node zero) 
	real sum 
	integer m, comm, i, ierr 
	! local sum 
	sum = 0.0 
	do i = 1, m 
	   sum = sum + a(i)*b(i) 
	end do 
	! global sum 
	call mpi_reduce(sum, c, 1, mpi_real, mpi_sum, 0, comm, ierr) 
	end subroutine

	! Example:
	! a routine that computes the product of a vector and an array 
	! that are distributed across a group of processes and returns 
	! the answer at node zero.

	subroutine par_blas2(m, n, a, b, c, comm) 
	real a(m), b(m,n)    ! local slice of array 
	real c(n)            ! result 
	real sum(n) 
	integer n, comm, i, j, ierr 
	! local sum 
	do j= 1, n 
	  sum(j) = 0.0 
	  do i = 1, m 
	    sum(j) = sum(j) + a(i)*b(i,j) 
	  end do 
	end do 
	! global sum 
	call mpi_reduce(sum, c, n, mpi_real, mpi_sum, 0, comm, ierr) 
	end subroutine
	! return result at node zero (and garbage at the other nodes) 
