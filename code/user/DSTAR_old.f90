       module DSTAR_mod
       use current_precision_mod
       use cell_mod
       use mesh_mod
       use mesh_geometries_mod
       use omp_lib
       use VF_mod
       use IO_VF_mod

       implicit none

       private
       public :: DSTAR

       contains
 
       subroutine DSTAR(dir,dir_full)
         implicit none
         character(len=*),intent(in) :: dir ! Output directory
         character(len=*),intent(in) :: dir_full
         type(mesh) :: m
         type(cell) :: c
         type(VF) :: v
         real(cp) :: hmin,hmax
         real(cp),dimension(3) :: h,dh
         integer :: i,k
         integer,dimension(3) :: s
         hmin = -1.0_cp; hmax = 1.0_cp
         write(*,*) 'Got here 0'

         ! call init_cube(m)
         ! call init_BC_geometry(m)
         call straight_duct(m)

         write(*,*) 'Got here 0.5'

         ! i = 26 ! last mesh
         ! i = 22
         ! do k=1,3
         !   call refine(m%c(i),c,1,i); call insert_cell(m,c,i)
         !   call refine(m%c(i),c,2,i); call insert_cell(m,c,i)
         ! enddo

         write(*,*) 'Got here 1'
         call init_all_mesh_props(m)
         write(*,*) 'Got here 2'
         call init(v,m%total%C%s)
         write(*,*) 'Got here 3'

         ! i = 26
         ! do k=1,3
         !   call refine(m%c(i),c,1,i); call insert_cell(m,c,i)
         !   call refine(m%c(i),c,2,i); call insert_cell(m,c,i)
         !   call insert_element(v,real(i,cp),i)
         !   call insert_element(v,real(i,cp),i)
         ! enddo
         write(*,*) 'Got here 4'
         ! call init_Node_ID(m,m%total%C,m%total%hn)
         call init(v,m%total%N%s)

         write(*,*) 'Got here 5'
         ! call assign(v,0.0_cp,m%total%C%i,m%total%C%s)
         ! call assign(v,8.0_cp,m%ghost%C%i,m%ghost%C%s)
         ! call assign(v,10.0_cp,26)
         ! call assign(v,40.0_cp,27)
         write(*,*) 'Got here 6'
         call print(m)
         write(*,*) 'Got here 7'
         write(*,*) 's%v = ',v%s
         ! call print(m%c(26))

         call export(m,v,m%total%C,'out/','v')
         ! export_VF_new(m,x,y,z,v,dir,name)
         call export(m,m%total%hn(1),m%total%hn(2),m%total%hn(3),v,m%total%CN,'out/','v_new')
         call delete(v)
         call delete(c)
         call delete(m)
       end subroutine

       end module
