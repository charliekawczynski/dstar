       module DSTAR_mod
       use current_precision_mod
       use IO_tools_mod
       use stop_clock_mod
       use vec_OMP_mod
       use mpi_obj_mod
       use vec_mod ! MPI capable

       implicit none

       private
       public :: DSTAR

       contains
 
       subroutine DSTAR(dir)
         implicit none
         character(len=*),intent(in) :: dir
         call DSTAR_MPI(dir)
       end subroutine

       subroutine DSTAR_MPI(dir)
         implicit none
         character(len=*),intent(in) :: dir
         type(vec) :: x,y
         type(stop_clock) :: sc
         real(cp) :: a,b
         integer :: N,un,i_err
         integer(li) :: t,N_t
         type(mpi_obj) :: m
         call init(m)
         if (master(m)) call print(m)

         a = 1.0_cp
         b = 2.0_cp
         N_t = 10
         N = 3
         if (master(m)) call init(sc,N_t,'out/','WALL_CLOCK_TIME_INFO_MPI')

         write(*,*) 'GH 1'
         call init(y,m,'out/','y')
         call init(x,m,'out/','x')
         write(*,*) 'GH 2'
         call init(y,1,N)
         call init(x,1,N)
         write(*,*) 'GH 3'
         call init(x,2,N)
         call init(y,2,N)
         write(*,*) 'GH 4'

         call assign(x,1,a)
         call assign(x,2,b)
         write(*,*) 'GH 5'
         
         call mpi_barrier(MCW(m),i_err)
         write(*,*) 'GH 5.0'
         call distribute_int(m,un)
         write(*,*) 'GH 5.1'
         call export(x)
         call mpi_barrier(MCW(m),i_err)

         write(*,*) 'GH 5.2'
         call print(x)
         call push(x,1,2)
         call print(x)
         call mpi_barrier(MCW(m),i_err)
         write(*,*) 'GH 5.3'

         ! if (master(m)) un = new_and_open('out/','x_after')
         ! call export(x,un)
         ! if (master(m)) call close_and_message(un,'out/','x_after')

         write(*,*) 'GH 5.4'
         call assign(y,b)
         write(*,*) 'GH 6'
         call mpi_barrier(MCW(m),i_err)
         do t=1,N_t
           if (master(m)) call tic(sc)
           call assign(x,y)
           if (master(m)) call toc(sc)
           if (mod(t,N_t/10).eq.1) then; write(*,*) 'Iterations_MPI',t; endif
         enddo
         write(*,*) 'GH 7'
         call mpi_barrier(MCW(m),i_err)
         if (master(m)) call print(sc)
         call mpi_barrier(MCW(m),i_err)
         write(*,*) 'GH 8'

         call delete(x)
         write(*,*) 'GH 9'
         call delete(y)
         write(*,*) 'GH 10'
         call delete(sc)
         write(*,*) 'GH 11'
         call delete(m)
         write(*,*) 'GH 12'
       end subroutine

       end module
