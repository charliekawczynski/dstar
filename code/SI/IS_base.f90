      module IS_base_mod
      ! Compiler flags: (_DEBUG_IS_)
      use current_precision_mod
      implicit none

      private
      public :: IS
      public :: init,delete,export,import,display,print

      public :: swap,size,defined

      public :: insist_allocated,insist_bounds
 
      type IS
        integer,private :: N
        integer,dimension(:),allocatable,private :: i
        logical,private :: defined
      end type

      interface init;              module procedure init_size;            end interface
      interface init;              module procedure init_array;           end interface
      interface init;              module procedure init_copy;            end interface
      interface delete;            module procedure delete_IS;            end interface
      interface export;            module procedure export_IS;            end interface
      interface import;            module procedure import_IS;            end interface
      interface display;           module procedure display_IS;           end interface
      interface print;             module procedure print_IS;             end interface

      interface swap;              module procedure swap_IS;              end interface
      interface size;              module procedure size_IS;              end interface
      interface defined;           module procedure defined_IS;           end interface

      interface insist_bounds;     module procedure insist_bounds_IS;     end interface
      interface insist_allocated;  module procedure insist_allocated_IS;  end interface
      interface check_allocated;   module procedure check_allocated_IS;   end interface
      interface insist_same_size;  module procedure insist_same_size_IS;  end interface

      contains

      subroutine init_size(i,N)
        implicit none
        type(IS),intent(inout) :: i
        integer,intent(in) :: N
#ifdef _DEBUG_IS_
        if (N.lt.1) stop 'Error: cannot init array of size<1 in init_size in IS_base.f90'
#endif
        call delete(i)
        i%N = N
        allocate(i%s(N))
        i%defined = .true.
      end subroutine

      subroutine init_set(i,s,N)
        implicit none
        type(IS),intent(inout) :: i
        integer,dimension(N),intent(in) :: s
        integer,intent(in) :: N
#ifdef _DEBUG_IS_
        if (size(s).ne.N) stop 'Error: size(s) mismatch in init_size in IS_base.f90'
#endif
        call init(i,N)
        i%s = s
        i%defined = .true.
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(IS),intent(inout) :: a
        type(IS),intent(in) :: b
#ifdef _DEBUG_IS_
        call check_allocated(a,'init_copy')
#endif
        if (allocated(b%s)) then
          call delete(a)
          allocate(a%s(b%N))
          a%N = b%N
          a%s = b%s
          a%defined = b%defined
        else
          a%defined = .false.
          b%defined = .false.
        endif
      end subroutine

      subroutine delete_IS(i)
        implicit none
        type(IS),intent(inout) :: i
        if (allocated(i%s)) deallocate(i%s)
        i%N = 0
        i%defined = .false.
      end subroutine

      subroutine export_IS(i,un)
        implicit none
        type(IS),intent(in) :: i
        integer,intent(in) :: un
        integer :: j
#ifdef _DEBUG_IS_
        call insist_allocated(i,'export_IS')
#endif
        write(un,*) 'i%N = '
        write(un,*) i%N
        do j=1,i%N; write(un,*) i%s(j); enddo
      end subroutine

      subroutine import_IS(i,un)
        implicit none
        type(IS),intent(inout) :: i
        integer,intent(in) :: un
        integer :: j,N
        read(un,*) 
        read(un,*) N
        call init(i,N)
        do j=1,i%N; read(un,*) i%s(j); enddo
      end subroutine

      subroutine display_IS(i,un)
        implicit none
        type(IS),intent(in) :: i
        integer,intent(in) :: un
        integer :: j
#ifdef _DEBUG_IS_
        call insist_allocated(i,'export_IS')
#endif
        write(un,*) 'i%N = ',i%N
        do j=1,i%N; write(un,*) 'i%s(',j,') = ',i%s(j); enddo
      end subroutine

      subroutine print_IS(i)
        implicit none
        type(IS),intent(in) :: i
        call display(i,6)
      end subroutine

      subroutine swap_IS(i,j,k)
        implicit none
        type(IS),intent(inout) :: i
        integer,intent(in) :: j,k
        integer :: temp
#ifdef _DEBUG_IS_
        call insist_allocated(i,'swap_IS')
        call insist_bounds(i,j,'swap_IS j')
        call insist_bounds(i,k,'swap_IS k')
#endif
        temp = i%s(j)
        i%s(j) = i%s(k)
        i%s(k) = temp
      end subroutine

      pure function size_IS(i) result(s)
        implicit none
        type(IS),intent(in) :: i
        integer :: s
        s = i%N
      end function

      pure function defined_IS(i) result(L)
        implicit none
        type(IS),intent(in) :: i
        logical :: L
        L = i%defined
      end function

      subroutine insist_allocated_IS(i,caller)
        implicit none
        type(IS),intent(in) :: i
        character(len=*),intent(in) :: caller
        if (.not.allocated(i%s)) then
          write(*,*) 'Error: need allocated IS in ',caller,' in IS_base.f90'
          write(*,*) 'i%N = ',i%N
          stop 'Done'
        endif
      end subroutine

      subroutine check_allocated_IS(i,caller)
        implicit none
        type(IS),intent(in) :: i
        character(len=*),intent(in) :: caller
        if (.not.allocated(i%s)) then
          write(*,*) '------------------------------------------------------'
          write(*,*) 'WARNING: IS not allocated in ',caller,' in IS_base.f90'
          write(*,*) '------------------------------------------------------'
        endif
      end subroutine

      subroutine insist_bounds_IS(i,j,caller)
        implicit none
        type(IS),intent(in) :: i
        integer,intent(in) :: j
        character(len=*),intent(in) :: caller
        call insist_allocated(i,caller)
        if (.not.((j.le.i%N).and.(j.ge.1))) then
          write(*,*) 'Error: IS index bound violated in ',caller,' in IS_base.f90'
          write(*,*) '(j.ge.1) = ',(j.ge.1)
          write(*,*) '(j.le.i%N) = ',(j.le.i%N)
          write(*,*) 'j = ',j
          write(*,*) 'i%N = ',i%N
          stop 'Done'
        endif
      end subroutine

      subroutine insist_same_size_IS(a,b,caller)
        implicit none
        type(IS),intent(in) :: a,b
        character(len=*),intent(in) :: caller
        if (a%N.ne.b%N) then
          write(*,*) 'Error: size mismatch in ',caller,' in IS_base.f90'
          write(*,*) 'a%N = ',a%N
          write(*,*) 'b%N = ',b%N
          stop 'Done'
        endif
      end subroutine

      end module