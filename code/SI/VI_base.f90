      module VI_base_mod
      ! Compiler flags: (_DEBUG_VI_)
      use current_precision_mod
      implicit none

      private
      public :: VI
      public :: init,delete,export,print

      public :: check_allocated,check_bounds

      type VI
        integer :: s
        integer,dimension(:),allocatable :: i
      end type

      interface init;              module procedure init_size;            end interface
      interface init;              module procedure init_array;           end interface
      interface init;              module procedure init_copy;            end interface
      interface delete;            module procedure delete_VI;            end interface
      interface export;            module procedure export_VI;            end interface
      interface print;             module procedure print_VI;             end interface

      interface check_bounds;      module procedure check_bounds_VI;      end interface
      interface check_allocated;   module procedure check_allocated_VI;   end interface

      contains

      subroutine init_size(v,s)
        implicit none
        type(VI),intent(inout) :: v
        integer,intent(in) :: s
#ifdef _DEBUG_VI_
        if (s.lt.1) stop 'Error: cannot init array of size<1 in init_size in VI_base.f90'
#endif
        call delete(v)
        v%s = s
        allocate(v%i(s))
      end subroutine

      subroutine init_array(v,i,s)
        implicit none
        type(VI),intent(inout) :: v
        integer,dimension(s),intent(in) :: i
        integer,intent(in) :: s
#ifdef _DEBUG_VI_
        if (size(i).ne.s) stop 'Error: size(i) mismatch in init_size in VI_base.f90'
#endif
        call init(v,s)
        v%i = i
      end subroutine

      subroutine init_copy(a,b)
        implicit none
        type(VI),intent(inout) :: a
        type(VI),intent(in) :: b
        if (allocated(b%i)) then
          call delete(a)
          allocate(a%i(b%s))
          a%s = b%s
          a%i = b%i
        endif
      end subroutine

      subroutine check_allocated_VI(v,s)
        implicit none
        type(VI),intent(in) :: v
        character(len=*),intent(in) :: s
        if (.not.allocated(v%i)) then
          write(*,*) 'Error: need allocated VI in ',s,' in VI_base.f90'
          write(*,*) 'v%s = ',v%s
          stop 'Done'
        endif
      end subroutine

      subroutine check_bounds_VI(v,i,s)
        implicit none
        type(VI),intent(in) :: v
        integer,intent(in) :: i
        character(len=*),intent(in) :: s
        call check_allocated(v,s)
        if (.not.((i.le.v%s).and.(i.ge.1))) then
          write(*,*) 'Error: VI index bound violated in ',s,' in VI_base.f90'
          write(*,*) '(i.ge.1) = ',(i.ge.1)
          write(*,*) '(i.le.v%s) = ',(i.le.v%s)
          write(*,*) 'i = ',i
          write(*,*) 'v%s = ',v%s
          stop 'Done'
        endif
      end subroutine

      subroutine delete_VI(v)
        implicit none
        type(VI),intent(inout) :: v
        if (allocated(v%i)) deallocate(v%i)
        v%s = 0
      end subroutine

      subroutine print_VI(v)
        implicit none
        type(VI),intent(in) :: v
        call export(v,6)
      end subroutine

      subroutine export_VI(v,un)
        implicit none
        type(VI),intent(in) :: v
        integer,intent(in) :: un
        integer :: j
        write(un,*) 'v%s = ',v%s
        if (.not.allocated(v%i)) then
          write(un,*) 'Empty VI set.'
        else
          do j=1,v%s; write(un,*) 'v%i(',j,') = ',v%i(j); enddo
        endif
      end subroutine

      end module