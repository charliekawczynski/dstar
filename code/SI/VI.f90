      module VI_mod
      use VI_base_mod
      use VI_assign_mod
      use VI_insert_mod
      implicit none

      private
      public :: VI,init,delete,export,print
      public :: assign
      public :: insert
      public :: insert_unique
      public :: append

      end module