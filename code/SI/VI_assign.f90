      module VI_assign_mod
      use current_precision_mod
      use VI_base_mod
      implicit none

      private
      public :: assign

      interface assign;       module procedure assign_VI_C;         end interface
      interface assign;       module procedure assign_VI_VI;        end interface
      interface assign;       module procedure assign_VI_VI_i_i;    end interface
      interface assign;       module procedure assign_VI_element;   end interface
      interface assign;       module procedure assign_VI_VI_all;    end interface

      contains

      subroutine assign_VI_C(A,B,i,s)
        implicit none
        type(VI),intent(inout) :: A
        integer,intent(in) :: B
        integer,intent(in) :: s
        integer,dimension(s),intent(in) :: i
        integer :: j
        do j=1,s
#ifdef _DEBUG_VI_
          call check_bounds(A,i(j),'assign_VI_VI')
#endif
          A%i(i(j)) = B
        enddo
      end subroutine

      subroutine assign_VI_element(A,B,i)
        implicit none
        type(VI),intent(inout) :: A
        integer,intent(in) :: B
        integer,intent(in) :: i
#ifdef _DEBUG_VI_
        call check_bounds(A,i,'assign_VI_element')
#endif
        A%i(i) = B
      end subroutine

      subroutine assign_VI_VI(A,B,i,s)
        implicit none
        type(VI),intent(inout) :: A
        type(VI),intent(in) :: B
        integer,dimension(s),intent(in) :: i
        integer,intent(in) :: s
        integer :: j
        do j=1,s
#ifdef _DEBUG_VI_
          call check_bounds(A,i(j),'assign_VI_VI')
          call check_bounds(B,i(j),'assign_VI_VI')
#endif
          A%i(i(j)) = B%i(i(j))
        enddo
      end subroutine

      subroutine assign_VI_VI_i_i(A,B,i_A,i_B,s)
        implicit none
        type(VI),intent(inout) :: A
        type(VI),intent(in) :: B
        integer,dimension(s),intent(in) :: i_A,i_B
        integer,intent(in) :: s
        integer :: j
        do j=1,s
#ifdef _DEBUG_VI_
          call check_bounds(A,i_A(j),'assign_VI_VI')
          call check_bounds(B,i_B(j),'assign_VI_VI')
#endif
          A%i(i_A(j)) = B%i(i_B(j))
        enddo
      end subroutine

      subroutine assign_VI_VI_all(A,B)
        implicit none
        type(VI),intent(inout) :: A
        type(VI),intent(in) :: B
        integer :: j
        do j=1,B%s
#ifdef _DEBUG_VI_
          call check_bounds(A,j,'assign_VI_VI_all')
          call check_bounds(B,j,'assign_VI_VI_all')
#endif
          A%i(j) = B%i(j)
        enddo
      end subroutine

      end module