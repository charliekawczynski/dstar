     module mpi_obj_mod
     ! Compiler flags (_DEBUG_MPI_)
     use mpi
     implicit none

     private
     public :: mpi_obj
     public :: init,delete,print,export,import

     public :: MCW,ID,size,get_cp,get_ip,tag

     public :: master,crew,processor
     public :: distribute_int

     interface init;           module procedure init_MPI;           end interface
     interface init;           module procedure init_MPI_copy;      end interface
     interface delete;         module procedure delete_MPI;         end interface
     interface display;        module procedure display_MPI;        end interface
     interface print;          module procedure print_MPI;          end interface
     interface export;         module procedure export_MPI;         end interface
     interface import;         module procedure import_MPI;         end interface

     interface MCW;            module procedure MCW_MPI;            end interface
     interface ID;             module procedure processor_ID_MPI;   end interface
     interface size;           module procedure N_processors_MPI;   end interface
     interface get_cp;         module procedure get_cp_MPI;         end interface
     interface get_ip;         module procedure get_ip_MPI;         end interface
     interface tag;            module procedure tag_MPI;            end interface

     interface processor;      module procedure processor_MPI;      end interface
     interface crew;           module procedure crew_MPI;           end interface
     interface master;         module procedure master_MPI;         end interface

     interface distribute_int; module procedure distribute_int_MPI; end interface

     type mpi_obj
       private
       integer :: i_err,rank,num_procs
       integer :: MPI_COMM_WORLD
       integer :: MPI_ANY_TAG

       integer :: cp_MPI,ip_MPI
       integer :: MPI_REAL,MPI_DOUBLE_PRECISION
       integer :: MPI_INTEGER,MPI_LONG
     end type

     contains

     subroutine init_MPI(M)
       implicit none
       type(mpi_obj),intent(inout) :: M
       integer,dimension(3) :: i_err_temp
       M%MPI_COMM_WORLD = MPI_COMM_WORLD
       ! M%MPI_ANY_TAG = MPI_ANY_TAG
       M%MPI_ANY_TAG = 8
       M%MPI_DOUBLE_PRECISION = MPI_DOUBLE_PRECISION
       M%MPI_REAL = MPI_REAL
       M%MPI_INTEGER = MPI_INTEGER
       M%MPI_LONG = MPI_LONG
       M%ip_MPI = MPI_INTEGER

#ifdef _QUAD_PRECISION_
       stop 'Error: quad precision not supported on MPI in init_MPI in mpi_obj.f90'
#else
#ifdef _SINGLE_PRECISION_
       M%cp_MPI = MPI_REAL
#else
       M%cp_MPI = MPI_DOUBLE_PRECISION
#endif
#endif

       call MPI_Init(M%i_err)
       i_err_temp(1) = M%i_err
       call MPI_Comm_rank(M%MPI_COMM_WORLD,M%rank,M%i_err)
       i_err_temp(2) = M%i_err
       call MPI_Comm_size(M%MPI_COMM_WORLD,M%num_procs,M%i_err)
       i_err_temp(3) = M%i_err
#ifdef _DEBUG_MPI_
       call insist_no_mpi_error(i_err_temp(1),'init_MPI(1)')
       call insist_no_mpi_error(i_err_temp(2),'init_MPI(2)')
       call insist_no_mpi_error(i_err_temp(3),'init_MPI(3)')
#endif

     end subroutine

     subroutine init_MPI_copy(a,b)
       implicit none
       type(mpi_obj),intent(inout) :: a
       type(mpi_obj),intent(in) :: b
       a%MPI_COMM_WORLD = b%MPI_COMM_WORLD
       a%MPI_ANY_TAG = b%MPI_ANY_TAG
       a%MPI_DOUBLE_PRECISION = b%MPI_DOUBLE_PRECISION
       a%MPI_REAL = b%MPI_REAL
       a%MPI_INTEGER = b%MPI_INTEGER
       a%MPI_LONG = b%MPI_LONG
       a%cp_MPI = b%cp_MPI
       a%ip_MPI = b%ip_MPI

       a%i_err = b%i_err
       a%num_procs = b%num_procs
       a%rank = b%rank
     end subroutine

     subroutine delete_MPI(M)
       implicit none
       type(mpi_obj),intent(inout) :: M
       call MPI_Finalize(M%i_err)
     end subroutine

     subroutine export_MPI(M,un)
       implicit none
       type(mpi_obj),intent(in) :: M
       integer,intent(in) :: un
       write(un,*) 'i_err = ';                write(un,*) M%i_err
       write(un,*) 'num_procs = ';            write(un,*) M%num_procs
       write(un,*) 'ID = ';                   write(un,*) M%rank
       write(un,*) 'MPI_COMM_WORLD = ';       write(un,*) M%MPI_COMM_WORLD
       write(un,*) 'MPI_ANY_TAG = ';          write(un,*) M%MPI_ANY_TAG
       write(un,*) 'cp_MPI = ';               write(un,*) M%cp_MPI
       write(un,*) 'ip_MPI = ';               write(un,*) M%ip_MPI
       write(un,*) 'MPI_DOUBLE_PRECISION = '; write(un,*) M%MPI_DOUBLE_PRECISION
       write(un,*) 'MPI_REAL = ';             write(un,*) M%MPI_REAL
       write(un,*) 'MPI_INTEGER = ';          write(un,*) M%MPI_INTEGER
       write(un,*) 'MPI_LONG = ';             write(un,*) M%MPI_LONG
     end subroutine

     subroutine import_MPI(M,un)
       implicit none
       type(mpi_obj),intent(inout) :: M
       integer,intent(in) :: un
       read(un,*); read(un,*) M%i_err
       read(un,*); read(un,*) M%num_procs
       read(un,*); read(un,*) M%rank
       read(un,*); read(un,*) M%MPI_COMM_WORLD
       read(un,*); read(un,*) M%MPI_ANY_TAG
       read(un,*); read(un,*) M%cp_MPI
       read(un,*); read(un,*) M%ip_MPI
       read(un,*); read(un,*) M%MPI_DOUBLE_PRECISION
       read(un,*); read(un,*) M%MPI_REAL
       read(un,*); read(un,*) M%MPI_INTEGER
       read(un,*); read(un,*) M%MPI_LONG
     end subroutine

     subroutine display_MPI(M,un)
       implicit none
       type(mpi_obj),intent(in) :: M
       integer,intent(in) :: un
       write(un,*) ' --------------- MPI --------------- '
       write(un,*) 'i_err = ',M%i_err
       write(un,*) 'num_procs = ',M%num_procs
       write(un,*) 'ID = ',M%rank
       write(un,*) 'MPI_COMM_WORLD = ',M%MPI_COMM_WORLD
       write(un,*) 'MPI_ANY_TAG = ', M%MPI_ANY_TAG
       write(un,*) ''
       write(un,*) 'cp_MPI = ', M%cp_MPI
       write(un,*) 'ip_MPI = ', M%ip_MPI
       write(un,*) 'MPI_DOUBLE_PRECISION = ', M%MPI_DOUBLE_PRECISION
       write(un,*) 'MPI_REAL = ', M%MPI_REAL
       write(un,*) 'MPI_INTEGER = ', M%MPI_INTEGER
       write(un,*) 'MPI_LONG = ', M%MPI_LONG
       write(un,*) ' ----------------------------------- '
     end subroutine

     subroutine print_MPI(M)
       implicit none
       type(mpi_obj),intent(in) :: M
       call display(M,6)
     end subroutine

     function processor_ID_MPI(M) result(ID)
       implicit none
       type(mpi_obj),intent(in) :: M
       integer :: ID,i_err
       call MPI_Comm_rank(M%MPI_COMM_WORLD,ID,i_err)
#ifdef _DEBUG_MPI_
       call insist_no_mpi_error(i_err,'processor_ID_MPI')
#endif
     end function

     function N_processors_MPI(M) result(s)
       implicit none
       type(mpi_obj),intent(in) :: M
       integer :: s,i_err
       call MPI_Comm_size(M%MPI_COMM_WORLD,s,i_err)
#ifdef _DEBUG_MPI_
       call insist_no_mpi_error(i_err,'N_processors_MPI')
#endif
     end function

     function MCW_MPI(M) result(MPI_COMM_WORLD)
       implicit none
       type(mpi_obj),intent(in) :: M
       integer :: MPI_COMM_WORLD
       MPI_COMM_WORLD = M%MPI_COMM_WORLD
     end function

     function master_MPI(M) result(L)
       implicit none
       type(mpi_obj),intent(in) :: M
       logical :: L
       L = ID(M).eq.0
     end function

     function crew_MPI(M) result(L)
       implicit none
       type(mpi_obj),intent(in) :: M
       logical :: L
       L = ID(M).ne.0
     end function

     function processor_MPI(M,proc) result(L)
       implicit none
       type(mpi_obj),intent(in) :: M
       integer,intent(in) :: proc
       logical :: L
       L = ID(M).eq.proc-1
     end function

     function get_cp_MPI(M) result(d)
       implicit none
       type(mpi_obj),intent(in) :: M
       integer :: d
       d = M%cp_MPI
     end function

     function get_ip_MPI(M) result(i)
       implicit none
       type(mpi_obj),intent(in) :: M
       integer :: i
       i = M%ip_MPI
     end function

     function tag_MPI(M) result(t)
       implicit none
       type(mpi_obj),intent(in) :: M
       integer :: t
       t = M%MPI_ANY_TAG
     end function

     subroutine distribute_int_MPI(m,i) ! distribute integer from master to crew
       implicit none
       type(mpi_obj),intent(in) :: m
       integer,intent(inout) :: i
       integer :: ierr
       call mpi_bcast(i,1,get_ip(m),0,MCW(m),ierr)
     end subroutine

#ifdef _DEBUG_MPI_
     subroutine insist_no_mpi_error(i_err,caller)
       implicit none
       integer,intent(in) :: i_err
       character(len=*),intent(in) :: caller
       if (i_err.ne.0) then
         write(*,*) 'Error: mpi error in '//caller//' in mpi_obj.f90'
         write(*,*) 'i_err = ',i_err
         stop 'Done'
       endif
     end subroutine
#endif

     end module